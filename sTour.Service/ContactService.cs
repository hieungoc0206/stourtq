﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IContactService
    {
        void Insert(Contact item);

        void Update(Contact item);

        void Delete(int Id);

        Contact GetById(int Id);

        IEnumerable<Contact> GetAll();

        int Count();

        IEnumerable<Contact> GetContact(int page, int pageSize);
    }

    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepository;

        public ContactService(IContactRepository contactRepostiory)
        {
            this._contactRepository = contactRepostiory;
        }

        public void Insert(Contact item)
        {
            if (item == null)
                throw new ArgumentNullException("Contact");
            _contactRepository.Insert(item);
        }

        public void Update(Contact item)
        {
            if (item == null)
                throw new ArgumentNullException("Contact");
            _contactRepository.Update(item);
        }

        public Contact GetById(int Id)
        {
            return _contactRepository.GetSingleById(Id);
        }

        public void Delete(int Id)
        {
            if (Id > 0)
            {
                var item = _contactRepository.GetSingleById(Id);
                _contactRepository.Delete(item);
            }
        }

        public IEnumerable<Contact> GetAll()
        {
            return _contactRepository.GetAll();
        }

        public IEnumerable<Contact> GetContact(int page, int pageSize)
        {
            return _contactRepository.GetContact(page, pageSize);
        }

        public int Count()
        {
            return _contactRepository.Count();
        }
    }
}