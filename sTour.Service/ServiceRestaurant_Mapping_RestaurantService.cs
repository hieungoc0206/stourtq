﻿using sTour.Data.Repositories;
using sTour.Entity;

namespace sTour.Service
{
    public interface IServiceRestaurant_Mapping_RestaurantService
    {
        void Insert(ServiceRestaurant_Mapping_Restaurant item);

        void DeleteByRestaurantId(int Id);
    }

    public class ServiceRestaurant_Mapping_RestaurantService : IServiceRestaurant_Mapping_RestaurantService
    {
        private IServiceRestaurant_Restaurant_MappingRepository _mappingRepository;

        public ServiceRestaurant_Mapping_RestaurantService(IServiceRestaurant_Restaurant_MappingRepository mappingRepository)
        {
            this._mappingRepository = mappingRepository;
        }

        public void Insert(ServiceRestaurant_Mapping_Restaurant item)
        {
            _mappingRepository.Insert(item);
        }

        public void DeleteByRestaurantId(int Id)
        {
            _mappingRepository.DeleteMulti(c => c.RestaurantId == Id);
        }
    }
}