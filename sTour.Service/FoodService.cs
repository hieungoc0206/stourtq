﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IFoodService
    {
        void Insert(Foods item);

        void Update(Foods item);

        void Delete(int Id);

        Foods GetById(int Id);

        IEnumerable<Foods> GetAll();

        IEnumerable<Foods> GetFoods(int page, int pageSize, string keyword, out int total);
    }

    public class FoodService : IFoodService
    {
        private readonly IFoodRepository _foodRepository;

        public FoodService(IFoodRepository foodRepository)
        {
            this._foodRepository = foodRepository;
        }

        public void Insert(Foods item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert Foods");
            }
            _foodRepository.Insert(item);
        }

        public void Update(Foods item)
        {
            _foodRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _foodRepository.GetSingleById(Id);
            if (item != null)
            {
                _foodRepository.Delete(item);
            }
        }

        public Foods GetById(int Id)
        {
            return _foodRepository.GetSingleById(Id);
        }

        public IEnumerable<Foods> GetAll()
        {
            return _foodRepository.GetAll();
        }

        public IEnumerable<Foods> GetFoods(int page, int pageSize, string keyword, out int total)
        {
            return _foodRepository.GetFoods(page, pageSize, keyword, out total);
        }
    }
}