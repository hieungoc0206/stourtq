﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IPermissionService
    {
        void Insert(Permisstion item);

        void Update(Permisstion item);

        void Delete(int Id);

        Permisstion GetById(int Id);

        IEnumerable<Permisstion> GetAll();

        IEnumerable<Permisstion> GetListPermissionByRoleId(string Id);

        void AddPermisionsToRole(List<Role_Permisstion_Mapping> permisions, string roleId);
    }

    public class PermisstionService : IPermissionService
    {
        private readonly IPermissionRepository _permisstionRepository;
        private readonly IRole_Permisstion_MappingRepository _role_Permisstion_MappingRepository;

        public PermisstionService(IPermissionRepository permisstionRepository, IRole_Permisstion_MappingRepository role_Permisstion_MappingRepository)
        {
            this._permisstionRepository = permisstionRepository;
            this._role_Permisstion_MappingRepository = role_Permisstion_MappingRepository;
        }

        public void Insert(Permisstion item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Permisstion");
            }
            _permisstionRepository.Insert(item);
        }

        public void Update(Permisstion item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Permisstion");
            }
            _permisstionRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _permisstionRepository.GetSingleById(Id);
            if (item != null)
            {
                _permisstionRepository.Delete(item);
            }
        }

        public Permisstion GetById(int Id)
        {
            if (Id == 0)
            {
                return null;
            }
            return _permisstionRepository.GetSingleById(Id);
        }

        public IEnumerable<Permisstion> GetAll()
        {
            return _permisstionRepository.GetAll();
        }

        public IEnumerable<Permisstion> GetListPermissionByRoleId(string Id)
        {
            return _permisstionRepository.GetListPermissionByRoleId(Id);
        }

        public void AddPermisionsToRole(List<Role_Permisstion_Mapping> rolePermisions, string roleId)
        {
            _role_Permisstion_MappingRepository.DeleteMulti(x => x.RoleId.Equals(roleId));
            rolePermisions.ForEach(p =>
            {
                _role_Permisstion_MappingRepository.Insert(p);
            });
        }
    }
}