﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface ICountryService
    {
        void Insert(Country item);

        void Update(Country item);

        void Delete(int Id);

        Country GetById(int Id);

        IEnumerable<Country> GetAll();

        IEnumerable<Country> GetCountry(int page, int pageSize,string keyword, out int total);
    }

    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository countryRepository)
        {
            this._countryRepository = countryRepository;
        }

        public void Insert(Country item)
        {
            if (item == null)
                throw new ArgumentNullException("Country");
            _countryRepository.Insert(item);
        }

        public void Update(Country item)
        {
            if (item == null)
                throw new ArgumentNullException("Country");
            _countryRepository.Update(item);
        }

        public void Delete(int Id)
        {
            if (Id > 0)
            {
                var item = _countryRepository.GetSingleById(Id);
                _countryRepository.Delete(item);
            }
        }

        public Country GetById(int Id)
        {
            return _countryRepository.GetSingleById(Id);
        }

        public IEnumerable<Country> GetCountry(int page, int pageSize,string keyword, out int total)
        {
            return _countryRepository.GetCountry(page, pageSize,keyword,out total);
        }

        public IEnumerable<Country> GetAll()
        {
            return _countryRepository.GetAll();
        }
    }
}