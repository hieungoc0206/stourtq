﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IActivityLogService
    {
        void Insert(ActivityLog item);

        void Update(ActivityLog item);

        void Delete(int Id);

        ActivityLog GetById(int Id);

        int Count();

        IEnumerable<ActivityLog> GetAll();

        IEnumerable<ActivityLog> GetActivityLog(int page, int pageSize);
    }

    public class ActivityLogService : IActivityLogService
    {
        private IActivityLogRepository _activityRepository;

        public ActivityLogService(IActivityLogRepository activityRepository)
        {
            this._activityRepository = activityRepository;
        }

        public void Insert(ActivityLog item)
        {
            _activityRepository.Insert(item);
        }

        public void Update(ActivityLog item)
        {
            _activityRepository.Update(item);
        }

        public ActivityLog GetById(int Id)
        {
            return _activityRepository.GetSingleById(Id);
        }

        public void Delete(int Id)
        {
            if (Id > 0)
            {
                var item = _activityRepository.GetSingleById(Id);
                _activityRepository.Delete(item);
            }
        }

        public int Count()
        {
            return _activityRepository.Count();
        }

        public IEnumerable<ActivityLog> GetAll()
        {
            return _activityRepository.GetAll();
        }

        public IEnumerable<ActivityLog> GetActivityLog(int page, int pageSize)
        {
            return _activityRepository.GetActivityLog(page, pageSize);
        }
    }
}