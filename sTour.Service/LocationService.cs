﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;
using sTour.Data.Model;
using System.Linq;

namespace sTour.Service
{
    public interface ILocationService
    {
        void Insert(Location item);

        void Update(Location item);

        void Delete(int Id);

        Location GetById(int Id);

        IEnumerable<Location> GetAll();
        IEnumerable<AllMarker> GetAllMarker();

        IEnumerable<Location> GetByCategory(int categoryId);

        IEnumerable<Location> GetLocations(int page, int pageSize, string keyword, out int total);

        void UpdateLatLon(int locationId, double lat, double lon);

        IEnumerable<Location> GetByHotelId(int hotelId);
        IEnumerable<Location> GetByRestaurantId(int restaurantId);
        IEnumerable<Location> GetLocationByNumber(int number);
        IEnumerable<Location> GetLocationInHome();
    }
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;

        public LocationService(ILocationRepository locationRepository)
        {
            this._locationRepository = locationRepository;
        }

        public void Insert(Location item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert Location");
            }
            _locationRepository.Insert(item);
        }

        public void Update(Location item)
        {
            _locationRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _locationRepository.GetSingleById(Id);
            if (item != null)
            {
                _locationRepository.Delete(item);
            }
        }

        public Location GetById(int Id)
        {
            return _locationRepository.GetSingleById(Id);
        }

        public IEnumerable<Location> GetAll()
        {
            return _locationRepository.GetAll();
        }

        public IEnumerable<Location> GetByCategory(int categoryId)
        {
            return _locationRepository.GetByCategory(categoryId);
        }

        public IEnumerable<Location> GetLocations(int page, int pageSize, string keyword, out int total)
        {
            return _locationRepository.GetLocations(page, pageSize, keyword, out total);
        }

        public void UpdateLatLon(int locationId, double lat, double lon)
        {
            _locationRepository.UpdateLatLon(locationId, lat, lon);
        }

        public IEnumerable<Location> GetByHotelId(int hotelId)
        {
            return _locationRepository.GetByHotelId(hotelId);
        }

        public IEnumerable<Location> GetByRestaurantId(int restaurantId)
        {
            return _locationRepository.GetByRestaurantId(restaurantId);
        }
        public IEnumerable<Location> GetLocationByNumber(int number)
        {
            return _locationRepository.GetLocationByNumber(number);
        }

        public IEnumerable<Location> GetLocationInHome()
        {
            return _locationRepository.GetLocationInHome();
        }

        public IEnumerable<AllMarker> GetAllMarker()
        {
            var locations = _locationRepository.GetAll();
            var query = (from l in locations
                         select new AllMarker
                         {
                             Name = l.Name,
                             Url = l.Avatar,
                             Lat = l.Latitude,
                             Lon = l.Longitude,
                             Description = l.ShortDes,
                         });
            return query;
        }
    }
}
