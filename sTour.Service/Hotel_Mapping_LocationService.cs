﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sTour.Entity;
using sTour.Data.Repositories;
namespace sTour.Service
{
    public interface IHotel_Mapping_LocationService
    {
        void Insert(Hotel_Mapping_Location item);
        void DeleteByHotelId(int Id);
    }
    public class Hotel_Mapping_LocationService : IHotel_Mapping_LocationService
    {
        private readonly IHotel_Mapping_LocationRepository _mappingRepository;
        public Hotel_Mapping_LocationService(IHotel_Mapping_LocationRepository mappingRepository)
        {
            this._mappingRepository = mappingRepository;
        }

        public void Insert(Hotel_Mapping_Location item)
        {
            _mappingRepository.Insert(item);
        }

        public void DeleteByHotelId(int Id)
        {
            _mappingRepository.DeleteMulti(c=>c.HotelId == Id);
        }
    }

}
