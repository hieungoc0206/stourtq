﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;
namespace sTour.Service
{
    public interface IProvincialService
    {
        void Insert(Provincial item);

        void Update(Provincial item);

        void Delete(int Id);

        Provincial GetById(int Id);

        IEnumerable<Provincial> GetProvincial(int page, int pageSize, string keyword, out int total);

        IEnumerable<Provincial> GetAll();

        int Count();
    }
    public class ProvincialService : IProvincialService
    {
        private readonly IProvincialRepository _provincialRepository;

        public ProvincialService(IProvincialRepository provincialRepository)
        {
            this._provincialRepository = provincialRepository;
        }

        public void Insert(Provincial item)
        {
            if (item == null)
                throw new ArgumentNullException("Provincial");
            _provincialRepository.Insert(item);
        }

        public void Update(Provincial item)
        {
            if (item == null)
                throw new ArgumentNullException("Provincial");
            _provincialRepository.Update(item);
        }

        public void Delete(int Id)
        {
            if(Id > 0)
            {
                var item = _provincialRepository.GetSingleById(Id);
                _provincialRepository.Delete(item);
            }
        }

        public Provincial GetById(int Id)
        {
            return _provincialRepository.GetSingleById(Id);
        }

        public IEnumerable<Provincial> GetAll()
        {
            return _provincialRepository.GetAll();
        }

        public IEnumerable<Provincial> GetProvincial(int page, int pageSize, string keyword, out int total)
        {
            return _provincialRepository.GetProvincial(page, pageSize, keyword, out total);
        }

        public int Count()
        {
            return _provincialRepository.Count();
        }
    }
}