﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IExceptionLogService
    {
        void Insert(ExceptionLog item);

        void Delete(int Id);

        IEnumerable<ExceptionLog> GetAll();

        int Count();

        IEnumerable<ExceptionLog> GetExceptionLog(int page, int pageSize);
    }

    public class ExceptionLogService : IExceptionLogService
    {
        private readonly IExceptionLogRepository _exceptionLogRepository;

        public ExceptionLogService(IExceptionLogRepository exceptionLogRepository)
        {
            this._exceptionLogRepository = exceptionLogRepository;
        }

        public void Insert(ExceptionLog item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("ExceptionLog");
            }
            _exceptionLogRepository.Insert(item);
        }

        public void Delete(int Id)
        {
            var item = _exceptionLogRepository.GetSingleById(Id);
            if (item != null)
            {
                _exceptionLogRepository.Delete(item);
            }
        }
        public IEnumerable<ExceptionLog> GetAll()
        {
            return _exceptionLogRepository.GetAll();
        }

        public IEnumerable<ExceptionLog> GetExceptionLog(int page, int pageSize)
        {
            return _exceptionLogRepository.GetExceptionLog(page, pageSize);
        }

        public int Count()
        {
            return _exceptionLogRepository.Count();
        }
    }
}