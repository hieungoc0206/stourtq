﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;
using System.Linq;
using sTour.Data.Model;

namespace sTour.Service
{
    public interface IHotelService
    {
        void Insert(Hotel item);

        void Update(Hotel item);

        void Delete(int Id);

        Hotel GetById(int Id);

        IEnumerable<Hotel> GetAll();

        IEnumerable<Hotel> GetByLocationId(int locationId);

        IEnumerable<Hotel> GetHotels(int? enterpriseId, int page, int pageSize, string keyword, out int total);

        IEnumerable<Hotel> GetHotelPagings(int page, int pageSize, string keyword, string sort,int fromprice, int toprice, List<int> starts, List<int> services, out int total);

        IEnumerable<Hotel> GetHotelInHome();
        IEnumerable<AllMarker> GetAllMarker();
        IEnumerable<SearchResult> SearchQuery(string q);
    }
    public class HotelService : IHotelService
    {
        private readonly IHotelRepository _hotelRepository;

        public HotelService(IHotelRepository hotelRepository)
        {
            this._hotelRepository = hotelRepository;
        }

        public void Insert(Hotel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert Hotel");
            }
            _hotelRepository.Insert(item);
        }

        public void Update(Hotel item)
        {
            _hotelRepository.Update(item);

        }

        public void Delete(int Id)
        {
            var item = _hotelRepository.GetSingleById(Id);
            if (item != null)
            {
                _hotelRepository.Delete(item);
            }
        }

        public Hotel GetById(int Id)
        {
            return _hotelRepository.GetById(Id);
        }

        public IEnumerable<Hotel> GetAll()
        {
            return _hotelRepository.GetAll();
        }

        public IEnumerable<Hotel> GetByLocationId(int locationId)
        {
            return _hotelRepository.GetByLocationId(locationId);
        }

        public IEnumerable<Hotel> GetHotels(int? enterpriseId, int page, int pageSize, string keyword, out int total)
        {
            return _hotelRepository.GetHotels(enterpriseId, page, pageSize, keyword, out total);
        }

        public IEnumerable<Hotel> GetHotelInHome()
        {
            var hotels = _hotelRepository.GetHotelInHome();
            return hotels;
        }

        public IEnumerable<Hotel> GetHotelPagings(int page, int pageSize, string keyword, string sort, int fromprice, int toprice, List<int> starts, List<int> services, out int total)
        {
            var model= _hotelRepository.GetHotelPagings(page, pageSize, keyword, sort, fromprice, toprice, starts, services, out total);
            return _hotelRepository.GetHotelPagings(page, pageSize, keyword, sort, fromprice, toprice,starts, services, out total);
        }
        public IEnumerable<AllMarker> GetAllMarker()
        {
            var hotels = _hotelRepository.GetAll();
            var query = (from h in hotels
                         select new AllMarker
                         {
                             Name = h.Name,
                             Url = h.Avatar,
                             Lat = h.Latitude,
                             Lon = h.Longitude,
                             Description = h.ShortDes,
                             Star = h.Star
                         });
            return query;
        }

        public IEnumerable<SearchResult> SearchQuery(string q)
        {
            return _hotelRepository.SearchQuery(q);
        }
    }
}
