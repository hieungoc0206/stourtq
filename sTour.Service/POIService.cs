﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IPOIService
    {
        IEnumerable<POI> GetAll();

        POI GetById(int Id);

        void Insert(POI poi);
    }

    public class POIService : IPOIService
    {
        private readonly IPOIRepository _poiRepository;

        public POIService(IPOIRepository poiRepository)
        {
            this._poiRepository = poiRepository;
        }

        public IEnumerable<POI> GetAll()
        {
            return _poiRepository.GetAll();
        }

        public POI GetById(int Id)
        {
            return _poiRepository.GetSingleById(Id);
        }

        public void Insert(POI poi)
        {
            _poiRepository.Insert(poi);
        }
    }
}