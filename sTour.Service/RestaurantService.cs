﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;
using System.Linq;
using sTour.Data.Model;

namespace sTour.Service
{
    public interface IRestaurantService
    {
        void Insert(Restaurant item);

        void Update(Restaurant item);

        void Delete(int Id);

        Restaurant GetById(int Id);

        IEnumerable<Restaurant> GetAll();


        IEnumerable<Restaurant> GetByLocationId(int locationId);

        IEnumerable<Restaurant> GetRestaurants(int? enterpriseId, int page, int pageSize, string keyword, out int total);

        IEnumerable<Restaurant> GetRestaurantPagings(int page, int pageSize, string keyword, string sort, int fromprice, int toprice, List<int> starts, List<int> services, out int total);

        IEnumerable<Restaurant> GetRestaurantsBySearch(SearchOptionHome option, out int total);

        IEnumerable<Restaurant> GetRestaurantInHome();
        IEnumerable<AllMarker> GetAllMarker();

    }
    public class RestaurantService : IRestaurantService
    {
        private readonly IRestaurantRepository _restaurantRepository;
        public RestaurantService(IRestaurantRepository restaurantRepository, IServiceRestaurant_Restaurant_MappingRepository servicerestaurant_mappingRepository)
        {
            this._restaurantRepository = restaurantRepository;
        }

        public void Insert(Restaurant item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert Restaurant");
            }
            _restaurantRepository.Insert(item);
          
        }

        public void Update(Restaurant item)
        {
            _restaurantRepository.Update(item);
            

        }

        public void Delete(int Id)
        {
            var item = _restaurantRepository.GetSingleById(Id);
            if (item != null)
            {
                _restaurantRepository.Delete(item);
            }
        }

        public Restaurant GetById(int Id)
        {
            return _restaurantRepository.GetSingleById(Id);
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurantRepository.GetAll();
        }

        public IEnumerable<Restaurant> GetByLocationId(int locationId)
        {
            return _restaurantRepository.GetByLocationId(locationId);
        }

        public IEnumerable<Restaurant> GetRestaurants(int? enterpriseId, int page, int pageSize, string keyword, out int total)
        {
            return _restaurantRepository.GetRestaurants(enterpriseId, page, pageSize, keyword, out total);
        }

        public IEnumerable<Restaurant> GetRestaurantsBySearch(SearchOptionHome option, out int total)
        {
            return _restaurantRepository.GetRestaurantsBySearch(option, out total);
        }

        public IEnumerable<Restaurant> GetRestaurantInHome()
        {
            return _restaurantRepository.GetRestaurantInHome();
        }

        public IEnumerable<Restaurant> GetRestaurantPagings(int page, int pageSize, string keyword, string sort, int fromprice, int toprice, List<int> starts, List<int> services, out int total)
        {
            return _restaurantRepository.GetRestaurantPagings(page, pageSize, keyword, sort,  fromprice, toprice, starts, services, out total);
        }
        public IEnumerable<AllMarker> GetAllMarker()
        {
            var restaurants = _restaurantRepository.GetAll();
            var query = (from r in restaurants
                         select new AllMarker
                         {
                             Name = r.Name,
                             Url = r.Avatar,
                             Lat = r.Latitude,
                             Lon = r.Longitude,
                             Description = r.ShortDes,
                             Star = r.Star
                         });
            return query;
        }
    }
}
