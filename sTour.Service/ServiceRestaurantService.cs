﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IServiceRestaurantService
    {
        void Insert(ServiceRestaurant item);

        void Update(ServiceRestaurant item);

        void Delete(int Id);

        ServiceRestaurant GetById(int Id);

        IEnumerable<ServiceRestaurant> GetAll();

        IEnumerable<ServiceRestaurant> GetServiceRestaurants(int page, int pageSize, string keyword, out int total);


        IEnumerable<ServiceRestaurant> GetByRestaurantId(int restaurantId);
    }

    public class ServiceRestaurantService : IServiceRestaurantService
    {
        private readonly IServiceRestaurantRepository _servicerestaurantRepository;

        public ServiceRestaurantService(IServiceRestaurantRepository servicerestaurantRepository)
        {
            this._servicerestaurantRepository = servicerestaurantRepository;
        }

        public void Insert(ServiceRestaurant item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert ServiceRestaurant");
            }
            _servicerestaurantRepository.Insert(item);
        }

        public void Update(ServiceRestaurant item)
        {
            _servicerestaurantRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _servicerestaurantRepository.GetSingleById(Id);
            if (item != null)
            {
                _servicerestaurantRepository.Delete(item);
            }
        }

        public ServiceRestaurant GetById(int Id)
        {
            return _servicerestaurantRepository.GetSingleById(Id);
        }

        public IEnumerable<ServiceRestaurant> GetAll()
        {
            return _servicerestaurantRepository.GetAll();
        }

        public IEnumerable<ServiceRestaurant> GetServiceRestaurants(int page, int pageSize, string keyword, out int total)
        {
            return _servicerestaurantRepository.GetServiceRestaurants(page, pageSize, keyword, out total);
        }

        public IEnumerable<ServiceRestaurant> GetByRestaurantId(int restaurantId)
        {
            return _servicerestaurantRepository.GetByRestaurantId(restaurantId);
        }
    }
}