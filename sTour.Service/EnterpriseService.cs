﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;
namespace sTour.Service
{
    public interface IEnterpriseService
    {
        void Insert(Enterprise item);

        void Update(Enterprise item);

        void Delete(int Id);

        Enterprise GetById(int Id);

        IEnumerable<Enterprise> GetAll();

    }
    public class EnterpriseService : IEnterpriseService
    {
        private IEnterpriseRepository _enterpriseRepository;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository)
        {
            this._enterpriseRepository = enterpriseRepository;
        }

        public void Insert(Enterprise item)
        {
            if(item == null)
            {
                throw new ArgumentNullException("Insert enterprise");
            }
            _enterpriseRepository.Insert(item);
        }

        public void Update(Enterprise item)
        {
            _enterpriseRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _enterpriseRepository.GetSingleById(Id);
            if(item != null)
            {
                _enterpriseRepository.Delete(item);
            }
        }

        public Enterprise GetById(int Id)
        {
            return _enterpriseRepository.GetSingleById(Id);
        }

        public IEnumerable<Enterprise> GetAll()
        {
            return _enterpriseRepository.GetAll();
        }
    }
}
