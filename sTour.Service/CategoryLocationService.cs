﻿using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;

namespace sTour.Service
{
    public interface ICategoryLocationService
    {
        void Insert(CategoryLocation item);

        void Update(CategoryLocation item);

        void Delete(int Id);

        CategoryLocation GetById(int Id);

        IEnumerable<CategoryLocation> GetAll();
    }
    public class CategoryLocationService : ICategoryLocationService
    {
        private readonly ICategoryLocationRepository _categoryLocationRepository;

        public CategoryLocationService(ICategoryLocationRepository categoryLocationRepository)
        {
            this._categoryLocationRepository = categoryLocationRepository;
        }

        public void Insert(CategoryLocation item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert CategoryLocation");
            }
            _categoryLocationRepository.Insert(item);
        }

        public void Update(CategoryLocation item)
        {
            _categoryLocationRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _categoryLocationRepository.GetSingleById(Id);
            if (item != null)
            {
                _categoryLocationRepository.Delete(item);
            }
        }

        public CategoryLocation GetById(int Id)
        {
            return _categoryLocationRepository.GetSingleById(Id);
        }

        public IEnumerable<CategoryLocation> GetAll()
        {
            return _categoryLocationRepository.GetAll();
        }
    }
}
