﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface ITouristServiceService
    {
        void Insert(TouristService item);

        void Update(TouristService item);

        void Delete(int Id);

        TouristService GetById(int Id);

        IEnumerable<TouristService> GetAll();

        IEnumerable<TouristService> GetTouristService(int page, int pageSize, string keyword, out int total);
    }

    public class TouristServiceService : ITouristServiceService
    {
        private readonly ITouristServiceRepository _TouristServiceRepository;

        public TouristServiceService(ITouristServiceRepository TouristServiceRepository)
        {
            this._TouristServiceRepository = TouristServiceRepository;
        }

        public void Insert(TouristService item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("TouristService");
            }
            _TouristServiceRepository.Insert(item);
        }

        public void Update(TouristService item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("TouristService");
            }
            _TouristServiceRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _TouristServiceRepository.GetSingleById(Id);
            if (item != null)
            {
                _TouristServiceRepository.Delete(item);
            }
        }

        public TouristService GetById(int Id)
        {
            if (Id == 0)
            {
                return null;
            }
            return _TouristServiceRepository.GetSingleById(Id);
        }

        public IEnumerable<TouristService> GetAll()
        {
            return _TouristServiceRepository.GetAll();
        }

        public IEnumerable<TouristService> GetTouristService(int page, int pageSize, string keyword, out int total)
        {
            return _TouristServiceRepository.GetTouristService(page, pageSize, keyword, out total);
        }
    }
}