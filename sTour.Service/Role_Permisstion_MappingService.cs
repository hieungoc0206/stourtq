﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IRole_Permisstion_MappingService
    {
        void Insert(Role_Permisstion_Mapping item);

        void Update(Role_Permisstion_Mapping item);

        IEnumerable<Role_Permisstion_Mapping> GetAll();

        IEnumerable<Role_Permisstion_Mapping> GetByRoleId(string roleId);

        Role_Permisstion_Mapping GetByKey(string roleId, int permisstionId);

        void Delete(string roleId, int permisstionId);

    }

    public class Role_Permisstion_MappingService : IRole_Permisstion_MappingService
    {
        private readonly IRole_Permisstion_MappingRepository _role_Permisstion_MappingRepository;

        public Role_Permisstion_MappingService(IRole_Permisstion_MappingRepository role_Permisstion_MappingRepository)
        {
            this._role_Permisstion_MappingRepository = role_Permisstion_MappingRepository;
        }

        public void Insert(Role_Permisstion_Mapping item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Role_Permisstion_Mapping");
            }
            _role_Permisstion_MappingRepository.Insert(item);
        }

        public void Update(Role_Permisstion_Mapping item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Role_Permisstion_Mapping");
            }
            _role_Permisstion_MappingRepository.Update(item);
        }

        public IEnumerable<Role_Permisstion_Mapping> GetAll()
        {
            return _role_Permisstion_MappingRepository.GetAll();
        }

        public IEnumerable<Role_Permisstion_Mapping> GetByRoleId(string roleId)
        {
            return _role_Permisstion_MappingRepository.GetByRoleId(roleId);
        }

        public Role_Permisstion_Mapping GetByKey(string roleId, int permisstionId)
        {
            return _role_Permisstion_MappingRepository.GetByKey(roleId, permisstionId);
        }

        public void Delete(string roleId, int permisstionId)
        {
            var item = GetByKey(roleId, permisstionId);
            if (item != null)
            {
                _role_Permisstion_MappingRepository.Delete(item);
            }
        }


    }
}