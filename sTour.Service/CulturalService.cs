﻿using sTour.Data.Model;
using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sTour.Service
{
    public interface ICulturalService
    {
        void Insert(Cultural item);

        void Update(Cultural item);

        void Delete(int Id);

        Cultural GetById(int Id);

        IEnumerable<Cultural> GetAll();

        IEnumerable<Cultural> GetCulturals(int page, int pageSize, string keyword, out int total);
       
        IEnumerable<AllMarker> GetAllMarker();
        //Add
        //IEnumerable<Cultural> GetCulturals(int? enterpriseId, int pageSize, string keyword, out int total);
    }
    public class CulturalService : ICulturalService
    {
        private readonly ICulturalRepository _culturalRepository;

        public CulturalService(ICulturalRepository culturalRepository)
        {
            this._culturalRepository = culturalRepository;
        }

        public void Insert(Cultural item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert Cultural");
            }
            _culturalRepository.Insert(item);
        }

        public void Update(Cultural item)
        {
            _culturalRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _culturalRepository.GetSingleById(Id);
            if (item != null)
            {
                _culturalRepository.Delete(item);
            }
        }

        public Cultural GetById(int Id)
        {
            return _culturalRepository.GetSingleById(Id);
        }

        public IEnumerable<Cultural> GetAll()
        {
            return _culturalRepository.GetAll();
        }


        public IEnumerable<Cultural> GetCulturals(int page, int pageSize, string keyword, out int total)
        {
            return _culturalRepository.GetCulturals(page, pageSize, keyword, out total);
        }
        public IEnumerable<AllMarker> GetAllMarker()
        {
            var culturals = _culturalRepository.GetAll();
            var query = (from c in culturals
                         select new AllMarker
                         {
                             Name = c.Name,
                             Url = c.Avatar,
                             Lat = c.Latitude,
                             Lon = c.Longitude,
                             Description = c.ShortDes,
                             Star = c.Star
                         });
            return query;
        }

        //public IEnumerable<Cultural> GetCulturals(int? EnterpriseId, int pageSize, string keyword, out int total)
        //{
        //    return 
        //}
    }
}

