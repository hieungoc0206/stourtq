﻿using sTour.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sTour.Data.Repositories;
namespace sTour.Service
{
    public interface IServiceHotel_Mapping_HotelService
    {
        void Insert(ServiceHotel_Mapping_Hotel item);

        void DeleteByHotelId(int Id);
    }

    public class ServiceHotel_Mapping_HotelService : IServiceHotel_Mapping_HotelService
    {
        private readonly IServiceHotel_Hotel_MappingRepository _mappingRepository;

        public ServiceHotel_Mapping_HotelService(IServiceHotel_Hotel_MappingRepository mappingRepository)
        {
            this._mappingRepository = mappingRepository;
        }

        public void Insert(ServiceHotel_Mapping_Hotel item)
        {
            _mappingRepository.Insert(item);
        }

        public void DeleteByHotelId(int Id)
        {
            _mappingRepository.DeleteMulti(c => c.HotelId == Id);
        }
    }
}
