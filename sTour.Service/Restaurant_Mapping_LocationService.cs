﻿using sTour.Data.Repositories;
using sTour.Entity;

namespace sTour.Service
{
    public interface IRestaurant_Mapping_LocationService
    {
        void Insert(Restaurant_Mapping_Location item);

        void DeleteByRestaurantId(int Id);
    }

    public class Restaurant_Mapping_LocationService : IRestaurant_Mapping_LocationService
    {
        private readonly IRestaurant_Mapping_LocationRepository _mappingRepository;

        public Restaurant_Mapping_LocationService(IRestaurant_Mapping_LocationRepository mappingRepository)
        {
            this._mappingRepository = mappingRepository;
        }

        public void Insert(Restaurant_Mapping_Location item)
        {
            _mappingRepository.Insert(item);
        }

        public void DeleteByRestaurantId(int Id)
        {
            _mappingRepository.DeleteMulti(c=>c.RestaurantId == Id);
        }
    }
}