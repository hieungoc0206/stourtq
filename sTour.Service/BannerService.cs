﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Service
{
    public interface IBannerService
    {
        void Insert(Banner item);

        void Update(Banner item);

        void Delete(int Id);

        Banner GetById(int Id);

        IEnumerable<Banner> GetAll();

        IEnumerable<Banner> GetBanners(int page, int pageSize, string keyword, out int total);
    }

    public class BannerService : IBannerService
    {
        private readonly IBannerRepository _bannerRepository;

        public BannerService(IBannerRepository bannerRepository)
        {
            this._bannerRepository = bannerRepository;
        }

        public void Insert(Banner item)
        {
            if (item == null)
                throw new ArgumentNullException("Banner");
            _bannerRepository.Insert(item);
        }

        public void Update(Banner item)
        {
            if (item == null)
                throw new ArgumentNullException("Banner");
            _bannerRepository.Update(item);
        }

        public void Delete(int Id)
        {
            if (Id > 0)
            {
                var item = _bannerRepository.GetSingleById(Id);
                _bannerRepository.Delete(item);
            }
        }

        public Banner GetById(int Id)
        {
            return _bannerRepository.GetSingleById(Id);
        }

        public IEnumerable<Banner> GetBanners(int page, int pageSize, string keyword,out int total)
        {
            return _bannerRepository.GetBanners(page,pageSize,keyword,out total);
        }

        public IEnumerable<Banner> GetAll()
        {
            return _bannerRepository.GetAll().Where(x=>x.Active);
        }
    }
}