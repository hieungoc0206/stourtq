﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface ISystemConfigService
    {
        void Insert(SystemConfig item);

        void Update(SystemConfig item);

        void Delete(int Id);

        SystemConfig GetById(int Id);

        IEnumerable<SystemConfig> GetAll();
    }

    public class SystemConfigService : ISystemConfigService
    {
        private readonly ISystemConfigRepository _systemConfigRepository;

        public SystemConfigService(ISystemConfigRepository systemConfigRepository)
        {
            this._systemConfigRepository = systemConfigRepository;
        }

        public void Insert(SystemConfig item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("SystemConfig");
            }
            _systemConfigRepository.Insert(item);
        }

        public void Update(SystemConfig item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("SystemConfig");
            }
            _systemConfigRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _systemConfigRepository.GetSingleById(Id);
            if (item != null)
            {
                _systemConfigRepository.Delete(item);
            }
        }

        public SystemConfig GetById(int Id)
        {
            if (Id == 0)
            {
                return null;
            }
            return _systemConfigRepository.GetSingleById(Id);
        }

        public IEnumerable<SystemConfig> GetAll()
        {
            return _systemConfigRepository.GetAll();
        }
    }
}