﻿
using sTour.Data.Repositories;
using sTour.Entity;
using System.Collections.Generic;
using System;
namespace sTour.Service
{
    public interface IDistrictService
    {
        void Insert(District item);

        void Update(District item);

        void Delete(int Id);

        District GetById(int Id);

        IEnumerable<District> GetDistrict(int page, int pageSize,string keyword,out int total);

        int Count();

        IEnumerable<District> GetAll();
    }
    public class DistrictService : IDistrictService
    {
        private readonly IDistrictRepository _districtRepository;

        public DistrictService(IDistrictRepository districtRepository)
        {
            this._districtRepository = districtRepository;
        }

        public void Insert(District item)
        {
            if (item == null)
                throw new ArgumentNullException("District");
            _districtRepository.Insert(item);
        }

        public void Update(District item)
        {
            if (item == null)
                throw new ArgumentNullException("District");
            _districtRepository.Update(item);
        }

        public void Delete(int Id)
        {
            if(Id > 0)
            {
                var item = _districtRepository.GetSingleById(Id);
                _districtRepository.Delete(item);
            }
        }

        public District GetById(int Id)
        {
            return _districtRepository.GetSingleById(Id);
        }

        public IEnumerable<District> GetAll()
        {
            return _districtRepository.GetAll();
        }

        public int Count()
        {
            return _districtRepository.Count();
        }

        public IEnumerable<District> GetDistrict(int page, int pageSize, string keyword, out int total)
        {
            return _districtRepository.GetDistrict(page, pageSize,keyword,out total);
        }
    }
}