﻿using sTour.Data.Repositories;
using sTour.Entity;
using System;
using System.Collections.Generic;

namespace sTour.Service
{
    public interface IServiceHotelService
    {
        void Insert(ServiceHotel item);

        void Update(ServiceHotel item);

        void Delete(int Id);

        ServiceHotel GetById(int Id);

        IEnumerable<ServiceHotel> GetAll();

        IEnumerable<ServiceHotel> GetServiceHotels(int page, int pageSize, string keyword, out int total);

        IEnumerable<ServiceHotel> GetByHotelId(int hotelId);
    }

    public class ServiceHotelService : IServiceHotelService
    {
        private readonly IServiceHotelRepository _servicehotelRepository;

        public ServiceHotelService(IServiceHotelRepository servicehotelRepository)
        {
            this._servicehotelRepository = servicehotelRepository;
        }

        public void Insert(ServiceHotel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Insert ServiceHotel");
            }
            _servicehotelRepository.Insert(item);
        }

        public void Update(ServiceHotel item)
        {
            _servicehotelRepository.Update(item);
        }

        public void Delete(int Id)
        {
            var item = _servicehotelRepository.GetSingleById(Id);
            if (item != null)
            {
                _servicehotelRepository.Delete(item);
            }
        }

        public ServiceHotel GetById(int Id)
        {
            return _servicehotelRepository.GetSingleById(Id);
        }

        public IEnumerable<ServiceHotel> GetAll()
        {
            return _servicehotelRepository.GetAll();
        }

        public IEnumerable<ServiceHotel> GetServiceHotels(int page, int pageSize, string keyword, out int total)
        {
            return _servicehotelRepository.GetServiceHotels(page, pageSize, keyword, out total);
        }

        public IEnumerable<ServiceHotel> GetByHotelId(int hotelId)
        {
            return _servicehotelRepository.GetByHotelId(hotelId);
        }
    }
}