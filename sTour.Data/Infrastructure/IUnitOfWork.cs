﻿namespace sTour.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}