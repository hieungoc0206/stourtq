﻿namespace sTour.Data.Infrastructure
{
    public class DbFactory : Disposeable, IDbFactory
    {
        private STourContext dbContext;

        public STourContext Init()
        {
            return dbContext ?? (dbContext = new STourContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }
    }
}