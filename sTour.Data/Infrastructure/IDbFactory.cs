﻿using System;

namespace sTour.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        STourContext Init();
    }
}