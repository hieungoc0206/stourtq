﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sTour.Data.Model
{
    public class SearchOptionHome
    {
        public int Page { set; get; }
        public int PageSize { set; get; }
        public string KeyWord { set; get; }
    }
}
