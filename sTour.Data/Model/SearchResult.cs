﻿namespace sTour.Data.Model
{
    public class SearchResult
    {
        public int ObjectId { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public string Category { get; set; }
    }
}