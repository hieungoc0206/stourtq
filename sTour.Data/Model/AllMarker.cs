﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sTour.Data.Model
{
    public class AllMarker
    {
        public string Name { set; get; }
        public string Url { set; get; }
        public double Lat { set; get; }
        public double Lon { set; get; }
        public string Description { set; get; }
        public int? Star { set; get; }
    }
}
