namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv12 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Restaurants", "ShortDes", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Restaurants", "ShortDes", c => c.String(maxLength: 300));
        }
    }
}
