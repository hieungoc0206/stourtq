namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Location", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.Location", "ShortDesEn", c => c.String());
            AddColumn("dbo.Location", "LongDesEn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "LongDesEn");
            DropColumn("dbo.Location", "ShortDesEn");
            DropColumn("dbo.Location", "NameEn");
        }
    }
}
