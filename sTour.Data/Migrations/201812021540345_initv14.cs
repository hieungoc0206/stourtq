namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Culturals", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.Culturals", "AddressEn", c => c.String(maxLength: 500));
            AddColumn("dbo.Culturals", "ShortDesEn", c => c.String());
            AddColumn("dbo.Culturals", "LongDesEn", c => c.String());
            AddColumn("dbo.Foods", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.Foods", "AddressEn", c => c.String(maxLength: 500));
            AddColumn("dbo.Foods", "ShortDesEn", c => c.String());
            AddColumn("dbo.Foods", "LongDesEn", c => c.String());
            AddColumn("dbo.Hotel", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.Hotel", "AddressEn", c => c.String(maxLength: 500));
            AddColumn("dbo.Hotel", "ShortDesEn", c => c.String());
            AddColumn("dbo.Hotel", "LongDesEn", c => c.String());
            AddColumn("dbo.Location", "AddressEn", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "AddressEn");
            DropColumn("dbo.Hotel", "LongDesEn");
            DropColumn("dbo.Hotel", "ShortDesEn");
            DropColumn("dbo.Hotel", "AddressEn");
            DropColumn("dbo.Hotel", "NameEn");
            DropColumn("dbo.Foods", "LongDesEn");
            DropColumn("dbo.Foods", "ShortDesEn");
            DropColumn("dbo.Foods", "AddressEn");
            DropColumn("dbo.Foods", "NameEn");
            DropColumn("dbo.Culturals", "LongDesEn");
            DropColumn("dbo.Culturals", "ShortDesEn");
            DropColumn("dbo.Culturals", "AddressEn");
            DropColumn("dbo.Culturals", "NameEn");
        }
    }
}
