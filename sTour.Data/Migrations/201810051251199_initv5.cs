namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HotelComments",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Content = c.String(),
                        HotelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Hotel", t => t.HotelId, cascadeDelete: true)
                .Index(t => t.HotelId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelComments", "HotelId", "dbo.Hotel");
            DropIndex("dbo.HotelComments", new[] { "HotelId" });
            DropTable("dbo.HotelComments");
        }
    }
}
