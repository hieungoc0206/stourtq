namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Culturals", "AliasEn", c => c.String(maxLength: 256));
            AddColumn("dbo.Foods", "AliasEn", c => c.String(maxLength: 256));
            AddColumn("dbo.Hotel", "AliasEn", c => c.String(maxLength: 256));
            AddColumn("dbo.Location", "AliasEn", c => c.String(maxLength: 256));
            AddColumn("dbo.Restaurants", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.Restaurants", "AliasEn", c => c.String(maxLength: 256));
            AddColumn("dbo.Restaurants", "AddressEn", c => c.String(maxLength: 500));
            AddColumn("dbo.Restaurants", "ShortDesEn", c => c.String());
            AddColumn("dbo.Restaurants", "LongDesEn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Restaurants", "LongDesEn");
            DropColumn("dbo.Restaurants", "ShortDesEn");
            DropColumn("dbo.Restaurants", "AddressEn");
            DropColumn("dbo.Restaurants", "AliasEn");
            DropColumn("dbo.Restaurants", "NameEn");
            DropColumn("dbo.Location", "AliasEn");
            DropColumn("dbo.Hotel", "AliasEn");
            DropColumn("dbo.Foods", "AliasEn");
            DropColumn("dbo.Culturals", "AliasEn");
        }
    }
}
