namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Foods", "Alias", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.Hotel", "Alias", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.Location", "Alias", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.Restaurants", "Alias", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Restaurants", "Alias");
            DropColumn("dbo.Location", "Alias");
            DropColumn("dbo.Hotel", "Alias");
            DropColumn("dbo.Foods", "Alias");
        }
    }
}
