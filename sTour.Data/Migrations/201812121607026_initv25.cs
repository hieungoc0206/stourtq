namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv25 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Culturals", "AudioEn", c => c.String());
            AddColumn("dbo.Location", "AudioEn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "AudioEn");
            DropColumn("dbo.Culturals", "AudioEn");
        }
    }
}
