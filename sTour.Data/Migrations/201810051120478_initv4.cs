namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Culturals", "Alias", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Culturals", "Alias");
        }
    }
}
