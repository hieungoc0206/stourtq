namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.POI", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.POI", "Url");
        }
    }
}
