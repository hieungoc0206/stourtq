namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TouristServices", "HotLine", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TouristServices", "HotLine");
        }
    }
}
