namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TouristServices", "Avatar", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TouristServices", "Avatar");
        }
    }
}
