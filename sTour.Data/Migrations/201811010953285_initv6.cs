namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.POI", "CategoryId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.POI", "CategoryId");
        }
    }
}
