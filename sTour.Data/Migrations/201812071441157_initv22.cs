namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv22 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceHotel", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.ServiceHotel", "DescriptionEn", c => c.String(maxLength: 500));
            AddColumn("dbo.ServiceRestaurant", "NameEn", c => c.String(maxLength: 300));
            AddColumn("dbo.ServiceRestaurant", "DescriptionEn", c => c.String(maxLength: 500));
            AddColumn("dbo.TouristServices", "NameEn", c => c.String());
            AddColumn("dbo.TouristServices", "AddressEn", c => c.String());
            AddColumn("dbo.TouristServices", "ContentEn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TouristServices", "ContentEn");
            DropColumn("dbo.TouristServices", "AddressEn");
            DropColumn("dbo.TouristServices", "NameEn");
            DropColumn("dbo.ServiceRestaurant", "DescriptionEn");
            DropColumn("dbo.ServiceRestaurant", "NameEn");
            DropColumn("dbo.ServiceHotel", "DescriptionEn");
            DropColumn("dbo.ServiceHotel", "NameEn");
        }
    }
}
