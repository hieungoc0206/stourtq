namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv27 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Culturals", "EnterpriseId", c => c.Int());
            //AddColumn("dbo.Foods", "EnterpriseId", c => c.Int());
            //AddColumn("dbo.Location", "EnterpriseId", c => c.Int());
            //CreateIndex("dbo.Culturals", "EnterpriseId");
            //CreateIndex("dbo.Foods", "EnterpriseId");
            //CreateIndex("dbo.Location", "EnterpriseId");
            //AddForeignKey("dbo.Culturals", "EnterpriseId", "dbo.Enterprise", "Id");
            //AddForeignKey("dbo.Foods", "EnterpriseId", "dbo.Enterprise", "Id");
            //AddForeignKey("dbo.Location", "EnterpriseId", "dbo.Enterprise", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.Location", "EnterpriseId", "dbo.Enterprise");
            //DropForeignKey("dbo.Foods", "EnterpriseId", "dbo.Enterprise");
            //DropForeignKey("dbo.Culturals", "EnterpriseId", "dbo.Enterprise");
            //DropIndex("dbo.Location", new[] { "EnterpriseId" });
            //DropIndex("dbo.Foods", new[] { "EnterpriseId" });
            //DropIndex("dbo.Culturals", new[] { "EnterpriseId" });
            //DropColumn("dbo.Location", "EnterpriseId");
            //DropColumn("dbo.Foods", "EnterpriseId");
            //DropColumn("dbo.Culturals", "EnterpriseId");
        }
    }
}
