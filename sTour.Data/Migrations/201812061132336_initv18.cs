namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv18 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Foods", "AliasEn");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Foods", "AliasEn", c => c.String(maxLength: 256));
        }
    }
}
