namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Foods", "AliasEn", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Foods", "AliasEn");
        }
    }
}
