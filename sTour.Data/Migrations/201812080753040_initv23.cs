namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv23 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryLocation", "NameEn", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CategoryLocation", "NameEn");
        }
    }
}
