namespace sTour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Foods", "Star");
            DropColumn("dbo.Location", "Star");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Location", "Star", c => c.Int());
            AddColumn("dbo.Foods", "Star", c => c.Int());
        }
    }
}
