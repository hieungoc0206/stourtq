﻿using sTour.Data.Infrastructure;
using sTour.Entity;

namespace sTour.Data.Repositories
{
    public interface IServiceRestaurant_Restaurant_MappingRepository : IRepository<ServiceRestaurant_Mapping_Restaurant>
    {
    }

    public class ServiceRestaurant_Restaurant_MappingRepository : RepositoryBase<ServiceRestaurant_Mapping_Restaurant>, IServiceRestaurant_Restaurant_MappingRepository
    {
        public ServiceRestaurant_Restaurant_MappingRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}