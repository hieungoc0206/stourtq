﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
namespace sTour.Data.Repositories
{
    public interface IRole_Permisstion_MappingRepository : IRepository<Role_Permisstion_Mapping>
    {
        IEnumerable<Role_Permisstion_Mapping> GetByRoleId(string roleId);

        Role_Permisstion_Mapping GetByKey(string roleId, int permisstionId);
    }

    public class Role_Permisstion_MappingRepository : RepositoryBase<Role_Permisstion_Mapping>, IRole_Permisstion_MappingRepository
    {
        public Role_Permisstion_MappingRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Role_Permisstion_Mapping> GetByRoleId(string roleId)
        {
            return this.DbContext.Role_Permisstion_Mappings.Where(x => x.RoleId == roleId);
        }

        public Role_Permisstion_Mapping GetByKey(string roleId, int permisstionId)
        {
            return this.DbContext.Role_Permisstion_Mappings.Where(x => x.RoleId == roleId && x.PermissionId == permisstionId).SingleOrDefault();
        }
    }
}