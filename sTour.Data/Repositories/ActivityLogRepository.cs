﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Linq;
using System.Collections.Generic;
namespace sTour.Data.Repositories
{
    public interface IActivityLogRepository : IRepository<ActivityLog>
    {
        int Count();

        IEnumerable<ActivityLog> GetActivityLog(int page, int pageSize);
    }

    public class ActivityLogRepository : RepositoryBase<ActivityLog>, IActivityLogRepository
    {
        public ActivityLogRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.ActivityLogs.Count();
        }

        public IEnumerable<ActivityLog> GetActivityLog(int page, int pageSize)
        {
            return this.DbContext.ActivityLogs.OrderBy(x => x.CreatedDate).Skip(page * pageSize).Take(pageSize);
        }
    }
}