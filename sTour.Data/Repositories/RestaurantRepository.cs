﻿using sTour.Data.Infrastructure;
using sTour.Data.Model;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        IEnumerable<Restaurant> GetByLocationId(int locationId);

        IEnumerable<Restaurant> GetRestaurants(int? enterpriseId, int page, int pageSize, string keyword, out int total);

        IEnumerable<Restaurant> GetRestaurantPagings(int page, int pageSize, string keyword, string sort, int fromprice, int toprice, List<int> starts, List<int> services, out int total);

        IEnumerable<Restaurant> GetRestaurantsBySearch(SearchOptionHome option, out int total);

        IEnumerable<Restaurant> GetRestaurantInHome();
    }

    public class RestaurantRepository : RepositoryBase<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Restaurant> GetByLocationId(int locationId)
        {
            var restaurants = from restaurant in this.DbContext.Restaurants
                              join restaurantt_location in this.DbContext.Restaurant_Mapping_Locations
                              on restaurant.Id equals restaurantt_location.LocationId
                              where restaurantt_location.LocationId == locationId
                              select restaurant;
            return restaurants;
        }

        public IEnumerable<Restaurant> GetRestaurants(int? enterpriseId, int page, int pageSize, string keyword, out int total)
        {
            total = (from restaurant in DbContext.Restaurants where ((string.IsNullOrEmpty(keyword) || restaurant.Name.Contains(keyword)) && (!enterpriseId.HasValue || restaurant.EnterpriseId == enterpriseId)) select restaurant).Count();
            var restaurants = (from restaurant in DbContext.Restaurants where ((string.IsNullOrEmpty(keyword) || restaurant.Name.Contains(keyword)) && (!enterpriseId.HasValue || restaurant.EnterpriseId == enterpriseId)) select restaurant).OrderByDescending(c => c.Id).Skip((page - 1) * pageSize).Take(pageSize);
            foreach (var restaurant in restaurants)
            {
                if (restaurant.EnterpriseId.HasValue)
                {
                    restaurant.enterprise = DbContext.Enterprises.Where(e => e.Id == restaurant.EnterpriseId.Value).SingleOrDefault();
                }
            }

            return restaurants;
        }

        public IEnumerable<Restaurant> GetRestaurantsBySearch(SearchOptionHome option, out int total)
        {
            total = (from restaurant in DbContext.Restaurants where (string.IsNullOrEmpty(option.KeyWord) || restaurant.Name.Contains(option.KeyWord)) select restaurant).Count();
            return (from restaurant in DbContext.Restaurants where (string.IsNullOrEmpty(option.KeyWord) || restaurant.Name.Contains(option.KeyWord)) select restaurant).OrderBy(c => c.Id).Skip(option.Page * option.PageSize).Take(option.PageSize);
        }

        public IEnumerable<Restaurant> GetRestaurantInHome()
        {
            var restaurants = this.DbContext.Restaurants.Where(x => x.isActive).OrderByDescending(x => x.Star).Take(6).ToList();
            return restaurants;
        }

        public IEnumerable<Restaurant> GetRestaurantPagings(int page, int pageSize, string keyword, string sort, int fromprice, int toprice, List<int> starts, List<int> services, out int total)
        {
            var restaurants = (from restaurant in DbContext.Restaurants
                          where restaurant.isActive && (services.Count == 0 || services.All(s => (from serivce in DbContext.ServiceRestaurant_Mapping_Restaurants where serivce.RestaurantId == restaurant.Id select serivce.ServiceRestaurantId).ToList().Any(c => c == s)))
                          && ((string.IsNullOrEmpty(keyword) || restaurant.Name.Contains(keyword))
                          && (toprice == 0 ? toprice == 0 : restaurant.Price <= toprice)
                          && (fromprice == 0 ? fromprice == 0 : restaurant.Price >= fromprice)
                          && (starts.Count == 0 || starts.Any(y => y == restaurant.Star)))
                          select restaurant).Distinct();
            total = restaurants.Count();
            foreach (var h in restaurants)
            {
                h.Services = (from service in DbContext.ServiceRestaurants
                                  //join service_map in DbContext.ServiceHotel_Mapping_Hotels
                                  //on service.Id equals service_map.ServiceHotelId
                                  //where service_map.HotelId == h.Id
                                  //select service);
                              join service_map in DbContext.ServiceRestaurant_Mapping_Restaurants
                              on service.Id equals service_map.ServiceRestaurantId
                              where service_map.RestaurantId == h.Id
                              select service);

            }
            switch (sort)
            {
                case "alphabet":
                    restaurants= from data in restaurants
                                 orderby data.Name ascending
                                 select data;
                    break;
                case "hot":
                    restaurants = restaurants.OrderByDescending(x => x.ViewCount);
                    break;
                case "higher":
                    restaurants = restaurants.OrderBy(x => x.Price);
                    break;
                case "lower":
                    restaurants = restaurants.OrderByDescending(x => x.Price);
                    break;
                default:
                    restaurants = restaurants.OrderBy(x => x.Price);
                    break;
            }

            restaurants = restaurants.Skip((page - 1) * pageSize).Take(pageSize);

            return restaurants;
        }
    }
}