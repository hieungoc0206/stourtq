﻿using sTour.Data.Infrastructure;
using sTour.Entity;

namespace sTour.Data.Repositories
{
    public interface IHotel_Mapping_LocationRepository : IRepository<Hotel_Mapping_Location>
    {
    }

    public class Hotel_Mapping_LocationRepository : RepositoryBase<Hotel_Mapping_Location>, IHotel_Mapping_LocationRepository
    {
        public Hotel_Mapping_LocationRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}