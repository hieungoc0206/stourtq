﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Linq;
using System.Collections.Generic;

namespace sTour.Data.Repositories
{
    public interface ICategoryLocationRepository : IRepository<CategoryLocation>
    {

    }
    public class CategoryLocationRepository : RepositoryBase<CategoryLocation>, ICategoryLocationRepository
    {
        public CategoryLocationRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
