﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface ITouristServiceRepository : IRepository<TouristService>
    {
        IEnumerable<TouristService> GetTouristService(int page, int pageSize, string keyword, out int total);
    }

    public class TouristServiceRepository : RepositoryBase<TouristService>, ITouristServiceRepository
    {
        public TouristServiceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<TouristService> GetTouristService(int page, int pageSize, string keyword, out int total)
        {
            total = (from touristService in DbContext.TouristServices where (string.IsNullOrEmpty(keyword) || touristService.Name.Contains(keyword)) select touristService).Count();
            return (from touristService in DbContext.TouristServices where (string.IsNullOrEmpty(keyword) || touristService.Name.Contains(keyword)) select touristService).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}