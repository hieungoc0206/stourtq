﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
namespace sTour.Data.Repositories
{
    public interface IProvincialRepository : IRepository<Provincial>
    {
        int Count();

        IEnumerable<Provincial> GetProvincial(int page, int pageSize,string keyword,out int total);
    }

    public class ProvincialRepository : RepositoryBase<Provincial>, IProvincialRepository
    {
        public ProvincialRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Provincial> GetProvincial(int page, int pageSize,string keyword, out int total)
        {
            total = (from province in DbContext.Provincials where (string.IsNullOrEmpty(keyword) || province.Name.Contains(keyword)) select province).Count();
            return (from province in DbContext.Provincials where (string.IsNullOrEmpty(keyword) || province.Name.Contains(keyword)) select province).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }

        public int Count()
        {
            return this.DbContext.Provincials.Count();
        }
    }
}