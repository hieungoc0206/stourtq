﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IServiceRestaurantRepository : IRepository<ServiceRestaurant>
    {
        IEnumerable<ServiceRestaurant> GetServiceRestaurants(int page, int pageSize, string keyword, out int total);
        IEnumerable<ServiceRestaurant> GetByRestaurantId(int restaurantId);
    }

    public class ServiceRestaurantRepository : RepositoryBase<ServiceRestaurant>, IServiceRestaurantRepository
    {
        public ServiceRestaurantRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<ServiceRestaurant> GetByRestaurantId(int restaurantId)
        {
            var services = from service in this.DbContext.ServiceRestaurants
                           join restaurant_service in this.DbContext.ServiceRestaurant_Mapping_Restaurants
                           on service.Id equals restaurant_service.ServiceRestaurantId
                           where restaurant_service.RestaurantId == restaurantId
                           select service;
            return services;
        }
        public IEnumerable<ServiceRestaurant> GetServiceRestaurants(int page, int pageSize, string keyword, out int total)
        {
            total = (from servicerestaurant in DbContext.ServiceRestaurants where (string.IsNullOrEmpty(keyword) || servicerestaurant.Name.Contains(keyword)) select servicerestaurant).Count();
            return (from servicerestaurant in DbContext.ServiceRestaurants where (string.IsNullOrEmpty(keyword) || servicerestaurant.Name.Contains(keyword)) select servicerestaurant).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}