﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
using System;

namespace sTour.Data.Repositories
{
    public interface IServiceHotelRepository : IRepository<ServiceHotel>
    {
        IEnumerable<ServiceHotel> GetServiceHotels(int page, int pageSize, string keyword, out int total);
        IEnumerable<ServiceHotel> GetByHotelId(int hotelId);
    }

    public class ServiceHotelRepository : RepositoryBase<ServiceHotel>, IServiceHotelRepository
    {
        public ServiceHotelRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ServiceHotel> GetByHotelId(int hotelId)
        {
            var services = from service in this.DbContext.ServiceHotels
                            join hotel_service in this.DbContext.ServiceHotel_Mapping_Hotels
                            on service.Id equals hotel_service.ServiceHotelId
                            where hotel_service.HotelId == hotelId
                            select service;
            return services;
        }

        public IEnumerable<ServiceHotel> GetServiceHotels(int page, int pageSize, string keyword, out int total)
        {
            total = (from servicehotel in DbContext.ServiceHotels where (string.IsNullOrEmpty(keyword) || servicehotel.Name.Contains(keyword)) select servicehotel).Count();
            return (from servicehotel in DbContext.ServiceHotels where (string.IsNullOrEmpty(keyword) || servicehotel.Name.Contains(keyword)) select servicehotel).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}