﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
namespace sTour.Data.Repositories
{
    public interface IPermissionRepository : IRepository<Permisstion>
    {
        IEnumerable<Permisstion> GetListPermissionByRoleId(string RoleId);
    }

    public class PermissionRepository : RepositoryBase<Permisstion>, IPermissionRepository
    {
        public PermissionRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<Permisstion> GetListPermissionByRoleId(string RoleId)
        {
            var query = from p in DbContext.Permisstions
                        join rp in DbContext.Role_Permisstion_Mappings
                        on p.Id equals rp.PermissionId
                        where rp.RoleId == RoleId
                        select p ;
            return query;
        }
    }
}