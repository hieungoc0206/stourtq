﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Linq;
using System.Collections.Generic;

namespace sTour.Data.Repositories
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {

    }
    public class EnterpriseRepository : RepositoryBase<Enterprise>, IEnterpriseRepository
    {
        public EnterpriseRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
