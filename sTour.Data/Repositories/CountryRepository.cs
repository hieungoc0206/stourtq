﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
namespace sTour.Data.Repositories
{
    public interface ICountryRepository : IRepository<Country>
    {
        int Count();

        IEnumerable<Country> GetCountry(int page, int pageSize, string keyword, out int total);
    }

    public class CountryRepository : RepositoryBase<Country>, ICountryRepository
    {
        public CountryRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.Countries.Count();
        }

        public IEnumerable<Country> GetCountry(int page, int pageSize, string keyword, out int total)
        {
            total = (from country in DbContext.Countries where (string.IsNullOrEmpty(keyword) || country.Name.Contains(keyword)) select country).Count();
            return (from country in DbContext.Countries where (string.IsNullOrEmpty(keyword) || country.Name.Contains(keyword)) select country).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}