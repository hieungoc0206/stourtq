﻿using sTour.Data.Infrastructure;
using sTour.Entity;

namespace sTour.Data.Repositories
{
    public interface IServiceHotel_Hotel_MappingRepository : IRepository<ServiceHotel_Mapping_Hotel>
    {
    }

    public class ServiceHotel_Hotel_MappingRepository : RepositoryBase<ServiceHotel_Mapping_Hotel>, IServiceHotel_Hotel_MappingRepository
    {
        public ServiceHotel_Hotel_MappingRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}