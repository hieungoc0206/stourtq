﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface ICulturalRepository : IRepository<Cultural>
    {
        IEnumerable<Cultural> GetCulturals(int page, int pageSize, string keyword, out int total);
    }

    public class CulturalRepository : RepositoryBase<Cultural>, ICulturalRepository
    {
        public CulturalRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Cultural> GetCulturals(int page, int pageSize, string keyword, out int total)
        {
            total = (from cultural in DbContext.Culturals where (string.IsNullOrEmpty(keyword) || cultural.Name.Contains(keyword)) select cultural).Count();
            return (from cultural in DbContext.Culturals where (string.IsNullOrEmpty(keyword) || cultural.Name.Contains(keyword)) select cultural).OrderBy(c => c.Name).Skip((page - 1) * pageSize).Take(pageSize);//
        }
    }
}