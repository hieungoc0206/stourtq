﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IDistrictRepository : IRepository<District>
    {
        int Count();

        IEnumerable<District> GetDistrict(int page, int pageSize, string keyword, out int total);
    }

    public class DistrictRepository : RepositoryBase<District>, IDistrictRepository
    {
        public DistrictRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.Districts.Count();
        }

        public IEnumerable<District> GetDistrict(int page, int pageSize, string keyword, out int total)
        {
            total = (from district in DbContext.Districts where (string.IsNullOrEmpty(keyword) || district.Name.Contains(keyword)) select district).Count();
            return (from district in DbContext.Districts where (string.IsNullOrEmpty(keyword) || district.Name.Contains(keyword)) select district).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}