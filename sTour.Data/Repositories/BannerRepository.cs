﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IBannerRepository : IRepository<Banner>
    {
        int Count();

        IEnumerable<Banner> GetBanners(int page, int pageSize, string keyword, out int total);
    }

    public class BannerRepository : RepositoryBase<Banner>, IBannerRepository
    {
        public BannerRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.Banners.Count();
        }

        public IEnumerable<Banner> GetBanners(int page, int pageSize, string keyword, out int total)
        {
            total = (from banner in DbContext.Banners where (string.IsNullOrEmpty(keyword) || banner.Name.Contains(keyword) ) select banner).Count();
            return (from banner in DbContext.Banners where (string.IsNullOrEmpty(keyword) || banner.Name.Contains(keyword)) select banner).OrderBy(c => c.Id).Skip(page * pageSize).Take(pageSize);
        }
    }
}