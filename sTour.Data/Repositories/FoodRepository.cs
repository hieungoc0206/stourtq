﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IFoodRepository : IRepository<Foods>
    {
        IEnumerable<Foods> GetFoods(int page, int pageSize, string keyword, out int total);
    }

    public class FoodRepository : RepositoryBase<Foods>, IFoodRepository
    {
        public FoodRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Foods> GetFoods(int page, int pageSize, string keyword, out int total)
        {
            total = (from food in DbContext.Foods where (string.IsNullOrEmpty(keyword) || food.Name.Contains(keyword)) select food).Count();
            return (from food in DbContext.Foods where (string.IsNullOrEmpty(keyword) || food.Name.Contains(keyword)) select food).OrderBy(c => c.Name).Skip((page - 1) * pageSize).Take(pageSize);//
        }
    }
}