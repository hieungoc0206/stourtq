﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;

namespace sTour.Data.Repositories
{
    public interface IExceptionLogRepository : IRepository<ExceptionLog>
    {
        int Count();

        IEnumerable<ExceptionLog> GetExceptionLog(int page, int pageSize);
    }

    public class ExceptionLogRepository : RepositoryBase<ExceptionLog>, IExceptionLogRepository
    {
        public ExceptionLogRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.ExceptionLogs.Count();
        }

        public IEnumerable<ExceptionLog> GetExceptionLog(int page, int pageSize)
        {
            return this.DbContext.ExceptionLogs.OrderByDescending(x => x.LogTime).Skip(page * pageSize).Take(pageSize);
        }
    }
}