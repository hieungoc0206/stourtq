﻿using sTour.Data.Infrastructure;
using sTour.Entity;

namespace sTour.Data.Repositories
{
    public interface IRestaurant_Mapping_LocationRepository : IRepository<Restaurant_Mapping_Location>
    {
    }

    public class Restaurant_Mapping_LocationRepository : RepositoryBase<Restaurant_Mapping_Location>, IRestaurant_Mapping_LocationRepository
    {
        public Restaurant_Mapping_LocationRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}