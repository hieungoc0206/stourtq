﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
namespace sTour.Data.Repositories
{
    public interface ILocationRepository : IRepository<Location>
    {
        IEnumerable<Location> GetByCategory(int categoryId);
        IEnumerable<Location> GetLocations(int page, int pageSize, string keyword, out int total);
        void UpdateLatLon(int locationId, double lat, double lon);
        IEnumerable<Location> GetByHotelId(int hotelId);
        IEnumerable<Location> GetByRestaurantId(int restaurantId);
        IEnumerable<Location> GetLocationByNumber(int number);
        IEnumerable<Location> GetLocationInHome();
    }
    public class LocationRepository : RepositoryBase<Location>, ILocationRepository
    {
        public LocationRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<Location> GetByCategory(int categoryId)
        {
            return this.DbContext.Locations.Where(x => x.LocationCategoryId == categoryId);
        }

        public IEnumerable<Location> GetLocations(int page, int pageSize, string keyword, out int total)
        {
            var locations = (from location in this.DbContext.Locations where (String.IsNullOrEmpty(keyword) || location.Name.Contains(keyword)) && location.isActive select location);
            total = locations.Count();
            locations = locations.OrderBy(x => x.Name).Skip((page - 1) * pageSize).Take(pageSize);
            foreach(var location in locations)
            {
                if(location.LocationCategoryId.HasValue)
                {
                    location.categoryLocation = DbContext.CategoryLocations.Where(c => c.Id == location.LocationCategoryId.Value).SingleOrDefault();
                }
            }
            return locations;

        }

        public void UpdateLatLon(int locationId, double lat, double lon)
        {
            var paramLat = new SqlParameter { ParameterName = "@Lat", Value = lat };
            var paramLon = new SqlParameter { ParameterName = "@Lon", Value = lon };
            var paramLocationId = new SqlParameter { ParameterName = "@LocationId", Value = locationId };

            this.DbContext.Database.ExecuteSqlCommand("UpdateLatLon @LocationId,@Lat, @Lon", paramLocationId, paramLat, paramLon);
        }

        public IEnumerable<Location> GetByHotelId(int hotelId)
        {
            var locations = from location in this.DbContext.Locations
                         join hotel_location in this.DbContext.Hotel_Mapping_Locations
                         on location.Id equals hotel_location.LocationId
                         where hotel_location.HotelId == hotelId
                         select location;
            return locations;
        }

        public IEnumerable<Location> GetByRestaurantId(int restaurantId)
        {
            var locations = from location in this.DbContext.Locations
                            join restaurant_location in this.DbContext.Restaurant_Mapping_Locations
                            on location.Id equals restaurant_location.LocationId
                            where restaurant_location.RestaurantId == restaurantId
                            select location;
            return locations;
        }

        public IEnumerable<Location> GetLocationByNumber(int number)
        {
            var locations = this.DbContext.Locations.Take(number).ToList();
            return locations;
        }

        public IEnumerable<Location> GetLocationInHome()
        {
            var locations = this.DbContext.Locations.Where(x=>x.isActive).Take(6).ToList();
            return locations;
        }
    }
}
