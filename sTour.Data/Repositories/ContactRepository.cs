﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Collections.Generic;
using System.Linq;
namespace sTour.Data.Repositories
{
    public interface IContactRepository : IRepository<Contact>
    {
        int Count();
        IEnumerable<Contact> GetContact(int page, int pageSize);
    }

    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        public ContactRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int Count()
        {
            return this.DbContext.Contacts.Count();
        }

        public IEnumerable<Contact> GetContact(int page, int pageSize)
        {
            return this.DbContext.Contacts.OrderBy(x => x.CreatedDate).Skip(page * pageSize).Take(pageSize);
        }
    }
}