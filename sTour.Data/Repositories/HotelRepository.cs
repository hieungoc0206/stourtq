﻿using sTour.Data.Infrastructure;
using sTour.Entity;
using System.Linq;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using sTour.Data.Model;
using System.Data.SqlClient;
using System.Data;

namespace sTour.Data.Repositories
{
    public interface IHotelRepository : IRepository<Hotel>
    {
        IEnumerable<Hotel> GetByLocationId(int locationId);
        IEnumerable<Hotel> GetHotels(int? enterpriseId, int page, int pageSize, string keyword, out int total);
        IEnumerable<Hotel> GetHotelPagings(int page, int pageSize, string keyword, string sort,int fromprice,int toprice,List<int> starts, List<int> services, out int total);
        IEnumerable<Hotel> GetHotelInHome();
        Hotel GetById(int Id);
        IEnumerable<SearchResult> SearchQuery(string q);
    }
    public class HotelRepository : RepositoryBase<Hotel>, IHotelRepository
    {
        public HotelRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<Hotel> GetByLocationId(int locationId)
        {
            var hotels = from hotel in this.DbContext.Hotels
                         join hotel_location in this.DbContext.Hotel_Mapping_Locations
                         on hotel.Id equals hotel_location.HotelId
                         where hotel_location.LocationId == locationId
                         select hotel;
            return hotels;
        }

        public IEnumerable<Hotel> GetHotels(int? enterpriseId, int page, int pageSize, string keyword, out int total)
        {
            total = (from hotel in DbContext.Hotels where ((string.IsNullOrEmpty(keyword) || hotel.Name.Contains(keyword)) && (!enterpriseId.HasValue || hotel.EnterpriseId == enterpriseId)) select hotel).Count();
            var hotels = (from hotel in DbContext.Hotels where ((string.IsNullOrEmpty(keyword) || hotel.Name.Contains(keyword)) && (!enterpriseId.HasValue || hotel.EnterpriseId == enterpriseId)) select hotel).OrderByDescending(c => c.Id).Skip((page-1) * pageSize).Take(pageSize);
            foreach (var hotel in hotels)
            {
                if (hotel.EnterpriseId.HasValue)
                {
                    hotel.enterprise = DbContext.Enterprises.Where(e => e.Id == hotel.EnterpriseId.Value).SingleOrDefault();
                }
            }

            return hotels;
        }

        public IEnumerable<Hotel> GetHotelInHome()
        {
            var hotels = this.DbContext.Hotels.Where(x => x.isActive).OrderByDescending(x => x.Star).Take(6).ToList();
            return hotels;
        }

        public IEnumerable<Hotel> GetHotelPagings(int page, int pageSize, string keyword, string sort, int fromprice,int toprice, List<int> starts, List<int> services, out int total)
        {
            
            var hotels = (from hotel in DbContext.Hotels
                     where hotel.isActive && (services.Count == 0 ||  services.All(s=> (from serivce in DbContext.ServiceHotel_Mapping_Hotels where serivce.HotelId == hotel.Id select serivce.ServiceHotelId).ToList().Any(c=>c ==s)))
                     && ((string.IsNullOrEmpty(keyword) || hotel.Name.Contains(keyword)) 
                     && (toprice == 0 ? toprice == 0 : hotel.Price <= toprice)
                     && (fromprice == 0 ? fromprice == 0 : hotel.Price >= fromprice)
                     && (starts.Count == 0 || starts.Any(y => y == hotel.Star)))
                     select hotel).Distinct();
            total = hotels.Count();
            foreach(var h in hotels)
            {
                h.Services = (from service in DbContext.ServiceHotels
                              join service_map in DbContext.ServiceHotel_Mapping_Hotels
                              on service.Id equals service_map.ServiceHotelId
                              where service_map.HotelId == h.Id
                              select service);
               
            }
            switch (sort)
            {
                case "hot":
                    hotels = hotels.OrderByDescending(x => x.ViewCount);
                    break;
                case "higher":
                    hotels = hotels.OrderBy(x => x.Price);
                    break;
                case "alphabet":
                    hotels = from data in hotels
                             orderby data.Name ascending
                             select data;
                    break;
                case "lower":
                    hotels = hotels.OrderByDescending(x => x.Price);
                    break;
                default:
                    hotels = hotels.OrderBy(x => x.Price);
                    break;
                
            }

            hotels = hotels.Skip((page-1) * pageSize).Take(pageSize);

            return hotels;
        }

        public Hotel GetById(int Id)
        {
            var hotel = DbContext.Hotels.SingleOrDefault(x => x.Id.Equals(Id));
            hotel.Services = (from service in DbContext.ServiceHotels
                          join service_map in DbContext.ServiceHotel_Mapping_Hotels
                          on service.Id equals service_map.ServiceHotelId
                          where service_map.HotelId == hotel.Id
                          select service);
            return hotel;
        }

        public IEnumerable<SearchResult> SearchQuery(string q)
        {
            var query = new SqlParameter { ParameterName = "@q", Value = (object)q ?? DBNull.Value };
            var result = DbContext.Database.SqlQuery<SearchResult>("USP_SearchQuery @q", query).ToList();
            return result;
        }
    }
}
