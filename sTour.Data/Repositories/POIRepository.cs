﻿using sTour.Data.Infrastructure;
using sTour.Entity;

namespace sTour.Data.Repositories
{
    public interface IPOIRepository : IRepository<POI>
    {
    }

    public class POIRepository : RepositoryBase<POI>, IPOIRepository
    {
        public POIRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}