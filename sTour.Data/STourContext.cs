﻿using Microsoft.AspNet.Identity.EntityFramework;
using sTour.Entity;
using System.Data.Entity;

namespace sTour.Data
{
    public class STourContext : IdentityDbContext<ApplicationUser>
    {
        public STourContext() : base("name=sTourContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }

        public virtual DbSet<SystemConfig> SystemConfigs { get; set; }

        public virtual DbSet<Contact> Contacts { get; set; }

        public virtual DbSet<Country> Countries { get; set; }

        public virtual DbSet<District> Districts { get; set; }

        public virtual DbSet<Provincial> Provincials { get; set; }

        public virtual DbSet<ExceptionLog> ExceptionLogs { get; set; }

        public virtual DbSet<Permisstion> Permisstions { get; set; }

        public virtual DbSet<Role_Permisstion_Mapping> Role_Permisstion_Mappings { get; set; }

        public virtual DbSet<CategoryLocation> CategoryLocations { get; set; }

        public virtual DbSet<Location> Locations { get; set; }

        public virtual DbSet<Enterprise> Enterprises { get; set; }

        public virtual DbSet<Hotel> Hotels { get; set; }

        public virtual DbSet<HotelComment> HotelComments { get; set; }

        public virtual DbSet<Restaurant> Restaurants { get; set; }

        public virtual DbSet<Restaurant_Mapping_Location> Restaurant_Mapping_Locations { get; set; }

        public virtual DbSet<Hotel_Mapping_Location> Hotel_Mapping_Locations { get; set; }

        public virtual DbSet<Banner> Banners { get; set; }

        public virtual DbSet<Foods> Foods { get; set; }

        public virtual DbSet<Cultural> Culturals { get; set; }

        public virtual DbSet<ServiceHotel> ServiceHotels { get; set; }

        public virtual DbSet<ServiceRestaurant> ServiceRestaurants { get; set; }

        public virtual DbSet<ServiceRestaurant_Mapping_Restaurant> ServiceRestaurant_Mapping_Restaurants { get; set; }

        public virtual DbSet<ServiceHotel_Mapping_Hotel> ServiceHotel_Mapping_Hotels { get; set; }

        public virtual DbSet<POI> POIs { get; set; }

        public virtual DbSet<TouristService> TouristServices { get; set; }

        public static STourContext Create()
        {
            return new STourContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId });
            modelBuilder.Entity<IdentityUserLogin>().HasKey(i => i.UserId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
