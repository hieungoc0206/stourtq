$('#collapseMap').on('shown.bs.collapse', function (e) {
    (function (A) {

        if (!Array.prototype.forEach)
            A.forEach = A.forEach || function (action, that) {
                for (var i = 0, l = this.length; i < l; i++)
                    if (i in this)
                        action.call(that, this[i], i, this);
            };

    })(Array.prototype);

    var
        mapObject,
        markers = [],
        Lat = parseFloat($('#Lat').val()),
        Ln = parseFloat($('#Ln').val()),
        markersData = {
            'Hotels':
                {
                    name: $('#Name').val(),
                    location_latitude: Lat,
                    location_longitude: Ln,
                    map_image_url: $('#Avatar').val(),
                    name_point: $('#Name').val(),                  
                    get_directions_start_address: $('#Address').val(),                
                    phone: $('#HotLine').val()
                }

        };


    var mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(Lat, Ln),
        mapTypeId: google.maps.MapTypeId.ROADMAP ,

        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        scrollwheel: true,
        scaleControl: true,
        scaleControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },

    };
    var
        marker;
    mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(markersData['Hotels'].location_latitude, markersData['Hotels'].location_longitude),
        map: mapObject,
        icon: '/Assets/home/img/pins/Hotels.png',
    });
    google.maps.event.addListener(marker, 'click', (function () {
        closeInfoBox();
        getInfoBox(markersData['Hotels']).open(mapObject, this);
        mapObject.setCenter(new google.maps.LatLng(markersData['Hotels'].location_latitude, markersData['Hotels'].location_longitude));
    }));



    function hideAllMarkers() {     
          marker.setMap(null);
    };

    function closeInfoBox() {
        $('div.infoBox').remove();
    };

    function getInfoBox(item) {
        return new InfoBox({
            content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="' + item.map_image_url + '" alt="Image" style ="width:280px;height:140px"/>' +
                '<h3>' + item.name_point + '</h3>' +
                '<span>' + item.get_directions_start_address + '</span>' +
                '<div class="marker_tools">' +
                '<form action="https://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="' + item.get_directions_start_address + '" type="hidden"><input type="hidden" name="daddr" value="' + item.location_latitude + ',' + item.location_longitude + '"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
                '<a href="tel://' + item.phone + '" class="btn_infobox_phone">' + item.phone + '</a>' +
                '</div>' +
                '</div>',
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(10, 125),
            closeBoxMargin: '5px -20px 2px 2px',
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            isHidden: false,
            alignBottom: true,
            pane: 'floatPane',
            enableEventPropagation: true
        });


    };

});