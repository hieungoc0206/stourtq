﻿
var GetLang = function () {
    var ck_value = document.cookie;
    var ck_name = "_Culture";
    var ck_start = ck_value.indexOf(" " + ck_name + "=");
    if (ck_start == -1) {
        ck_start = ck_value.indexOf(ck_name + "=");
    }
    if (ck_start == -1) {
        ck_value = null;
    } else {
        ck_start = ck_value.indexOf("=", ck_start) + 1;
        var ck_end = ck_value.indexOf(";", ck_start);
        if (ck_end == -1) {
            ck_end = ck_value.length;
        }
        ck_value = unescape(ck_value.substring(ck_start, ck_end));
    }
    return ck_value;
}