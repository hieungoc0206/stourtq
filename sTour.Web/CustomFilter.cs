﻿using sTour.Entity;
using System;
using sTour.Data;
using System.Web.Mvc;

namespace sTour.Web
{
    public class ExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {

        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                ExceptionLog logger = new ExceptionLog()
                {
                    //ExceptionMessage = filterContext.Exception.Message.Length > 70 ? filterContext.Exception.Message.Substring(0,70) : filterContext.Exception.Message,
                    //ExceptionStackTrace = filterContext.Exception.StackTrace.Length > 150 ? filterContext.Exception.StackTrace.Substring(0,150) : filterContext.Exception.StackTrace,
                    ExceptionMessage = filterContext.Exception.Message,
                    ExceptionStackTrace = filterContext.Exception.StackTrace,
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    LogTime = DateTime.Now
                };

                STourContext db = new STourContext();
                db.ExceptionLogs.Add(logger);
                db.SaveChanges();
                filterContext.ExceptionHandled = true;
            }
        }
    }
}