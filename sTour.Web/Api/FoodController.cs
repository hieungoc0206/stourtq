﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/food")]
    [Authorize]
    public class FoodController : ApiControllerBase
    {
        private readonly IFoodService _foodService;
        private readonly IDistrictService _districtService;
        public FoodController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService, IFoodService foodService, IDistrictService districtService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._foodService = foodService;
            this._districtService = districtService;
        }

        [Route("create")]      
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerFood))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    FoodModel model = ConvertFormDataToModel(result.FormData);
                    var user = User.Identity;
                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Food/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Food/Avatar/" + fileName;
                    }

                    var food = new Foods();
                    food.Name = model.Name;
                    food.Alias = model.Alias;
                    food.Address = model.Address;
                    food.NameEn = model.NameEn;
                    food.AliasEn = model.AliasEn;
                    food.AddressEn = model.AddressEn;
                    food.Avatar = model.Avatar;
                    food.Latitude = model.Latitude;
                    food.Longitude = model.Longitude;
                    food.LongDes = model.LongDes;
                    food.ShortDes = model.ShortDes;
                    food.LongDesEn = model.LongDesEn;
                    food.ShortDesEn = model.ShortDesEn;
                    food.ViewCount = model.ViewCount;
                    food.VideoDir = model.VideoDir;
                    food.DistrictId = model.DistrictId;
                    food.CreatedAt = DateTime.Now;
                    food.isActive = true;
                    _foodService.Insert(food);
                    if (food.Id > 0)
                    {
                        //Save file
                        string foodDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Food/Food-{0}", food.Id)));
                        if (!System.IO.Directory.Exists(foodDirectory))
                        {
                            System.IO.Directory.CreateDirectory(foodDirectory);
                        }
                        var listFileAttach = new List<FileAttach>();
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Food/Food-{0}", food.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(foodDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttach.Add(fileAttach);
                        }

                        food.Images = JsonConvert.SerializeObject(listFileAttach);
                        _foodService.Update(food);
                    }

                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, false);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getfoods")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetFoods(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerFood))
            {
                HttpResponseMessage response = null;
                int totalRow;
               
                var userId = User.Identity.GetUserId();

                var foods = _foodService.GetFoods(page, pageSize, keyword, out totalRow).ToList();
                var model = Mapper.Map<IEnumerable<Foods>, IEnumerable<FoodModel>>(foods);
                foreach (var item in model)
                {

                    if (item.DistrictId != null)
                        item.DistrictName = _districtService.GetById(item.DistrictId.Value).Name;


                }
                PaginationSet<FoodModel> pagedSet = new PaginationSet<FoodModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request , int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerFood))
            {
                var food = _foodService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, food);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPost]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerFood))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    FoodModel model = ConvertFormDataToModel(result.FormData);
                    var food = _foodService.GetById(model.Id);
                    if (food != null)
                    {
                        if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                        {
                            model.Avatar = "/Assets/images_none.png";
                        }
                        else if (!model.Avatar.StartsWith("~/Content") && food.Avatar != model.Avatar)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Food/Avatar"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Avatar = "~/Content/Images/Food/Avatar/" + fileName;
                        }

                        food.Name = model.Name;
                        food.Alias = model.Alias;
                        food.Address = model.Address;
                        food.NameEn = model.NameEn;
                        food.AliasEn = model.AliasEn;
                        food.AddressEn = model.AddressEn;
                        food.Avatar = model.Avatar;
                        food.Latitude = model.Latitude;
                        food.Longitude = model.Longitude;
                        food.LongDes = model.LongDes;
                        food.ShortDes = model.ShortDes;
                        food.LongDesEn = model.LongDesEn;
                        food.ShortDesEn = model.ShortDesEn;
                        food.ViewCount = model.ViewCount;
                        food.DistrictId = model.DistrictId;                   
                        food.VideoDir = model.VideoDir;
                        food.isActive = model.isActive;
                        _foodService.Update(food);
                        string foodDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Food/Food-{0}", food.Id)));
                        if (!System.IO.Directory.Exists(foodDirectory))
                        {
                            System.IO.Directory.CreateDirectory(foodDirectory);
                        }
                        var listFileAttachNew = JsonConvert.DeserializeObject<List<FileAttach>>(model.Images);
                        var listFileAttachOld = JsonConvert.DeserializeObject<List<FileAttach>>(string.IsNullOrEmpty(food.Images) ? "[]" : food.Images);
                        foreach (var file in listFileAttachOld)
                        {
                            if (!listFileAttachNew.Any(f => f.FileName == file.FileName))
                            {
                                string path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Food/Food-{0}", food.Id) + "/" + file.FileName));
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                            }
                        }
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Food/Food-{0}", food.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(foodDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttachNew.Add(fileAttach);
                        }

                        food.Images = JsonConvert.SerializeObject(listFileAttachNew);
                        _foodService.Update(food);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }

        }

        FoodModel ConvertFormDataToModel(NameValueCollection formData)
        {
            var model = new FoodModel();
            if (!String.IsNullOrEmpty(formData["Id"]))
                model.Id = int.Parse(formData["Id"]);
            model.Address = formData["Address"];
            model.AddressEn = formData["AddressEn"];
            model.Avatar = formData["Avatar"];
            model.Images = formData["Images"];
            model.isActive = bool.Parse(formData["isActive"]);
            model.Latitude = double.Parse(formData["Latitude"]);
            model.LongDes = formData["LongDes"];
            model.LongDesEn = formData["LongDesEn"];
            model.Longitude = double.Parse(formData["Longitude"]);
            model.Name = formData["Name"];
            model.Alias = formData["Alias"];
            model.ShortDes = formData["ShortDes"];
            model.NameEn = formData["NameEn"];
            model.AliasEn = formData["AliasEn"];
            model.ShortDesEn = formData["ShortDesEn"];
            model.VideoDir = formData["VideoDir"];
            model.ViewCount = int.Parse(formData["ViewCount"]);
            if (formData["DistrictId"] != "null" && formData["DistrictId"] != "undefined")
                model.DistrictId = int.Parse(formData["DistrictId"]);            
            return model;
        }
    }
}
