﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sTour.Common;
using sTour.Data;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class ApplicationUserController : ApiControllerBase
    {
        protected ICountryService _countryService;
        public ApplicationUserController(
            ApplicationUserManager userManager,
            IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService,ICountryService countryService)
            : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._countryService = countryService;
        }
        #region Convert Entity<--->Model

        private void EntityToModel(Permisstion item, PermissionModel model)
        {
            model.Id = item.Id;
            model.Name = item.Name;
            model.Description = item.Description;
            model.Checked = false;
        }

        #endregion Convert Entity<--->Model
        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request, string keyword, int page, int pageSize)
        {
            bool result = await CheckPermission(SystemPermission.ManagerUsers);
            if (result) 
                return CreateHttpResponse(request, () =>
                {
                    var totalRow = _userManager.Users.Count();
                    page = page - 1;
                    var users = (from user in _userManager.Users where (string.IsNullOrEmpty(keyword) || user.UserName.Contains(keyword)) select user).OrderBy(x => x.FullName).Skip(page * pageSize).Take(pageSize).ToList();

                    foreach (var user in users)
                    {
                        if (user.CountryId != null)
                            user.Country = _countryService.GetById(user.CountryId.Value);
                    }
                    HttpResponseMessage response = null;
                    IEnumerable<UserModel> model = Mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserModel>>(users);
                    PaginationSet<UserModel> pagedSet = new PaginationSet<UserModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = model
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });
            else
            {
                return UnAuthorized();
            }
        }



        [Authorize]
        [Route("getcurrentuser")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetCurrentUser(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    ApplicationUser user = _userManager.FindById(User.Identity.GetUserId());
                    var model = Mapper.Map<ApplicationUser, UserModel>(user);
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Authorize]
        [Route("getallpermission")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAllPermission(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {

                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    ApplicationUser user = _userManager.FindById(User.Identity.GetUserId());
                    var permisstions = _permissionService.GetAll();
                    response = request.CreateResponse(HttpStatusCode.OK, permisstions);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getpermission")]
        [HttpGet]
        public HttpResponseMessage GetPermission(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var userId = User.Identity.GetUserId();
                var userPer = new UserPermisstionModel();
                var permisstions = _permissionService.GetAll();
                var permisstionModels = permisstions.Select(c =>
                {
                    var permisstionModel = new PermissionModel();
                    EntityToModel(c, permisstionModel);
                    return permisstionModel;
                });
                var user = _userManager.FindById(userId);
                if (user.LockoutEnabled)
                    return UnAuthorized();
                userPer.permisstions = permisstionModels.ToList();
                var dbcontext = new STourContext();
                SqlParameter UserId = new SqlParameter("@UserId", userId);
                List<UserPermission> query = dbcontext.Database.SqlQuery<UserPermission>("exec GetPermissionById @UserId", UserId).ToList();
                foreach (var per in userPer.permisstions)
                {
                    if (query.Any(x => x.PermisstionId == per.Id))
                    {
                        per.Checked = true;
                    }
                }
                response = request.CreateResponse(HttpStatusCode.OK, userPer);
                return response;
            });
        }


        [Route("udateprofile")]
        [HttpPut]
        public HttpResponseMessage UpdateProfile(HttpRequestMessage request, ApplicationUser model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (model.Id.Equals(User.Identity.GetUserId()))
                {
                    var user = _userManager.FindById(model.Id);
                    var temp = _userManager.FindByEmail(model.Email);
                    if (temp != null && temp != user)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, "Đã có tài khoản dùng email này.Vui lòng chọn email khác");
                    }
                    else
                    {
                        user.Address = model.Address;
                        user.FullName = model.FullName;
                        user.Gender = model.Gender;
                        user.PhoneNumber = model.PhoneNumber;
                        user.Email = model.Email;
                        user.BirthDay = model.BirthDay;
                        user.CountryId = model.CountryId;
                        var successed = _userManager.Update(user);
                        if (successed.Succeeded)
                        {
                            response = request.CreateResponse(HttpStatusCode.OK, "Cập nhật tài khoản thành công");
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.BadRequest, "Cập nhật tài khoản thất bại");
                        }
                    }
                }
                return response;
            });
        }

        [HttpPut]
        [Route("changepass")]
        public async Task<HttpResponseMessage> ChangePass(HttpRequestMessage request, PasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(User.Identity.GetUserId());
                try
                {
                    if (model.NewPassword.Equals(model.VerityPassword))
                    {
                        var result = await _userManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
                        if (result.Succeeded)
                        {
                            return request.CreateResponse(HttpStatusCode.OK, "Đổi mật khẩu thành công");
                        }
                        else
                            return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Mật khẩu không trùng nhau");
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest,"Vui lòng nhập đúng thông tin");
            }
        }

        [Route("changepassuser")]
        [HttpPut]
        public async Task<HttpResponseMessage> ChangePassUser(HttpRequestMessage request, string userId, string password)
        {
            bool result = await CheckPermission(SystemPermission.ManagerUsers);
            if (result)
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(password);
                    var item = await _userManager.UpdateAsync(user);
                    if (item.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, "Thay đổi mật khẩu thành công");
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Thay đổi mật khẩu thất bại");
                    }
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Thay đổi mật khẩu thất bại");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("updateroleuser")]
        [HttpPost]
        public async Task<HttpResponseMessage> UpdateRoleUser(HttpRequestMessage request,string roleUserModel)
        {
            bool result = await CheckPermission(SystemPermission.ManagerUsers);
            if (result)
            {
                var model = new JavaScriptSerializer().Deserialize<RoleUserModel>(roleUserModel);
                var user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {
                    foreach (var role in model.Roles)
                    {
                        if (role.Checked)
                        {
                            if (!_userManager.IsInRole(user.Id, role.Name))
                            {
                                _userManager.AddToRole(user.Id, role.Name);
                            }
                        }
                        else
                        {
                            if (_userManager.IsInRole(user.Id, role.Name))
                            {
                                _userManager.RemoveFromRole(user.Id, role.Name);
                            }
                        }
                    }
                    string log = String.Format("Phân quyền thành công cho tài khoản {0}", user.UserName);
                    InsertActivityLog(log, User.Identity.GetUserId());
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getrolemodelbyuser")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetRoleModelByUser(HttpRequestMessage request,string userId)
        {
            bool result = await CheckPermission(SystemPermission.ManagerUsers);
            if (result)
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                    var rolesUser = await _userManager.GetRolesAsync(userId);
                    var roles = roleManager.Roles.ToList();
                    var rolesUserModel = new RoleUserModel();
                    rolesUserModel.UserId = userId;
                    foreach (var role in roles)
                    {
                        var roleModel = new RoleModel();
                        roleModel.Id = role.Id;
                        roleModel.Name = role.Name;
                        roleModel.Description = role.Description;
                        if (rolesUser.Any(x => x == roleModel.Name))
                        {
                            roleModel.Checked = true;
                        }
                        else
                        {
                            roleModel.Checked = false;
                        }
                        rolesUserModel.Roles.Add(roleModel);
                    }
                    return request.CreateResponse(HttpStatusCode.OK, rolesUserModel);
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("lockuser")]
        [HttpPut]
        public async Task<HttpResponseMessage> LockUser(HttpRequestMessage request,string UserId, bool Status)
        {
            bool result = await CheckPermission(SystemPermission.ManagerUsers);
            if (result)
            {
                var user = await _userManager.FindByIdAsync(UserId);
                if (user != null)
                {
                    if (!User.Identity.GetUserId().Equals(UserId))
                    {
                        user.LockoutEnabled = Status;
                        await _userManager.UpdateAsync(user);
                        string log = String.Empty;
                        if (Status)
                            log = String.Format("Khóa tài khoản {0} thành công", user.UserName);
                        else
                            log = String.Format("Mở khóa tài khoản {0} thành công", user.UserName);
                        InsertActivityLog(log, User.Identity.GetUserId());
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {

                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã xảy ra lỗi");
                    }
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã xảy ra lỗi");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [HttpPost]
        [Route("register")]
        public async Task<HttpResponseMessage> Register(HttpRequestMessage request, RegisterModel model)
        {          
            if (ModelState.IsValid)
            {
                if (!model.Password.Equals(model.VerifyPassword))
                {                   
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Mật khẩu không khớp");
                }

                var objUsername = await _userManager.FindByNameAsync(model.UserName);
                if (objUsername != null)
                {                    
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Tài khoản đã tồn tài từ trước đó");
                }

                var objEmail = await _userManager.FindByEmailAsync(model.Email);
                if (objEmail != null)
                {
                   
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email đã tồn tại từ trước đó");
                }

                var user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    EmailConfirmed = true,
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    Gender = model.Gender,
                    Address = model.Address
                };

                await _userManager.CreateAsync(user, model.Password);
                string log = String.Format("Thêm mới tài khoản  {0} thành công", user.UserName);
                InsertActivityLog(log, null);
                var userCreate = await _userManager.FindByEmailAsync(model.Email);
                if (userCreate != null)
                {
                    await _userManager.AddToRolesAsync(user.Id, new string[] { "User" });
                }
               
                return request.CreateResponse(HttpStatusCode.OK, "Đăng ký thành công");
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Vui lòng nhập đầy đủ thông tin");
            }
        }

    }
}
