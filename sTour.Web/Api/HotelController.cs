﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/hotel")]
    public class HotelController : ApiControllerBase
    {
        private readonly ILocationService _locationService;
        private readonly IHotelService _hotelService;
        private readonly IHotel_Mapping_LocationService _hotel_mapping_locationService;
        private readonly IServiceHotelService _servicehotelService;
        private readonly IServiceHotel_Mapping_HotelService _service_mapping_hotelService;

        public HotelController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService, ILocationService locationService, IHotelService hotelService, IHotel_Mapping_LocationService hotel_mapping_locationService,
            IServiceHotelService servicehotelService, IServiceHotel_Mapping_HotelService service_mapping_hotelService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._locationService = locationService;
            this._hotelService = hotelService;
            this._hotel_mapping_locationService = hotel_mapping_locationService;
            this._servicehotelService = servicehotelService;
            this._service_mapping_hotelService = service_mapping_hotelService;
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerHotel))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    HotelModel model = ConvertFormDataToModel(result.FormData);
                    var user = User.Identity;
                    if (!_userManager.IsInRole(user.GetUserId(), SystemRole.Admin))
                    {
                        model.EnterpriseId = _userManager.Users.Where(c => c.Id == user.GetUserId()).SingleOrDefault().EnterpriseId.Value;
                    }

                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Hotel/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Hotel/Avatar/" + fileName;
                    }

                    var hotel = new Hotel();
                    hotel.Name = model.Name;
                    hotel.Alias = model.Alias;
                    hotel.Address = model.Address;
                    hotel.NameEn = model.NameEn;
                    hotel.AliasEn = model.AliasEn;
                    hotel.AddressEn = model.AddressEn;
                    hotel.Avatar = model.Avatar;
                    hotel.Latitude = model.Latitude;
                    hotel.Longitude = model.Longitude;
                    hotel.LongDes = model.LongDes;
                    hotel.ShortDes = model.ShortDes;
                    hotel.LongDesEn = model.LongDesEn;
                    hotel.ShortDesEn = model.ShortDesEn;
                    hotel.ViewCount = model.ViewCount;
                    hotel.Star = model.Star;
                    hotel.VideoDir = model.VideoDir;
                    hotel.HotLine = model.HotLine;
                    hotel.EnterpriseId = model.EnterpriseId;
                    hotel.Price = model.Price;
                    hotel.isActive = model.isActive;
                    _hotelService.Insert(hotel);

                    //    _hotelService.UpdateLatLon(hotel.Id, hotel.Latitude, hotel.Longitude);
                    if (hotel.Id > 0)
                    {


                        //Save file
                        string hotelDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Hotel/Hotel-{0}", hotel.Id)));
                        if (!System.IO.Directory.Exists(hotelDirectory))
                        {
                            System.IO.Directory.CreateDirectory(hotelDirectory);
                        }
                        var listFileAttach = new List<FileAttach>();
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Hotel/Hotel-{0}", hotel.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(hotelDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttach.Add(fileAttach);
                        }

                        hotel.Images = JsonConvert.SerializeObject(listFileAttach);
                        _hotelService.Update(hotel);

                        //Insert table mapping location
                        List<string> locations = model.locationIds.Split(',').ToList();

                        foreach (string location in locations)
                        {
                            try
                            {
                                var locationmapping = new Hotel_Mapping_Location();
                                locationmapping.HotelId = hotel.Id;
                                locationmapping.LocationId = Convert.ToInt32(location);
                                _hotel_mapping_locationService.Insert(locationmapping);
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        //Insert table mapping service
                        List<string> services = model.serviceIds.Split(',').ToList();
                        foreach (string service in services)
                        {
                            try
                            {
                                var servicemapping = new ServiceHotel_Mapping_Hotel();
                                servicemapping.HotelId = hotel.Id;
                                servicemapping.ServiceHotelId = Convert.ToInt32(service);
                                _service_mapping_hotelService.Insert(servicemapping);
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                    }

                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }
        [Route("gethotels")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetHotels(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerHotel))
            {
                HttpResponseMessage response = null;
                int totalRow;
               
                var userId = User.Identity.GetUserId();
                dynamic hotels;
                if (_userManager.IsInRole(userId, SystemRole.Admin))
                {
                    hotels = _hotelService.GetHotels(null, page, pageSize, keyword, out totalRow).ToList();
                }
                else
                {
                    hotels = _hotelService.GetHotels(_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0, page, pageSize, keyword, out totalRow).ToList();
                }
                var model = Mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelModel>>(hotels);
                PaginationSet<HotelModel> pagedSet = new PaginationSet<HotelModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)

        {
            if (await CheckPermission(SystemPermission.ManagerHotel))
            {
                var hotel = _hotelService.GetById(Id);
                var userId = User.Identity.GetUserId();
                if (!_userManager.IsInRole(userId, SystemRole.Admin))
                {
                    int enterpriseId = hotel.EnterpriseId ?? 0;
                    if (enterpriseId == 0)
                    {
                        return UnAuthorized();
                    }
                    if ((_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0) != enterpriseId)
                    {
                        return UnAuthorized();
                    }
                }

                var locationByHotelId = _locationService.GetByHotelId(Id).Select(c => new
                {
                    c.Id
                });


                var locationAll = _locationService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name,
                    _checked = (locationByHotelId.Any(x => x.Id == c.Id) ? true : false)
                });
                var serviceByHotelId = _servicehotelService.GetByHotelId(Id).Select(c => new
                {
                    c.Id
                });
                var serviceAll = _servicehotelService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name,
                    _checked = (serviceByHotelId.Any(x => x.Id == c.Id) ? true : false)
                });
                object data = new
                {
                    data = hotel,
                    locations = locationAll,
                    services = serviceAll
                };
                return request.CreateResponse(HttpStatusCode.OK, data);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPost]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerHotel))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    HotelModel model = ConvertFormDataToModel(result.FormData);
                    var hotel = _hotelService.GetById(model.Id);
                    if (hotel != null)
                    {
                        var userId = User.Identity.GetUserId();
                        if (!_userManager.IsInRole(userId, SystemRole.Admin))
                        {
                            int enterpriseId = hotel.EnterpriseId ?? 0;
                            if (enterpriseId == 0)
                            {
                                return UnAuthorized();
                            }
                            if ((_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0) != enterpriseId)
                            {
                                return UnAuthorized();
                            }
                        }
                        else
                        {
                            hotel.EnterpriseId = model.EnterpriseId;
                        }

                        if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                        {
                            model.Avatar = "/Assets/images_none.png";
                        }
                        else if (!model.Avatar.StartsWith("~/Content") && hotel.Avatar != model.Avatar)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Hotel/Avatar"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Avatar = "~/Content/Images/Hotel/Avatar/" + fileName;
                        }

                        hotel.Name = model.Name;
                        hotel.Alias = model.Alias;
                        hotel.Address = model.Address;
                        hotel.NameEn = model.NameEn;
                        hotel.AliasEn = model.AliasEn;
                        hotel.AddressEn = model.AddressEn;
                        hotel.Avatar = model.Avatar;
                        hotel.Latitude = model.Latitude;
                        hotel.Longitude = model.Longitude;
                        hotel.LongDes = model.LongDes;
                        hotel.ShortDes = model.ShortDes;
                        hotel.LongDesEn = model.LongDesEn;
                        hotel.ShortDesEn = model.ShortDesEn;
                        hotel.ViewCount = model.ViewCount;
                        hotel.Price = model.Price;
                        hotel.HotLine = model.HotLine;
                        hotel.Star = model.Star;
                        hotel.VideoDir = model.VideoDir;
                        hotel.isActive = model.isActive;
                        //    _hotelService.UpdateLatLon(hotel.Id, hotel.Latitude, hotel.Longitude);
                        string hotelDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Hotel/Hotel-{0}", hotel.Id)));
                        if (!System.IO.Directory.Exists(hotelDirectory))
                        {
                            System.IO.Directory.CreateDirectory(hotelDirectory);
                        }
                        var listFileAttachNew = JsonConvert.DeserializeObject<List<FileAttach>>(model.Images);
                        var listFileAttachOld = JsonConvert.DeserializeObject<List<FileAttach>>(string.IsNullOrEmpty(hotel.Images) ? "[]" : hotel.Images);
                        foreach (var file in listFileAttachOld)
                        {
                            if (!listFileAttachNew.Any(f => f.FileName == file.FileName))
                            {
                                string path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Hotel/Hotel-{0}", hotel.Id) + "/" + file.FileName));
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                            }
                        }
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Hotel/Hotel-{0}", hotel.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(hotelDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                //   fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttachNew.Add(fileAttach);
                        }

                        hotel.Images = JsonConvert.SerializeObject(listFileAttachNew);
                        _hotelService.Update(hotel);
                        _hotel_mapping_locationService.DeleteByHotelId(hotel.Id);
                        //Insert table mapping location
                        List<string> locations = model.locationIds.Split(',').ToList();

                        foreach (string location in locations)
                        {
                            try
                            {
                                var locationmapping = new Hotel_Mapping_Location();
                                locationmapping.HotelId = hotel.Id;
                                locationmapping.LocationId = Convert.ToInt32(location);
                                _hotel_mapping_locationService.Insert(locationmapping);
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        _service_mapping_hotelService.DeleteByHotelId(hotel.Id);
                        //Insert table mapping service
                        List<string> services = model.serviceIds.Split(',').ToList();
                        foreach (string service in services)
                        {
                            try
                            {
                                var servicemapping = new ServiceHotel_Mapping_Hotel();
                                servicemapping.HotelId = hotel.Id;
                                servicemapping.ServiceHotelId = Convert.ToInt32(service);
                                _service_mapping_hotelService.Insert(servicemapping);
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        HotelModel ConvertFormDataToModel(NameValueCollection formData)
        {
            var model = new HotelModel();
            if (!String.IsNullOrEmpty(formData["Id"]))
                model.Id = int.Parse(formData["Id"]);
            model.Address = formData["Address"];
            model.AddressEn = formData["AddressEn"];
            model.Avatar = formData["Avatar"];
            model.Images = formData["Images"];
            model.isActive = bool.Parse(formData["isActive"]);
            model.Latitude = double.Parse(formData["Latitude"]);
            model.LongDes = formData["LongDes"];
            model.LongDesEn = formData["LongDesEn"];
            model.Longitude = double.Parse(formData["Longitude"]);
            model.Name = formData["Name"];
            model.NameEn = formData["NameEn"];
            model.Alias = formData["Alias"];
            model.AliasEn = formData["AliasEn"];
            model.Price = int.Parse(formData["Price"]);
            model.HotLine = formData["HotLine"];
            model.Star = int.Parse(formData["Star"]);
            model.ShortDes = formData["ShortDes"];
            model.ShortDesEn = formData["ShortDesEn"];
            model.VideoDir = formData["VideoDir"];
            model.ViewCount = int.Parse(formData["ViewCount"]);
            if (formData["EnterpriseId"] != "null" && formData["EnterpriseId"] != "undefined")
                model.EnterpriseId = int.Parse(formData["EnterpriseId"]);
            model.locationIds = formData["locationIds"];
            model.serviceIds = formData["serviceIds"];
            return model;
        }
    }
}
