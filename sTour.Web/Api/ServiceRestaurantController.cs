﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/servicerestaurant")]
    [Authorize]
    public class ServiceRestaurantController : ApiControllerBase
    {
        private IServiceRestaurantService _servicerestaurantService;
        public ServiceRestaurantController(IServiceRestaurantService servicerestaurantService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._servicerestaurantService = servicerestaurantService;
        }

        [Route("getservicerestaurants")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetServiceRestaurants(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                HttpResponseMessage response = null;
                int totalRow;
                if (page >= 1)
                    page = page - 1;
                dynamic servicerestaurants;
                servicerestaurants = _servicerestaurantService.GetServiceRestaurants(page, pageSize, keyword, out totalRow).ToList();
                var model = Mapper.Map<IEnumerable<ServiceRestaurant>, IEnumerable<ServiceRestaurantModel>>(servicerestaurants);
                PaginationSet<ServiceRestaurantModel> pagedSet = new PaginationSet<ServiceRestaurantModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getall")]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage reuqest)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                var services = _servicerestaurantService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name
                });
                return reuqest.CreateResponse(HttpStatusCode.OK, services);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, ServiceRestaurantModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                try
                {
                    if (String.IsNullOrEmpty(model.Icon) || model.Icon.Contains("images_none.png"))
                    {
                        model.Icon = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Icon.Split(',')[1]);
                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/ServiceRestaurant/Icon"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Icon = "~/Content/Images/ServiceRestaurant/Icon/" + fileName;
                    }

                    var servicerestaurant = new ServiceRestaurant();
                    servicerestaurant.Name = model.Name;
                    servicerestaurant.NameEn = model.NameEn;
                    servicerestaurant.Icon = model.Icon;
                    servicerestaurant.Description = model.Description;
                    servicerestaurant.DescriptionEn = model.DescriptionEn;
                    _servicerestaurantService.Insert(servicerestaurant);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                var servicerestaurant = _servicerestaurantService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, servicerestaurant);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, ServiceRestaurantModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                try
                {
                    var servicerestaurant = _servicerestaurantService.GetById(model.Id);
                    if (servicerestaurant != null)
                    {
                        if (String.IsNullOrEmpty(model.Icon) || model.Icon.Contains("images_none.png"))
                        {
                            model.Icon = "/Assets/images_none.png";
                        }
                        else if (!model.Icon.StartsWith("~/Content") && servicerestaurant.Icon != model.Icon)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Icon.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/ServiceRestaurant/Icon"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Icon = "~/Content/Images/ServiceRestaurant/Icon/" + fileName;
                        }
                        servicerestaurant.NameEn = model.NameEn;
                        servicerestaurant.Name = model.Name;
                        servicerestaurant.Icon = model.Icon;
                        servicerestaurant.Description = model.Description;
                        servicerestaurant.DescriptionEn = model.DescriptionEn;
                        _servicerestaurantService.Update(servicerestaurant);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

    }
}
