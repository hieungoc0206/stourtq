﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/banner")]
    [Authorize]
    public class BannerController : ApiControllerBase
    {
        private IBannerService _bannerService;
        public BannerController(IBannerService bannerService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._bannerService = bannerService;
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, BannerModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerBanner))
            {
                try
                {
                    var user = User.Identity;
                    if (String.IsNullOrEmpty(model.Image) || model.Image.Contains("images_none.png"))
                    {
                        model.Image = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Image.Split(',')[1]);
                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Banner"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Image = "~/Content/Images/Banner/" + fileName;
                    }

                    var banner = new Banner();
                    banner.Name = model.Name;
                    banner.Description = model.Description;
                    banner.Image = model.Image;
                    banner.Active = model.Active;
                    _bannerService.Insert(banner);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerBanner))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var banners = _bannerService.GetAll().OrderByDescending(x => x.Id).Take(3).ToList();
                    var model = Mapper.Map<IEnumerable<Banner>, IEnumerable<BannerModel>>(banners);
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }

        }
        [Route("getbanners")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetBanners(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerBanner))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    int totalRow;
                    if (page >= 1)
                        page = page - 1;
                    var banners = _bannerService.GetBanners(page, pageSize, keyword, out totalRow).ToList();
                    var model = Mapper.Map<IEnumerable<Banner>, IEnumerable<BannerModel>>(banners);
                    PaginationSet<BannerModel> pagedSet = new PaginationSet<BannerModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = model
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerBanner))
            {
                var banner = _bannerService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, banner);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request,BannerModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerBanner))
            {
                try
                {
                    var banner = _bannerService.GetById(model.Id);
                    if (banner != null)
                    {
                        if (String.IsNullOrEmpty(model.Image) || model.Image.Contains("images_none.png"))
                        {
                            model.Image = "/Assets/images_none.png";
                        }
                        else if (!model.Image.StartsWith("~/Content") && banner.Image != model.Image)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Image.Split(',')[1]);
                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Banner"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Image = "~/Content/Images/Banner/" + fileName;
                        }

                        banner.Name = model.Name;
                        banner.Image = model.Image;
                        banner.Description = model.Description;
                        banner.Active = model.Active;
                        _bannerService.Update(banner);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.OK, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
