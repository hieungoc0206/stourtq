﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/cultural")]
    [Authorize]
    public class CulturalController : ApiControllerBase
    {
        private readonly ICulturalService _culturalService;
        private readonly IDistrictService _districtService;
        public CulturalController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService, ICulturalService culturalService, IDistrictService districtService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._culturalService = culturalService;
            this._districtService = districtService;
        }

        [Route("create")]      
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerCultural))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    CulturalModel model = ConvertFormDataToModel(result.FormData);
                    var user = User.Identity;
                    ////
                    //if(!_userManager.IsInRole(user.GetUserId(), SystemRole.Admin))
                    //{
                    //    model.EnterpriseId = _userManager.Users.Where(c => c.Id == user.GetUserId()).SingleOrDefault().EnterpriseId.Value;
                    //}
                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Cultural/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Cultural/Avatar/" + fileName;
                    }

                    var cultural = new Cultural();
                    cultural.Name = model.Name;
                    cultural.Alias = model.Alias;
                    cultural.Address = model.Address;
                    cultural.NameEn = model.NameEn;
                    cultural.AliasEn = model.AliasEn;
                    cultural.AddressEn = model.AddressEn;
                    cultural.Avatar = model.Avatar;
                    cultural.Latitude = model.Latitude;
                    cultural.Longitude = model.Longitude;
                    cultural.LongDes = model.LongDes;
                    cultural.ShortDes = model.ShortDes;
                    cultural.LongDesEn = model.LongDesEn;
                    cultural.ShortDesEn = model.ShortDesEn;
                    cultural.ViewCount = model.ViewCount;
                    cultural.VideoDir = model.VideoDir;
                    cultural.VideoDirEn = model.VideoDirEn;
                    cultural.DistrictId = model.DistrictId;
                    //cultural.EnterpriseId = model.EnterpriseId;
                    cultural.CreatedAt = DateTime.Now;
                    cultural.isActive = model.isActive;
                    cultural.Audio = model.Audio;
                    cultural.AudioEn = model.AudioEn;
                    cultural.OpenDate = model.OpenDate;
                    _culturalService.Insert(cultural);
                    if (cultural.Id > 0)
                    {
                        //Save file
                        string culturalDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Cultural/Cultural-{0}", cultural.Id)));
                        if (!System.IO.Directory.Exists(culturalDirectory))
                        {
                            System.IO.Directory.CreateDirectory(culturalDirectory);
                        }
                        var listFileAttach = new List<FileAttach>();
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Cultural/Cultural-{0}", cultural.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(culturalDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttach.Add(fileAttach);
                        }

                        cultural.Images = JsonConvert.SerializeObject(listFileAttach);
                        _culturalService.Update(cultural);
                    }

                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getculturals")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetCulturals(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerCultural))
            {
                HttpResponseMessage response = null;
                int totalRow;               
                var userId = User.Identity.GetUserId();
                //dynamic culturals;
                //if(_userManager.IsInRole(userId, SystemRole.Admin))
                //{
                //    culturals=_culturalService.GetCulturals()
                //}

                var culturals = _culturalService.GetCulturals(page, pageSize, keyword, out totalRow).ToList();
                var model = Mapper.Map<IEnumerable<Cultural>, IEnumerable<CulturalModel>>(culturals);
                foreach (var item in model)
                {

                    if (item.DistrictId != null)
                        item.DistrictName = _districtService.GetById(item.DistrictId.Value).Name;


                }
                PaginationSet<CulturalModel> pagedSet = new PaginationSet<CulturalModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerCultural))
            {
                var cultural = _culturalService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, cultural);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]        
        [HttpPost]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerCultural))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    CulturalModel model = ConvertFormDataToModel(result.FormData);
                    var cultural = _culturalService.GetById(model.Id);
                    if (cultural != null)
                    {
                        if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                        {
                            model.Avatar = "/Assets/images_none.png";
                        }
                        else if (!model.Avatar.StartsWith("~/Content") && cultural.Avatar != model.Avatar)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Cultural/Avatar"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Avatar = "~/Content/Images/Cultural/Avatar/" + fileName;
                        }

                        cultural.Name = model.Name;
                        cultural.Alias = model.Alias;
                        cultural.Address = model.Address;
                        cultural.NameEn = model.NameEn;
                        cultural.AliasEn = model.AliasEn;
                        cultural.AddressEn = model.AddressEn;
                        cultural.Avatar = model.Avatar;
                        cultural.Latitude = model.Latitude;
                        cultural.Longitude = model.Longitude;
                        cultural.LongDes = model.LongDes;
                        cultural.ShortDes = model.ShortDes;
                        cultural.LongDesEn = model.LongDesEn;
                        cultural.ShortDesEn = model.ShortDesEn;
                        cultural.ViewCount = model.ViewCount;
                        cultural.DistrictId = model.DistrictId;                     
                        cultural.VideoDir = model.VideoDir;
                        cultural.VideoDirEn = model.VideoDirEn;
                        cultural.isActive = model.isActive;
                        cultural.Audio = model.Audio;
                        cultural.AudioEn = model.AudioEn;
                        cultural.OpenDate = model.OpenDate;
                        _culturalService.Update(cultural);
                        string culturalDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Cultural/Cultural-{0}", cultural.Id)));
                        if (!System.IO.Directory.Exists(culturalDirectory))
                        {
                            System.IO.Directory.CreateDirectory(culturalDirectory);
                        }
                        var listFileAttachNew = JsonConvert.DeserializeObject<List<FileAttach>>(model.Images);
                        var listFileAttachOld = JsonConvert.DeserializeObject<List<FileAttach>>(string.IsNullOrEmpty(cultural.Images) ? "[]" : cultural.Images);
                        foreach (var file in listFileAttachOld)
                        {
                            if (!listFileAttachNew.Any(f => f.FileName == file.FileName))
                            {
                                string path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Cultural/Cultural-{0}", cultural.Id) + "/" + file.FileName));
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                            }
                        }
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Cultural/Cultural-{0}", cultural.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(culturalDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttachNew.Add(fileAttach);
                        }

                        cultural.Images = JsonConvert.SerializeObject(listFileAttachNew);
                        _culturalService.Update(cultural);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        CulturalModel ConvertFormDataToModel(NameValueCollection formData)
        {
            var model = new CulturalModel();
            if (!String.IsNullOrEmpty(formData["Id"]))
                model.Id = int.Parse(formData["Id"]);
            model.Address = formData["Address"];
            model.AddressEn = formData["AddressEn"];
            model.Avatar = formData["Avatar"];
            model.Images = formData["Images"];
            model.isActive = bool.Parse(formData["isActive"]);
            model.Latitude = double.Parse(formData["Latitude"]);
            model.LongDes = formData["LongDes"];
            model.LongDesEn = formData["LongDesEn"];
            model.Longitude = double.Parse(formData["Longitude"]);
            model.Name = formData["Name"];
            model.Alias = formData["Alias"];
            model.NameEn = formData["NameEn"];
            model.AliasEn = formData["AliasEn"];
            model.ShortDes = formData["ShortDes"];
            model.ShortDesEn = formData["ShortDesEn"];
            model.VideoDir = formData["VideoDir"];
            model.VideoDirEn = formData["VideoDirEn"];
            model.ViewCount = int.Parse(formData["ViewCount"]);
            if (formData["DistrictId"] != "null" && formData["DistrictId"] != "undefined")
                model.DistrictId = int.Parse(formData["DistrictId"]);
            if (formData["Audio"] != "null" && formData["Audio"] != "undefined")
                model.Audio = formData["Audio"];
            if (formData["AudioEn"] != "null" && formData["AudioEn"] != "undefined")
                model.AudioEn = formData["AudioEn"];
            if (formData["OpenDate"] != "null" && formData["OpenDate"] != "undefined")
                model.OpenDate = formData["OpenDate"];
            return model;
        }
    }
}
