﻿using Microsoft.AspNet.Identity;
using sTour.Common;
using sTour.Data;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/systemconfig")]
    public class SystemConfigController : ApiControllerBase
    {
        private readonly ISystemConfigService _systemConfigService;

        public SystemConfigController(ISystemConfigService systemConfigService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            _systemConfigService = systemConfigService;
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, SystemConfig item)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var config = _systemConfigService.GetAll().OrderByDescending(x => x.Id).SingleOrDefault();
                    config.SystemName = item.SystemName;
                    config.MailServer = item.MailServer;
                    config.CachedTime = item.CachedTime;
                    config.Email = item.Email;
                    config.Password = item.Password;
                    config.HotLine = item.HotLine;
                    config.Contact = item.Contact;
                    _systemConfigService.Update(config);
                    string message = String.Format("Thay đổi cấu hình hệ thống");
                    InsertActivityLog(message, User.Identity.GetUserId());
                    response = request.CreateResponse(HttpStatusCode.OK, true);
                    return response;
                });

            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getconfig")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetConfig(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var config = _systemConfigService.GetAll().OrderByDescending(x => x.Id).SingleOrDefault();
                    response = request.CreateResponse(HttpStatusCode.OK, config);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }


        [Route("systeminfo")]
        [HttpGet]
        public async Task<HttpResponseMessage> SystemInfo(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var model = new SystemInfoModel();
                    try
                    {
                        model.OperatingSystem = Environment.OSVersion.VersionString;
                    }
                    catch (Exception) { }

                    try
                    {
                        model.AspNetInfo = RuntimeEnvironment.GetSystemVersion();
                    }
                    catch (Exception) { }

                    try
                    {
                        model.IsFullTrust = AppDomain.CurrentDomain.IsFullyTrusted.ToString();
                    }
                    catch (Exception) { }

                    model.ServerTimeZone = TimeZone.CurrentTimeZone.StandardName;
                    model.ServerLocalTime = DateTime.Now;
                    model.UtcTime = DateTime.UtcNow;
                    model.HttpHost = Url.Request.RequestUri.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("backup")]
        [HttpGet]
        public async Task<HttpResponseMessage> BackUpDatabase(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    string namefile = "sTourTQ" + DateTime.Now.ToString("yyyyMMdd") + ".BAK";
                    string disk = HttpContext.Current.Server.MapPath("/Backup/") + namefile;
                    if (!System.IO.File.Exists(disk))
                    {
                        var dbcontext = new STourContext();
                        SqlParameter _disk = new SqlParameter("@disk", disk);
                        dbcontext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "exec BackUpdatabase @disk", _disk);
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (FileStream file = new FileStream(disk, FileMode.Open, FileAccess.Read))
                        {
                            byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            ms.Write(bytes, 0, (int)file.Length);
                            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
                            httpResponseMessage.Content = new ByteArrayContent(bytes.ToArray());
                            httpResponseMessage.Content.Headers.Add("x-filename", namefile);
                            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                            httpResponseMessage.Content.Headers.ContentDisposition.FileName = namefile;
                            httpResponseMessage.StatusCode = HttpStatusCode.OK;
                            string message = String.Format("Backup dữ liệu");
                            InsertActivityLog(message, User.Identity.GetUserId());
                            return httpResponseMessage;
                        }
                    };
                });
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}