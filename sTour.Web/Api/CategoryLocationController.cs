﻿using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/categorylocation")]
    [Authorize]
    public class CategoryLocationController : ApiControllerBase
    {
        private readonly ICategoryLocationService _categoryLocationService;
        public CategoryLocationController(ICategoryLocationService categoryLocationService,ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._categoryLocationService = categoryLocationService;
        }

        [HttpGet]
        [Route("getall")]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                var categories = _categoryLocationService.GetAll();
                return request.CreateResponse(HttpStatusCode.OK, categories);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [HttpPost]
        [Route("update")]
        public async Task<HttpResponseMessage> Update(HttpRequestMessage request,CategoryLocation model)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                HttpResponseMessage response = null;
                try
                {
                    if (model.Id == 0)
                    {
                        _categoryLocationService.Insert(model);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        var category = _categoryLocationService.GetById(model.Id);
                        category.Name = model.Name;
                        category.Description = model.Description;
                        _categoryLocationService.Update(category);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                    }
                }
                catch (Exception e)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                }
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }
        [HttpGet]
        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                var category = _categoryLocationService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, category);
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
