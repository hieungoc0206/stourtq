﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/enterprise")]
    [Authorize]
    public class EnterpriseController : ApiControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;
        public EnterpriseController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService, IEnterpriseService enterpriseService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._enterpriseService = enterpriseService;
        }

        [Route("getall")]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerEnterprise))
            {
                return CreateHttpResponse(request, () =>
                {
                    var userId = User.Identity.GetUserId();
                    if (_userManager.IsInRole(userId, SystemRole.Admin))
                    {
                        var enterprises = _enterpriseService.GetAll();
                        var model = Mapper.Map<IEnumerable<Enterprise>, IEnumerable<EnterpriseModel>>(enterprises);
                        return request.CreateResponse(HttpStatusCode.OK, model);
                    }
                    else
                    {
                        int enterpriseId = _userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0;
                        var enterprises = _enterpriseService.GetAll().Where(x => x.Id == enterpriseId);
                        var model = Mapper.Map<IEnumerable<Enterprise>, IEnumerable<EnterpriseModel>>(enterprises);
                        return request.CreateResponse(HttpStatusCode.OK, model);
                    }                                    
                });
            }
            else
            {
                return UnAuthorized();
            }
           
        }
    }
}
