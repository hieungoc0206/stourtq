﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sTour.Common;
using sTour.Data;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/role")]
    public class RoleController : ApiControllerBase
    {
        private readonly IRole_Permisstion_MappingService _rolePermisstionService;
        private readonly IPermissionService _permisstionService;

        public RoleController(IRole_Permisstion_MappingService rolePermisstionService, IPermissionService permisstionService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            _rolePermisstionService = rolePermisstionService;
            _permisstionService = permissonService;
        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                    var items = roleManager.Roles.ToList();
                    var models = items.Select(c =>
                    {
                        var model = Mapper.Map<ApplicationRole, RoleModel>(c);
                        return model;
                    });
                    response = request.CreateResponse(HttpStatusCode.OK, models);
                    return response;
                });

            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, ApplicationRole role)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                HttpResponseMessage response = null;
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var item = await roleManager.FindByNameAsync(role.Name);
                if (item == null)
                {
                    var successed = await roleManager.CreateAsync(new ApplicationRole { Name = role.Name, Description = role.Description });
                    if (successed.Succeeded)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, false);
                }
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, string id)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var item = await roleManager.FindByIdAsync(id);
                if (item != null)
                {
                    return request.CreateResponse(HttpStatusCode.OK, item);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, ApplicationRole role)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var item = roleManager.FindById(role.Id);
                if (item != null)
                {
                    item.Description = role.Description;
                    var successed = await roleManager.UpdateAsync(item);
                    if (successed.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, "Cập nhật thành công");
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Cập nhật thất bại");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getpermission/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPermisstion(HttpRequestMessage request, string id)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                var model = new RolePermissionModel();
                model.RoleId = id;
                var pemisstions = _permissionService.GetAll();
                var rolePermisstion = _rolePermisstionService.GetByRoleId(id);
                foreach (var permisstion in pemisstions)
                {
                    var permisstionModel = Mapper.Map<Permisstion, PermissionModel>(permisstion);
                    if (rolePermisstion.Any(x => x.RoleId == id && x.PermissionId == permisstion.Id))
                    {
                        permisstionModel.Checked = true;
                    }
                    model.Permisstions.Add(permisstionModel);
                }
                return request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("addpermissiontorole")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddPermisstionToRole(HttpRequestMessage request, string rolePermisstionModel)
        {
            bool result = await CheckPermission(SystemPermission.ManagerSystem);
            if (result)
            {
                var model = new JavaScriptSerializer().Deserialize<RolePermissionModel>(rolePermisstionModel);
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var role = await roleManager.FindByIdAsync(model.RoleId);
                if (role != null)
                {
                    var listRolePermissionMapping = new List<Role_Permisstion_Mapping>();
                    foreach(var per in model.Permisstions)
                    {
                        if (per.Checked)
                        {
                            listRolePermissionMapping.Add(new Role_Permisstion_Mapping()
                            {
                                RoleId = model.RoleId,
                                PermissionId = per.Id
                            });
                        }                  
                    }
                    _permissionService.AddPermisionsToRole(listRolePermissionMapping,model.RoleId);
                    string log = String.Format("Thêm chức năng thành công cho quyền {0}", role.Name);
                    InsertActivityLog(log, User.Identity.GetUserId());
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, false);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

    }
}
