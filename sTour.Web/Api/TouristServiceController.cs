﻿using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/touristservice")]
    [Authorize]
    public class TouristServiceController : ApiControllerBase
    {
        private ITouristServiceService _touristServiceService;
        public TouristServiceController(ITouristServiceService touristServiceService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._touristServiceService = touristServiceService;
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, TouristService model)
        {
            if (await CheckPermission(SystemPermission.ManagerTouristService))
            {
                try
                {
                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);
                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/TouristService/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/TouristService/Avatar/" + fileName;
                    }
                    var touristService = new TouristService();
                    touristService.Name = model.Name;
                    touristService.NameEn = model.NameEn;
                    touristService.HotLine = model.HotLine;
                    touristService.Address = model.Address;
                    touristService.AddressEn = model.AddressEn;
                    touristService.Content = model.Content;
                    touristService.ContentEn = model.ContentEn;
                    touristService.Avatar = model.Avatar;
                    touristService.Category = model.Category;
                    _touristServiceService.Insert(touristService);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("gettouristservices")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetTouristServices(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerTouristService))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    int totalRow;
                    if (page >= 1)
                        page = page - 1;                 
                    var touristServices = _touristServiceService.GetTouristService(page, pageSize, keyword, out totalRow).ToList();                   
                    PaginationSet<TouristService> pagedSet = new PaginationSet<TouristService>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = touristServices
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerTouristService))
            {
                var touristService = _touristServiceService.GetById(Id);               
                return request.CreateResponse(HttpStatusCode.OK, touristService);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, TouristService model)
        {
            if (await CheckPermission(SystemPermission.ManagerTouristService))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var touristService = _touristServiceService.GetById(model.Id);
                   
                    if (touristService != null)
                    {
                        if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                        {
                            model.Avatar = "/Assets/images_none.png";
                        }
                        else if (!model.Avatar.StartsWith("~/Content") && touristService.Avatar != model.Avatar)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Hotel/Avatar"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Avatar = "~/Content/Images/Hotel/Avatar/" + fileName;
                        }
                        touristService.Name = model.Name;
                        touristService.NameEn = model.NameEn;
                        touristService.HotLine = model.HotLine;
                        touristService.Address = model.Address;
                        touristService.AddressEn = model.AddressEn;
                        touristService.Category = model.Category;
                        touristService.Content = model.Content;
                        touristService.ContentEn = model.ContentEn;
                        touristService.Avatar = model.Avatar;
                        _touristServiceService.Update(touristService);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                        return response;
                    }
                    else
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                        return response;
                    }
                });
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
