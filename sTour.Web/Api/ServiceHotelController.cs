﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/servicehotel")]
    public class ServiceHotelController : ApiControllerBase
    {
        private IServiceHotelService _servicehotelService;
        public ServiceHotelController(IServiceHotelService servicehotelService,ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._servicehotelService = servicehotelService;
        }

        [Route("getservicehotels")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetServiceHotels(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                HttpResponseMessage response = null;
                int totalRow;
                if (page >= 1)
                    page = page - 1;
                dynamic servicehotels;
                servicehotels = _servicehotelService.GetServiceHotels(page, pageSize, keyword, out totalRow).ToList();
                var model = Mapper.Map<IEnumerable<ServiceHotel>, IEnumerable<ServiceHotelModel>>(servicehotels);
                PaginationSet<ServiceHotelModel> pagedSet = new PaginationSet<ServiceHotelModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getall")]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage reuqest)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                var services = _servicehotelService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name
                });
                return reuqest.CreateResponse(HttpStatusCode.OK, services);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request,ServiceHotelModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                try
                {
                    if (String.IsNullOrEmpty(model.Icon) || model.Icon.Contains("images_none.png"))
                    {
                        model.Icon = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Icon.Split(',')[1]);
                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/ServiceHotel/Icon"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Icon = "~/Content/Images/ServiceHotel/Icon/" + fileName;
                    }

                    var servicehotel = new ServiceHotel();
                    servicehotel.Name = model.Name;
                    servicehotel.NameEn = model.NameEn;
                    servicehotel.Icon = model.Icon;
                    servicehotel.Description = model.Description;
                    servicehotel.DescriptionEn = model.DescriptionEn;
                    _servicehotelService.Insert(servicehotel);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                var servicehotel = _servicehotelService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, servicehotel);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]    
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, ServiceHotelModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerService))
            {
                try
                {
                    var servicehotel = _servicehotelService.GetById(model.Id);
                    if (servicehotel != null)
                    {
                        if (String.IsNullOrEmpty(model.Icon) || model.Icon.Contains("images_none.png"))
                        {
                            model.Icon = "/Assets/images_none.png";
                        }
                        else if (!model.Icon.StartsWith("~/Content") && servicehotel.Icon != model.Icon)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Icon.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/ServiceHotel/Icon"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Icon = "~/Content/Images/ServiceHotel/Icon/" + fileName;
                        }

                        servicehotel.Name = model.Name;
                        servicehotel.NameEn = model.NameEn;
                        servicehotel.Icon = model.Icon;
                        servicehotel.Description = model.Description;
                        servicehotel.DescriptionEn = model.DescriptionEn;
                        _servicehotelService.Update(servicehotel);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
