﻿using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/activitylog")]
    public class ActivityLogController : ApiControllerBase
    {


        public ActivityLogController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {

        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request,int page, int pageSize)
        {
            if (await CheckPermission(SystemPermission.ManagerUsers))
            {
                HttpResponseMessage response = null;
                page = page - 1;
                var logs = _activityLogService.GetActivityLog(page, pageSize);
                int totalRow = _activityLogService.Count();
                PaginationSet<ActivityLog> pagedSet = new PaginationSet<ActivityLog>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = logs
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }


    }
}
