﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/country")]
    public class CountryController : ApiControllerBase
    {
        protected ICountryService _countryService;
        public CountryController(ICountryService countryService,ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._countryService = countryService;
        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                return CreateHttpResponse(request, () =>
                {
                    var country = _countryService.GetAll();
                    var model = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryModel>>(country);
                    HttpResponseMessage response = null;                  
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                    return response;
                });         
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request,CountryModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                return CreateHttpResponse(request, () =>
                {                             
                    HttpResponseMessage response = null;
                    if (_countryService.GetAll().Where(x => x.Name == model.Name || x.SortName == model.SortName).Count() == 0)
                    {
                        var country = new Country();
                        country.Name = model.Name;
                        country.PhoneCode = model.PhoneCode;
                        country.SortName = model.SortName;
                        _countryService.Insert(country);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã xảy ra lỗi");
                    }
                   
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }
        [Route("getcountries")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetCountries(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    int totalRow;
                    if (page >= 1)
                        page = page - 1;
                    dynamic countries;
                    countries = _countryService.GetCountry(page, pageSize, keyword, out totalRow).ToList();
                    var model = Mapper.Map<IEnumerable<Country>,IEnumerable<CountryModel>>(countries);
                    PaginationSet<CountryModel> pagedSet = new PaginationSet<CountryModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = model
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });                         
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request,int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                var country = _countryService.GetById(Id);
                var model = Mapper.Map<Country, CountryModel>(country);
                return request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]   
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request,CountryModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    var country = _countryService.GetById(model.Id);
                    if (country != null)
                    {
                        country.Name = model.Name;
                        country.SortName = model.SortName;
                        country.PhoneCode = model.PhoneCode;
                        _countryService.Update(country);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                        return response;
                    }
                    else
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                        return response;
                    }
                });           
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
