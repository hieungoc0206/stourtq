﻿using AutoMapper;
using OfficeOpenXml;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/province")]
    [Authorize]
    public class ProvinceController : ApiControllerBase
    {
        private IProvincialService _provincialService;
        private ICountryService _countryService;

        public ProvinceController(IProvincialService provincialService, ICountryService countryService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._provincialService = provincialService;
            this._countryService = countryService;
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, ProvinceModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerProvince))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    if (_provincialService.GetAll().Where(x => x.Name == model.Name || x.SortName == model.SortName).Count() == 0)
                    {
                        var province = new Provincial();
                        province.Name = model.Name;
                        province.SortName = model.SortName;
                        province.CountryId = model.CountryId;
                        _provincialService.Insert(province);
                        response = request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã xảy ra lỗi");
                    }
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getprovinces")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetProvinces(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerProvince))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    int totalRow;
                    if (page >= 1)
                        page = page - 1;
                    dynamic provinces;
                    provinces = _provincialService.GetProvincial(page, pageSize, keyword, out totalRow).ToList();
                    var model = Mapper.Map<IEnumerable<Provincial>, IEnumerable<ProvinceModel>>(provinces);
                    foreach (var item in model)
                    {
                        item.CountryName = _countryService.GetById(item.CountryId).Name;
                    }
                    PaginationSet<ProvinceModel> pagedSet = new PaginationSet<ProvinceModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = model
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerProvince))
            {
                return CreateHttpResponse(request, () =>
                {
                    var provinces = _provincialService.GetAll();
                    var model = Mapper.Map<IEnumerable<Provincial>, IEnumerable<ProvinceModel>>(provinces);
                    return request.CreateResponse(HttpStatusCode.OK, model);
                });
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerProvince))
            {
                var province = _provincialService.GetById(Id);
                var model = Mapper.Map<Provincial, ProvinceModel>(province);
                return request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, ProvinceModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerCountry))
            {
                var province = _provincialService.GetById(model.Id);
                if (province != null)
                {
                    province.Name = model.Name;
                    province.SortName = model.SortName;
                    province.CountryId = model.CountryId;
                    _provincialService.Update(province);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Đã có lỗi xảy ra");
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("import")]
        [HttpPost]
        public async Task<HttpResponseMessage> Import(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerProvince))
            {
                HttpResponseMessage response = null;
                foreach (string file in HttpContext.Current.Request.Files)
                {
                    var fileupload = HttpContext.Current.Request.Files[file];
                    using (var package = new ExcelPackage(fileupload.InputStream))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        List<Provincial> listProvince = new List<Provincial>();
                        Provincial province;
                        for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)
                        {
                            if (_provincialService.GetAll().Where(x => x.Name == workSheet.Cells[i, 2].Value.ToString()).Count() == 0)
                            {
                                province = new Provincial();
                                province.Name = workSheet.Cells[i, 2].Value.ToString();
                                province.CountryId = int.Parse(HttpContext.Current.Request.Form["CountryId"]);
                                _provincialService.Insert(province);
                            }
                        }
                    }
                }
                response = request.CreateResponse(HttpStatusCode.OK, "Nhập dữ liệu thành công");
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}