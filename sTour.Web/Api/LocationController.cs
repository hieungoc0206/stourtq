﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/location")]
    [Authorize]
    public class LocationController : ApiControllerBase
    {
        private readonly ILocationService _locationService;
        public LocationController(ILocationService locationService,ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._locationService = locationService;
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                var location = _locationService.GetById(Id);
                return request.CreateResponse(HttpStatusCode.OK, location);
            }
             else
            {
                return UnAuthorized();
            }  
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    LocationModel model = ConvertFormDataToModel(result.FormData);
                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Location/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Location/Avatar/" + fileName;
                    }

                    var location = new Location();
                    //
                    location.Name = model.Name;
                    location.NameEn = model.NameEn;
                    location.Alias = model.Alias;
                    location.AliasEn = model.AliasEn;
                    location.Address = model.Address;
                    location.AddressEn = model.AddressEn;
                    location.Avatar = model.Avatar;
                    location.Latitude = model.Latitude;
                    location.Longitude = model.Longitude;
                    location.LongDes = model.LongDes;
                    location.LongDesEn = model.LongDesEn;
                    location.ShortDes = model.ShortDes;
                    location.ShortDesEn = model.ShortDesEn;
                    location.ViewCount = model.ViewCount;
                    location.VideoDir = model.VideoDir;
                    location.VideoDirEn = model.VideoDirEn;
                    location.isActive = model.isActive;
                    location.CreatedAt = DateTime.Now;
                    location.Audio = model.Audio;
                    location.AudioEn = model.AudioEn;
                    location.OpenDate = model.OpenDate;
                    location.PhoneNumber = model.PhoneNumber;                 
                    location.LocationCategoryId = model.LocationCategoryId;                    
                    _locationService.Insert(location);
                    location.Latitude = model.Latitude;
                    location.Longitude = model.Longitude;
                    //_locationService.UpdateLatLon(location.Id, location.Latitude, location.Longitude);
                    if (location.Id > 0)
                    {
                        string locationDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Location/Location-{0}", location.Id)));
                        if (!System.IO.Directory.Exists(locationDirectory))
                        {
                            System.IO.Directory.CreateDirectory(locationDirectory);
                        }
                        var listFileAttach = new List<FileAttach>();
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Location/Location-{0}", location.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                var path = Path.Combine(locationDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);

                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttach.Add(fileAttach);
                        }

                        location.Images = JsonConvert.SerializeObject(listFileAttach);
                        _locationService.Update(location);
                    }

                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }

            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getall")]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                var locations = _locationService.GetAll().Select(c => new {
                    c.Id,
                    c.Name
                });
                return request.CreateResponse(HttpStatusCode.OK, locations);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPost]  
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    LocationModel model = ConvertFormDataToModel(result.FormData);
                    var location = _locationService.GetById(model.Id);
                    if (location == null)
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, false);
                    }

                    if (string.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else if (!model.Avatar.StartsWith("~/Content") && location.Avatar != model.Avatar)
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Location/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Location/Avatar/" + fileName;
                    }

                    //
                    location.Name = model.Name;
                    location.NameEn = model.NameEn;
                    location.Alias = model.Alias;
                    location.AliasEn = model.AliasEn;
                    location.Address = model.Address;
                    location.AddressEn = model.AddressEn;
                    location.Avatar = model.Avatar;
                    location.LongDes = model.LongDes;
                    location.LongDesEn = model.LongDesEn;
                    location.ShortDes = model.ShortDes;
                    location.ShortDesEn = model.ShortDesEn;
                    location.ViewCount = model.ViewCount;
                    location.Latitude = model.Latitude;
                    location.Longitude = model.Longitude;
                    location.VideoDir = model.VideoDir;
                    location.VideoDirEn = model.VideoDirEn;
                    location.isActive = model.isActive;
                    location.Audio = model.Audio;
                    location.AudioEn = model.AudioEn;
                    location.OpenDate = model.OpenDate;
                    location.PhoneNumber = model.PhoneNumber;                  
                    location.LocationCategoryId = model.LocationCategoryId;                    
                    //Process Images
                    List<FileAttach> listFileAttachNew = JsonConvert.DeserializeObject<List<FileAttach>>(model.Images);
                    List<FileAttach> listFileAttachAll = JsonConvert.DeserializeObject<List<FileAttach>>(String.IsNullOrEmpty(location.Images) ? "[]" : location.Images);
                    foreach (var image in listFileAttachAll)
                    {
                        if (!listFileAttachNew.Any(x => x.FileName == image.FileName))
                        {
                            string path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Location/Location-{0}", location.Id) + "/" + image.FileName));
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                            }
                        }
                    }
                    string locationDirectory = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Location/Location-{0}", location.Id)));
                    if (!Directory.Exists(locationDirectory))
                    {
                        Directory.CreateDirectory(locationDirectory);
                    }
                    foreach (string file in HttpContext.Current.Request.Files)
                    {
                        var fileAttach = new FileAttach();

                        var fileContent = HttpContext.Current.Request.Files[file];
                        string fileName = fileContent.FileName.Split('.')[0];
                        string fileType = fileContent.FileName.Split('.')[1];
                        fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                        fileAttach.TypeFile = fileType;
                        fileAttach.FileSize = fileContent.ContentLength.ToString();
                        fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                        fileAttach.FileUrl = string.Format("~/Content/Images/Location/Location-{0}", location.Id) + "/" + fileAttach.FileName;
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            var inputStream = fileContent.InputStream;
                            var path = Path.Combine(locationDirectory, fileAttach.FileName);
                            if (!System.IO.File.Exists(path))
                            {
                                using (var fileStream = System.IO.File.Create(path))
                                {
                                    Image image;
                                    using (image = System.Drawing.Image.FromStream(inputStream))
                                    {
                                        if (image.Width > 1000)
                                        {
                                            double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                            ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                            fileAttach.FileSize = fileStream.Length.ToString();
                                        }
                                        else
                                        {
                                            inputStream.Seek(0, SeekOrigin.Begin);
                                            inputStream.CopyTo(fileStream);

                                        }
                                    }
                                }
                            }
                        }
                        listFileAttachNew.Add(fileAttach);
                    }

                    location.Images = JsonConvert.SerializeObject(listFileAttachNew);
                    _locationService.Update(location);
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }

            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getlocations")]
        public async Task<HttpResponseMessage> GetLocations(HttpRequestMessage request, int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerLocation))
            {
                HttpResponseMessage response = null;
                int totalRow;             
                var locations = _locationService.GetLocations(page, pageSize, keyword, out totalRow).ToList();
                var model = Mapper.Map<IEnumerable<Location>, IEnumerable<LocationModel>>(locations);
                PaginationSet<LocationModel> pagedSet = new PaginationSet<LocationModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }

        }
        
        LocationModel ConvertFormDataToModel(NameValueCollection formData)
        {
            var model = new LocationModel();
            if(!String.IsNullOrEmpty(formData["Id"]))
            model.Id = int.Parse(formData["Id"]);
            model.Address = formData["Address"];
            model.AddressEn = formData["AddressEn"];
            if (formData["Audio"] != "null" && formData["Audio"] != "undefined")
            model.Audio = formData["Audio"];
            if (formData["AudioEn"] != "null" && formData["AudioEn"] != "undefined")
                model.AudioEn = formData["AudioEn"];
            model.Avatar = formData["Avatar"];
            if (!string.IsNullOrEmpty(formData["LocationCategoryId"]))
            {
                model.LocationCategoryId = int.Parse(formData["LocationCategoryId"]);
            }        
            model.Images = formData["Images"];
            model.isActive = bool.Parse(formData["isActive"]);
            model.Latitude = double.Parse(formData["Latitude"]);
            model.LongDes = formData["LongDes"];
            model.LongDesEn = formData["LongDesEn"];
            model.Longitude = double.Parse(formData["Longitude"]);
            model.Name = formData["Name"];
            model.NameEn = formData["NameEn"];
            model.Alias = formData["Alias"];
            model.AliasEn = formData["AliasEn"];
            model.OpenDate = formData["OpenDate"];
            model.PhoneNumber = formData["PhoneNumber"];
            model.ShortDes = formData["ShortDes"];
            model.ShortDesEn = formData["ShortDesEn"];
            model.VideoDir = formData["VideoDir"];
            model.VideoDirEn = formData["VideoDirEn"];
            model.ViewCount = int.Parse(formData["ViewCount"]);
            return model;
        }
    }
}
