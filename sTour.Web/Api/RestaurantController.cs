﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/restaurant")]
    [Authorize]
    public class RestaurantController : ApiControllerBase
    {
        private readonly ILocationService _locationService;
        private readonly IRestaurantService _restaurantService;
        private readonly IRestaurant_Mapping_LocationService _restaurant_mapping_locationService;
        private readonly IServiceRestaurant_Mapping_RestaurantService _service_mapping_restaurantService;
        private readonly IServiceRestaurantService _servicerestaurantService;

        public RestaurantController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService, ILocationService locationService,
            IRestaurantService restaurantService, IRestaurant_Mapping_LocationService restaurant_mapping_locationService,
            IServiceRestaurantService servicerestaurantService, IServiceRestaurant_Mapping_RestaurantService service_mapping_restaurantService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._locationService = locationService;
            this._restaurantService = restaurantService;
            this._restaurant_mapping_locationService = restaurant_mapping_locationService;
            this._servicerestaurantService = servicerestaurantService;
            this._service_mapping_restaurantService = service_mapping_restaurantService;
        }

        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerRestaurant))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    RestaurantModel model = ConvertFormDataToModel(result.FormData);
                    var user = User.Identity;
                    if (!_userManager.IsInRole(user.GetUserId(), SystemRole.Admin))
                    {
                        model.EnterpriseId = _userManager.Users.Where(c => c.Id == user.GetUserId()).SingleOrDefault().EnterpriseId.Value;
                    }

                    if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                    {
                        model.Avatar = "/Assets/images_none.png";
                    }
                    else
                    {
                        byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                        //Save the Byte Array as Image File.
                        string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                        string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Restaurant/Avatar"), fileName);
                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                        model.Avatar = "~/Content/Images/Restaurant/Avatar/" + fileName;
                    }

                    var restaurant = new Restaurant();
                    restaurant.Name = model.Name;
                    restaurant.Alias = model.Alias;
                    restaurant.Address = model.Address;
                    restaurant.NameEn = model.NameEn;
                    restaurant.AliasEn = model.AliasEn;
                    restaurant.AddressEn = model.AddressEn;
                    restaurant.Avatar = model.Avatar;
                    restaurant.Latitude = model.Latitude;
                    restaurant.Longitude = model.Longitude;
                    restaurant.LongDes = model.LongDes;
                    restaurant.ShortDes = model.ShortDes;
                    restaurant.LongDesEn = model.LongDesEn;
                    restaurant.ShortDesEn = model.ShortDesEn;
                    restaurant.ViewCount = model.ViewCount;
                    restaurant.Star = model.Star;
                    restaurant.VideoDir = model.VideoDir;
                    restaurant.HotLine = model.HotLine;
                    restaurant.Price = model.Price;
                    restaurant.isActive = model.isActive;
                    restaurant.EnterpriseId = model.EnterpriseId;
                    restaurant.Audio = model.Audio;
                    restaurant.OpenDate = model.OpenDate;
                    _restaurantService.Insert(restaurant);

                    //    _restaurantService.UpdateLatLon(restaurant.Id, restaurant.Latitude, restaurant.Longitude);
                    if (restaurant.Id > 0)
                    {
                        //Save file
                        string restaurantDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Restaurant/Restaurant-{0}", restaurant.Id)));
                        if (!System.IO.Directory.Exists(restaurantDirectory))
                        {
                            System.IO.Directory.CreateDirectory(restaurantDirectory);
                        }
                        var listFileAttach = new List<FileAttach>();
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/Restaurant/Restaurant-{0}", restaurant.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(restaurantDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttach.Add(fileAttach);
                        }

                        restaurant.Images = JsonConvert.SerializeObject(listFileAttach);
                        _restaurantService.Update(restaurant);

                        //Insert table mapping location
                        List<string> locations = model.locationIds.Split(',').ToList();

                        foreach (string location in locations)
                        {
                            try
                            {
                                var locationmapping = new Restaurant_Mapping_Location();
                                locationmapping.RestaurantId = restaurant.Id;
                                locationmapping.LocationId = Convert.ToInt32(location);
                                _restaurant_mapping_locationService.Insert(locationmapping);
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        //Insert table mapping service
                        List<string> services = model.serviceIds.Split(',').ToList();
                        foreach (string service in services)
                        {
                            try
                            {
                                var servicemapping = new ServiceRestaurant_Mapping_Restaurant();
                                servicemapping.RestaurantId = restaurant.Id;
                                servicemapping.ServiceRestaurantId = Convert.ToInt32(service);
                                _service_mapping_restaurantService.Insert(servicemapping);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }

                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getrestaurants")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetRestaurants(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerRestaurant))
            {
                HttpResponseMessage response = null;
                int totalRow;
             
                var userId = User.Identity.GetUserId();
                dynamic restaurants;
                if (_userManager.IsInRole(userId, SystemRole.Admin))
                {
                    restaurants = _restaurantService.GetRestaurants(null, page, pageSize, keyword, out totalRow).ToList();
                }
                else
                {
                    restaurants = _restaurantService.GetRestaurants(_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0, page, pageSize, keyword, out totalRow).ToList();
                }

                var model = Mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantModel>>(restaurants);
                PaginationSet<RestaurantModel> pagedSet = new PaginationSet<RestaurantModel>()
                {
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request, int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerRestaurant))
            {
                var restaurant = _restaurantService.GetById(Id);
                var userId = User.Identity.GetUserId();
                if (!_userManager.IsInRole(userId, SystemRole.Admin))
                {
                    int enterpriseId = restaurant.EnterpriseId ?? 0;
                    if (enterpriseId == 0)
                    {
                        return UnAuthorized();
                    }
                    if ((_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0) != enterpriseId)
                    {
                        return UnAuthorized();
                    }
                }

                var locationByRestaurantId = _locationService.GetByRestaurantId(Id).Select(c => new
                {
                    c.Id
                });

                var locationAll = _locationService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name,
                    _checked = (locationByRestaurantId.Any(x => x.Id == c.Id) ? true : false)
                });
                var serviceByRestaurantId = _servicerestaurantService.GetByRestaurantId(Id).Select(c => new
                {
                    c.Id
                });
                var serviceAll = _servicerestaurantService.GetAll().Select(c => new
                {
                    c.Id,
                    c.Name,
                    _checked = (serviceByRestaurantId.Any(x => x.Id == c.Id) ? true : false)
                });
                object data = new
                {
                    data = restaurant,
                    services = serviceAll,
                    locations = locationAll
                };
               return request.CreateResponse(HttpStatusCode.OK, data);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]       
        [HttpPost]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerRestaurant))
            {
                try
                {
                    if (!Request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }
                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
                    var provider = new MultipartFormDataStreamProvider(root);
                    var result = await Request.Content.ReadAsMultipartAsync(provider);
                    RestaurantModel model = ConvertFormDataToModel(result.FormData);
                    var restaurant = _restaurantService.GetById(model.Id);
                    if (restaurant != null)
                    {
                        var userId = User.Identity.GetUserId();
                        if (!_userManager.IsInRole(userId, SystemRole.Admin))
                        {
                            int enterpriseId = restaurant.EnterpriseId ?? 0;
                            if (enterpriseId == 0)
                            {
                                return UnAuthorized();
                            }
                            if ((_userManager.Users.Where(u => u.Id == userId).ToList()[0].EnterpriseId ?? 0) != enterpriseId)
                            {
                                return UnAuthorized();
                            }
                        }
                        else
                        {
                            restaurant.EnterpriseId = model.EnterpriseId;
                        }

                        if (String.IsNullOrEmpty(model.Avatar) || model.Avatar.Contains("images_none.png"))
                        {
                            model.Avatar = "/Assets/images_none.png";
                        }
                        else if (!model.Avatar.StartsWith("~/Content") && restaurant.Avatar != model.Avatar)
                        {
                            byte[] imageBytes = Convert.FromBase64String(model.Avatar.Split(',')[1]);

                            //Save the Byte Array as Image File.
                            string fileName = StringHepler.ToUnsignString(model.Name) + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".png";
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Images/Restaurant/Avatar"), fileName);
                            System.IO.File.WriteAllBytes(filePath, imageBytes);
                            model.Avatar = "~/Content/Images/Restaurant/Avatar/" + fileName;
                        }

                        restaurant.Name = model.Name;
                        restaurant.Alias = model.Alias;
                        restaurant.Address = model.Address;
                        restaurant.NameEn = model.NameEn;
                        restaurant.AliasEn = model.AliasEn;
                        restaurant.AddressEn = model.AddressEn;
                        restaurant.Avatar = model.Avatar;
                        restaurant.Latitude = model.Latitude;
                        restaurant.Longitude = model.Longitude;
                        restaurant.LongDes = model.LongDes;
                        restaurant.ShortDes = model.ShortDes;
                        restaurant.LongDesEn = model.LongDesEn;
                        restaurant.ShortDesEn = model.ShortDesEn;
                        restaurant.ViewCount = model.ViewCount;
                        restaurant.VideoDir = model.VideoDir;
                        restaurant.HotLine = model.HotLine;
                        restaurant.Star = model.Star;
                        restaurant.Price = model.Price;
                        restaurant.isActive = model.isActive;
                        restaurant.Audio = model.Audio;
                        restaurant.OpenDate = model.OpenDate;
                        //    _restaurantService.UpdateLatLon(restaurant.Id, restaurant.Latitude, restaurant.Longitude);
                        string restaurantDirectory = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/Content/Images/Restaurant/Restaurant-{0}", restaurant.Id)));
                        if (!System.IO.Directory.Exists(restaurantDirectory))
                        {
                            System.IO.Directory.CreateDirectory(restaurantDirectory);
                        }
                        var listFileAttachNew = JsonConvert.DeserializeObject<List<FileAttach>>(model.Images);
                        var listFileAttachOld = JsonConvert.DeserializeObject<List<FileAttach>>(string.IsNullOrEmpty(restaurant.Images) ? "[]" : restaurant.Images);
                        foreach (var file in listFileAttachOld)
                        {
                            if (!listFileAttachNew.Any(f => f.FileName == file.FileName))
                            {
                                string path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/Content/Images/Restaurant/Restaurant-{0}", restaurant.Id) + "/" + file.FileName));
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                            }
                        }
                        foreach (string file in HttpContext.Current.Request.Files)
                        {
                            var fileAttach = new FileAttach();

                            var fileContent = HttpContext.Current.Request.Files[file];
                            string fileName = fileContent.FileName.Split('.')[0];
                            string fileType = fileContent.FileName.Split('.')[1];
                            fileAttach.FileName = fileName + "-" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "." + fileType;
                            fileAttach.TypeFile = fileType;
                            fileAttach.FileSize = fileContent.ContentLength.ToString();
                            fileAttach.TimeCreate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                            fileAttach.FileUrl = String.Format("~/Content/Images/restaurant/restaurant-{0}", restaurant.Id) + "/" + fileAttach.FileName;
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                var inputStream = fileContent.InputStream;
                                //   var fileName = Path.GetFileName(file);
                                var path = Path.Combine(restaurantDirectory, fileAttach.FileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        Image image;
                                        using (image = System.Drawing.Image.FromStream(inputStream))
                                        {
                                            if (image.Width > 1000)
                                            {
                                                double newHeight = ((double)image.Height / (double)image.Width) * 1000;
                                                ImageHepler.Resize(inputStream, fileStream, 1000, (int)newHeight);
                                                fileAttach.FileSize = fileStream.Length.ToString();
                                            }
                                            else
                                            {
                                                inputStream.Seek(0, SeekOrigin.Begin);
                                                inputStream.CopyTo(fileStream);
                                                fileStream.Close();
                                            }
                                        }
                                    }
                                }
                            }
                            listFileAttachNew.Add(fileAttach);
                        }

                        restaurant.Images = JsonConvert.SerializeObject(listFileAttachNew);
                        _restaurantService.Update(restaurant);
                        _restaurant_mapping_locationService.DeleteByRestaurantId(restaurant.Id);
                        //Insert table mapping location
                        List<string> locations = model.locationIds.Split(',').ToList();

                        foreach (string location in locations)
                        {
                            try
                            {
                                var locationmapping = new Restaurant_Mapping_Location();
                                locationmapping.RestaurantId = restaurant.Id;
                                locationmapping.LocationId = Convert.ToInt32(location);
                                _restaurant_mapping_locationService.Insert(locationmapping);
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        //Insert table mapping service
                        _service_mapping_restaurantService.DeleteByRestaurantId(restaurant.Id);
                        List<string> services = model.serviceIds.Split(',').ToList();
                        foreach (string service in services)
                        {
                            try
                            {
                                var servicemapping = new ServiceRestaurant_Mapping_Restaurant();
                                servicemapping.RestaurantId = restaurant.Id;
                                servicemapping.ServiceRestaurantId = Convert.ToInt32(service);
                                _service_mapping_restaurantService.Insert(servicemapping);
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.OK, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        RestaurantModel ConvertFormDataToModel(NameValueCollection formData)
        {
            var model = new RestaurantModel();
            if (!String.IsNullOrEmpty(formData["Id"]))
                model.Id = int.Parse(formData["Id"]);
            model.Address = formData["Address"];
            model.AddressEn = formData["AddressEn"];
            model.Avatar = formData["Avatar"];
            model.Images = formData["Images"];
            model.isActive = bool.Parse(formData["isActive"]);
            model.Latitude = double.Parse(formData["Latitude"]);
            model.LongDes = formData["LongDes"];
            model.LongDesEn = formData["LongDesEn"];
            model.Longitude = double.Parse(formData["Longitude"]);
            model.Name = formData["Name"];
            model.Alias = formData["Alias"];
            model.NameEn = formData["NameEn"];
            model.AliasEn = formData["AliasEn"];
            model.HotLine = formData["HotLine"];
            model.Star = int.Parse(formData["Star"]);
            model.ShortDes = formData["ShortDes"];
            model.ShortDesEn = formData["ShortDesEn"];
            model.VideoDir = formData["VideoDir"];
            model.ViewCount = int.Parse(formData["ViewCount"]);
            if (formData["EnterpriseId"] != "null" && formData["EnterpriseId"] != "undefined")
                model.EnterpriseId = int.Parse(formData["EnterpriseId"]);
            if (formData["Audio"] != "null" && formData["Audio"] != "undefined")
                model.Audio = formData["Audio"];
            model.OpenDate = formData["OpenDate"];
            model.locationIds = formData["locationIds"];
            model.serviceIds = formData["serviceIds"];
            return model;
        }
    }
}
