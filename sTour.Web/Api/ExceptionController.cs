﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Api
{
    [Authorize]
    [RoutePrefix("api/exception")]
    public class ExceptionController : ApiControllerBase
    {
       
        public ExceptionController(ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {            
        }

        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request,int page, int pageSize)
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    page = page - 1;
                    var items = _errorService.GetExceptionLog(page, pageSize);
                    var logs = items.Select(c =>
                    {
                        var model = Mapper.Map<ExceptionLog, ExceptionErrorModel>(c);
                        model.ExceptionMessage = model.ExceptionMessage.Length > 70 ? model.ExceptionMessage.Substring(0, 70) : model.ExceptionMessage;
                        model.ExceptionStackTrace = model.ExceptionStackTrace.Length > 150 ? model.ExceptionStackTrace.Substring(0, 150) : model.ExceptionStackTrace;
                        return model;
                    });
                    int totalRow = _errorService.Count();
                    PaginationSet<ExceptionErrorModel> pagedSet = new PaginationSet<ExceptionErrorModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = logs
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });
                  
            }
            else
            {
                return UnAuthorized();
            }
        }

    }
}
