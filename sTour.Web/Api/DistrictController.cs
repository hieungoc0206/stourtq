﻿using AutoMapper;
using OfficeOpenXml;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace sTour.Web.Api
{
    [RoutePrefix("api/district")]
    [Authorize]
    public class DistrictController : ApiControllerBase
    {
        private IProvincialService _provincialService;
        private IDistrictService _districtService;

        public DistrictController(IProvincialService provincialService, IDistrictService districtService, ApplicationUserManager userManager, IExceptionLogService errorService, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService) : base(errorService, userManager, permissionMappingService, permissonService, activityLogService)
        {
            this._provincialService = provincialService;
            this._districtService = districtService;
        }

        [Route("create")]       
        [HttpPost]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request,DistrictModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {
                try
                {
                    if (_districtService.GetAll().Where(x => x.Name == model.Name || x.SortName == model.SortName).Count() == 0)
                    {
                        var district = new District();
                        district.Name = model.Name;
                        district.SortName = model.SortName;
                        district.ProvincialId = model.ProvincialId;
                        _districtService.Insert(district);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.OK, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateResponse(HttpStatusCode.OK, false);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }
        [Route("getall")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAll(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {                
                var districts = _districtService.GetAll();
                var model = Mapper.Map<IEnumerable<District>, IEnumerable<DistrictModel>>(districts);
                return request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getdistricts")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetDistricts(HttpRequestMessage request,int page, int pageSize, string keyword)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {
                return CreateHttpResponse(request, () =>
                {
                    HttpResponseMessage response = null;
                    int totalRow;
                    if (page >= 1)
                        page = page - 1;
                    dynamic districts;
                    districts = _districtService.GetDistrict(page, pageSize, keyword, out totalRow).ToList();
                    var model = Mapper.Map<IEnumerable<District>, IEnumerable<DistrictModel>>(districts);
                    foreach (var item in model)
                    {
                        item.ProvinceName = _provincialService.GetById(item.ProvincialId).Name;
                    }
                    PaginationSet<DistrictModel> pagedSet = new PaginationSet<DistrictModel>()
                    {
                        Page = page,
                        TotalCount = totalRow,
                        TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),
                        Items = model
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                    return response;
                });                
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("getbyid/{id:int}")]
        public async Task<HttpResponseMessage> GetById(HttpRequestMessage request,int Id)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {
                var district = _districtService.GetById(Id);
                var model = Mapper.Map<District, DistrictModel>(district);
                return request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("edit")]
        [HttpPut]
        public async Task<HttpResponseMessage> Edit(HttpRequestMessage request, DistrictModel model)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {
                try
                {
                    var district = _districtService.GetById(model.Id);
                    if (district != null)
                    {
                        district.Name = model.Name;
                        district.SortName = model.SortName;
                        district.ProvincialId = model.ProvincialId;
                        _districtService.Update(district);
                        return request.CreateResponse(HttpStatusCode.OK, true);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.OK, false);
                    }
                }
                catch (Exception ex)
                {
                    return request.CreateResponse(HttpStatusCode.OK, false);
                }
            }
            else
            {
                return UnAuthorized();
            }
        }

        [Route("import")]
        [HttpPost]
        public async Task<HttpResponseMessage> Import(HttpRequestMessage request)
        {
            if (await CheckPermission(SystemPermission.ManagerDistrict))
            {
                foreach (string file in HttpContext.Current.Request.Files)
                {
                    var fileupload = HttpContext.Current.Request.Files[file];
                    using (var package = new ExcelPackage(fileupload.InputStream))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        List<District> listDistrict = new List<District>();
                        District district;
                        for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                        {
                            if (_districtService.GetAll().Where(x => x.Name == workSheet.Cells[i, 3].Value.ToString()).Count() == 0)
                            {
                                district = new District();
                                district.Name = workSheet.Cells[i, 3].Value.ToString();
                                district.ProvincialId = int.Parse(HttpContext.Current.Request.Form["ProvincialId"]);
                                _districtService.Insert(district);
                            }
                        }
                    }
                }
                return request.CreateResponse(HttpStatusCode.OK, true);
            }
            else
            {
                return UnAuthorized();
            }
        }
    }
}
