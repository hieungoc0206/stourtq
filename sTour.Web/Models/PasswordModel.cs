﻿namespace sTour.Web.Models
{
    public class PasswordModel
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string VerityPassword { get; set; }
    }
}