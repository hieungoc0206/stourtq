﻿using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class HomeViewModel
    {
        public IEnumerable<HotelModel> Hotels { get; set; }

        public IEnumerable<RestaurantModel> Restaurants { get; set; }

        public IEnumerable<LocationModel> Locations { get; set; }
    }
}