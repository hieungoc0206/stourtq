﻿namespace sTour.Web.Models
{
    public class StarModel
    {
        public int Star { get; set; }

        public bool Check { get; set; }
    }
}