﻿using sTour.Entity;

namespace sTour.Web.Models
{
    public class DistrictModel : District
    {
        public string ProvinceName { get; set; }
    }
}