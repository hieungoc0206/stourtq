﻿using sTour.Entity;

namespace sTour.Web.Models
{
    public class RoleModel : ApplicationRole
    {
        public bool Checked { get; set; }
    }
}