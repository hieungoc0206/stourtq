﻿namespace sTour.Web.Models
{
    public class RegisterModel
    {
        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string VerifyPassword { get; set; }

        public bool Gender { get; set; }

        public int CountryId { get; set; }

        public string NewPassword { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string BirthDay { get; set; }
    }
}