﻿using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class RoleUserModel
    {
        public string UserId { get; set; }

        public List<RoleModel> Roles = new List<RoleModel>();
    }
}