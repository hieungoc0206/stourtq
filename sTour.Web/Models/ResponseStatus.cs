﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class ResponseStatus
    {
        public const int Success = 1;
        public const int Error = 2;
        public const int AccessDenied = 3;
    }
}