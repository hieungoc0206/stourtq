﻿using sTour.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class RestaurantModel : Restaurant
    {
        public List<FileAttach> FileAttachs { get; set; }

        public string locationIds { get; set; }

        public string serviceIds { get; set; }
        public RestaurantModel()
        {
            FileAttachs = new List<FileAttach>();
        }
    }
}