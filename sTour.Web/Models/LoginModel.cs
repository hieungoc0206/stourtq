﻿namespace sTour.Web.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }

        public string PassWord { get; set; }

        public bool RememberMe { get; set; }
    }
}