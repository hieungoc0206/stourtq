﻿using System;
using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class SystemInfoModel
    {
        public string AspNetInfo { get; set; }

        public string IsFullTrust { get; set; }

        public string Version { get; set; }

        public string OperatingSystem { get; set; }

        public DateTime ServerLocalTime { get; set; }

        public string ServerTimeZone { get; set; }

        public DateTime UtcTime { get; set; }

        public string HttpHost { get; set; }

        public List<LoadedAssembly> LoadedAssemblies = new List<LoadedAssembly>();

        public partial class LoadedAssembly
        {
            public string FullName { get; set; }
            public string Location { get; set; }
        }
    }
}