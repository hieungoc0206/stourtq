﻿using sTour.Entity;
using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class FoodModel : Foods
    {
        public List<FileAttach> FileAttachs { get; set; }

        public string DistrictName { get; set; }

        public FoodModel()
        {
            FileAttachs = new List<FileAttach>();
        }
    }
}