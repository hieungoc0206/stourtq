﻿using sTour.Entity;

namespace sTour.Web.Models
{
    public class ProvinceModel : Provincial
    {
        public string CountryName { get; set; }
    }
}