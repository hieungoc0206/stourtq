﻿using sTour.Entity;
using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class LocationModel : Location
    {
       
        public List<FileAttach> FileAttachs { get; set; }


        public LocationModel()
        {
            FileAttachs = new List<FileAttach>();
        }
    }
}