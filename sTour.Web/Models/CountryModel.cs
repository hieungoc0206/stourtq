﻿using sTour.Entity;

namespace sTour.Web.Models
{
    public class CountryModel 
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PhoneCode { get; set; }

        public string SortName { get; set; }
    }
}