﻿using sTour.Entity;
using System;

namespace sTour.Web.Models
{
    public class UserModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public bool LockoutEnabled { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public string Address { get; set; }

        public DateTime? BirthDay { get; set; }

        public DateTime? LoginEndDate { get; set; }

        public bool Gender { get; set; }

        public string LoginEndIP { get; set; }

        public int LoginCount { get; set; }

        public int? CountryId { get; set; }

        public string CountryName { get; set; }

        public Country Country { get; set; }

        public int? EnterpriseId { get; set; }

    }
}