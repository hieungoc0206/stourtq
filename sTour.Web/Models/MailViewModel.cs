﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class MailViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Subject { get; set; }

        public string Content { get; set; }
    }
}