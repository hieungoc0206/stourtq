﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public int TotalPage { get; set; }
        public ResponseModel()
        {

        }
        public ResponseModel(string status, string message, object data, int pages)
        {
            Status = status;
            Message = message;
            Data = data;
            TotalPage = pages;
        }
    }
}