﻿using sTour.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class HotelModel : Hotel
    {
        public List<FileAttach> FileAttachs { get; set; }

        public string locationIds { get; set; }

        public string serviceIds { get; set; }
        public HotelModel()
        {
            FileAttachs = new List<FileAttach>();
        }
    }
}