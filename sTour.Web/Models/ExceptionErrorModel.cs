﻿using System;

namespace sTour.Web.Models
{
    public class ExceptionErrorModel
    {
        public int Id { get; set; }

        public string ExceptionMessage { get; set; }

        public string ControllerName { get; set; }

        public string ExceptionStackTrace { get; set; }

        public string ExceptionMessageFull { get; set; }

        public string ExceptionStackTraceFull { get; set; }

        public DateTime LogTime { get; set; }
    }
}