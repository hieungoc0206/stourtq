﻿using sTour.Entity;

namespace sTour.Web.Models
{
    public class ServiceRestaurantModel : ServiceRestaurant
    {
        public bool Check { get; set; }
    }
}