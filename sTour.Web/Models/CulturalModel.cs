﻿using sTour.Entity;
using System.Collections.Generic;

namespace sTour.Web.Models
{
    public class CulturalModel : Cultural
    {
        public string DistrictName { get; set; }
        public List<FileAttach> FileAttachs { get; set; }

        public CulturalModel()
        {
            FileAttachs = new List<FileAttach>();
        }
    }
}