﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace sTour.Web.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage ="Nhập tên")]
        public string FullName { get; set; }

        [Required(ErrorMessage ="Nhập tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Nhập mật khẩu")]
        [MinLength(6, ErrorMessage ="Mật khẩu tối thiểu 6 ký tự")]
        public string Password { get; set; }

        [MinLength(6, ErrorMessage = "Mật khẩu tối thiểu 6 ký tự")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage ="Bạn cần nhập Email")]
        [EmailAddress(ErrorMessage ="Invalid emali address")]
        public string Email { get; set; }

        public string Address { get; set; }
        
        [Required(ErrorMessage ="Nhập số điện thoại")]
        public string PhoneNumber { get; set; }

    }
}