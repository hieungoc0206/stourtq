﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sTour.Web.Models
{
    public class FileAttach
    {
        public string FileName { get; set; }
        public string TypeFile { get; set; }
        public string FileUrl { get; set; }
        public string FileSize { get; set; }
        public string TimeCreate { get; set; }
    }
}