﻿using AutoMapper;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class RestaurantController : BaseController
    {
        private IRestaurantService _restaurantService;
        private IServiceRestaurantService _serviceRestaurantService;

        public RestaurantController(IRestaurantService restaurantService, ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService, IServiceRestaurantService serviceRestaurantService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._restaurantService = restaurantService;
            this._serviceRestaurantService = serviceRestaurantService;
        }

        // GET: Restaurant
        public ActionResult Index(string keyword, string sort = null, string prices = null, string stars = null, string services = null, int page = 1)
        {
            int pageSize = 6;
            int totalRow = 0;
            int fromPrice = 0;
            int toPrice = 0;
            List<int> starChecked = new List<int>();
            List<int> serviceChecked = new List<int>();
            if (!String.IsNullOrEmpty(stars))
            {
                stars.Split('_').ToList().ForEach(x =>
                {
                    starChecked.Add(int.Parse(x));
                });
            }
            if (!String.IsNullOrEmpty(services))
            {
                services.Split('_').ToList().ForEach(x =>
                {
                    serviceChecked.Add(int.Parse(x));
                });
            }
            if (!String.IsNullOrEmpty(prices))
            {
                fromPrice = int.Parse(prices.Split('-')[0]);
                toPrice = int.Parse(prices.Split('-')[1]);
            }
            var restaurants = _restaurantService.GetRestaurantPagings(page, pageSize, keyword, sort, fromPrice, toPrice, starChecked, serviceChecked, out totalRow).ToList();
            var restaurantModel = Mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantModel>>(restaurants);
            int totalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            ViewBag.Services = _serviceRestaurantService.GetAll();
            var paginationSet = new PaginationSet<RestaurantModel>()
            {
                Items = restaurantModel,
                MaxPage = 3,
                Page = page,
                TotalCount = totalRow,
                TotalPages = totalPage
            };
            return View(paginationSet);
        }

        public ActionResult Detail(int Id)
        {
            var restaurant = _restaurantService.GetById(Id);
            var model = Mapper.Map<Restaurant, RestaurantModel>(restaurant);
            return View(model);
        }
    }
}