﻿using sTour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class TouristServiceController : BaseController
    {
        private ITouristServiceService _touristServiceService;

        public TouristServiceController(ITouristServiceService touristServiceService):base(null, null, null, null, null)
        {
            this._touristServiceService = touristServiceService;
        }
        // GET: TouristService
        public ActionResult Index()
        {
            var touristService = _touristServiceService.GetAll();
            return View(touristService);
        }
    }
}