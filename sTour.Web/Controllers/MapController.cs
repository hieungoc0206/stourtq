﻿using AutoMapper;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class MapController : BaseController
    {
        private readonly IPOIService _poiService;
        private readonly ICategoryLocationService _categoryLocationService;

        public MapController(IPOIService poiService, ICategoryLocationService categoryLocationService):base(null, null, null, null, null)
        {
            this._poiService = poiService;
            this._categoryLocationService = categoryLocationService;
        }
        // GET: Map
        public ActionResult Index()
        {
            var items = _categoryLocationService.GetAll();
            var categories = new List<SelectListItem>();
            var itemTop = new SelectListItem();
            itemTop.Text = Resources.General.All;
            itemTop.Value = "0";
            categories.Add(itemTop);
            foreach (var category in items)
            {
                var item = new SelectListItem();
                item.Text = Common.CommonConstants.CurrentCulture.Equals("vi")? category.Name : category.NameEn;
                item.Value = category.Id.ToString();
                categories.Add(item);
            }
            ViewBag.Categories = categories;
            return View();
        }

        [HttpGet]
        public ActionResult GetPois()
        {
            var pois = Mapper.Map<IEnumerable<POI>, IEnumerable<POIModel>>(_poiService.GetAll());
            return Json(pois, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InsertPOI(string name,double lat,double lon)
        {
            var poi = new POI();
            poi.Name = name;
            poi.Latitude = lat;
            poi.Longitude = lon;
            _poiService.Insert(poi);
            return Json(new { data = true }, JsonRequestBehavior.AllowGet);
        }
    }
}