﻿using AutoMapper;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class CulturalController : BaseController
    {
        private ICulturalService _culturalService;

        public CulturalController(ICulturalService culturalService, ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._culturalService = culturalService;
        }

        public ActionResult Index(string keyword, int page = 1)
        {
            int pageSize = 6;
            int totalRow = 0;
            var culturals = _culturalService.GetCulturals(page, pageSize, keyword, out totalRow).ToList();
            var culturalModel = Mapper.Map<IEnumerable<Cultural>, IEnumerable<CulturalModel>>(culturals);
            int totalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            var paginationSet = new PaginationSet<CulturalModel>()
            {
                Items = culturalModel,
                MaxPage = 3,
                Page = page,
                TotalCount = totalRow,
                TotalPages = totalPage
            };
            return View(paginationSet);
        }

        public ActionResult Detail(int Id)
        {
            var cultural = _culturalService.GetById(Id);
            var model = Mapper.Map<Cultural, CulturalModel>(cultural);
            return View(model);
        }
    }
}