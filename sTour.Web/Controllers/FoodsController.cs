﻿using AutoMapper;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class FoodsController : BaseController
    {
        private IFoodService _foodService;

        public FoodsController(IFoodService foodService, ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._foodService = foodService;
        }

        public ActionResult Index(string keyword, int page = 1)
        {
            int pageSize = 6;
            int totalRow = 0;
            var foods = _foodService.GetFoods(page, pageSize, keyword, out totalRow).ToList();
            var foodModel = Mapper.Map<IEnumerable<Foods>, IEnumerable<FoodModel>>(foods);
            int totalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            var paginationSet = new PaginationSet<FoodModel>()
            {
                Items = foodModel,
                MaxPage = 3,
                Page = page,
                TotalCount = totalRow,
                TotalPages = totalPage
            };
            return View(paginationSet);
        }

        public ActionResult Detail(int Id)
        {
            var food = _foodService.GetById(Id);
            var model = Mapper.Map<Foods, FoodModel>(food);
            return View(model);
        }
    }
}