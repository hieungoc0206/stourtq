﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using sTour.Common;
using sTour.Data;
using sTour.Entity;
using sTour.Service;
using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace sTour.Web.Controllers
{

    [RequireHttps]
    public class BaseController : Controller
    {
        #region Field

        protected ApplicationSignInManager _signInManager;
        protected ApplicationUserManager _userManager;
        protected IRole_Permisstion_MappingService _permissonMappingService;
        protected IPermissionService _permissionService;
        protected IActivityLogService _activityLogService;

        #endregion Field

        #region Ctr

        public BaseController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            this._permissionService = permissonService;
            this._permissonMappingService = permissionMappingService;
            this._activityLogService = activityLogService;
        }

        #endregion Ctr

        #region Properties

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion Properties

        //Initilizing culture on controller initialization
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            //if (Session[CommonConstants.CurrentCulture] != null)
            //{
            //    Thread.CurrentThread.CurrentCulture = new CultureInfo(Session[CommonConstants.CurrentCulture].ToString());
            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session[CommonConstants.CurrentCulture].ToString());
            //}   
            //else
            //{
            //    Session[CommonConstants.CurrentCulture] = "vi";
            //    Thread.CurrentThread.CurrentCulture = new CultureInfo("vi");
            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi");
            //}
            if (Common.CommonConstants.CurrentCulture == null)
                SetCulture("vi");
            else
                SetCulture(Common.CommonConstants.CurrentCulture);
        }

        //Set culture
        private void SetCulture(String culture)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            CommonConstants.CurrentCulture = culture;

            if (HttpContext.Request.Cookies["_Culture"] != null)
            {
                HttpCookie cultureCookie = new HttpCookie("_Culture", culture);

                HttpContext.Response.SetCookie(cultureCookie);
            }
            else
            {
                HttpCookie cookie = new HttpCookie("_Culture");
                cookie.Value = culture;

                HttpContext.Response.Cookies.Add(cookie);
            }
        }

        //GetCulture
        [HttpGet]
        public ActionResult GetCultureCookieValue(HttpContext httpContext)
        {
            HttpCookie cultureCookie = httpContext.Request.Cookies["_Culture"];

            if (cultureCookie != null)
                return Json(cultureCookie, JsonRequestBehavior.AllowGet);
            else
                return Json(httpContext.Request.UserLanguages[0], JsonRequestBehavior.AllowGet); // obtain it from HTTP header AcceptLanguages
        }

        //Changing culture
        [HttpPost]
        public ActionResult ChangeCulture(string ddlCulture, string returnUrl)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo(ddlCulture);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(ddlCulture);

            //Session[CommonConstants.CurrentCulture] = ddlCulture;
            SetCulture(ddlCulture);
            return Redirect(returnUrl);
        }

        // GET: Base
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return new HttpStatusCodeResult(401);
        }

        public ActionResult Forbidden()
        {
            return View();
        }

        public ActionResult InternalServerError()
        {
            return View();
        }

        public ActionResult PermisstionDenied()
        {
            return new HttpStatusCodeResult(550);
        }

        public async Task<bool> CheckPermission(string permisstionName)
        {
            if (String.IsNullOrEmpty(permisstionName))
            {
                return false;
            }
            if (Request.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                ApplicationUser user = await UserManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return false;
                }
                if (user.LockoutEnabled)
                {
                    return false;
                }
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var userRoles = await UserManager.GetRolesAsync(userId);
                foreach (var roleName in userRoles)
                {
                    var applicationRole = await roleManager.FindByNameAsync(roleName);
                    string roleId = applicationRole.Id;
                    var pemisstions = _permissonMappingService.GetByRoleId(roleId);
                    foreach (var permissionMapping in pemisstions)
                    {
                        if (_permissionService.GetById(permissionMapping.PermissionId).Name.Equals(permisstionName))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public void InsertActivityLog(string log, string userId)
        {
            var activity = new ActivityLog();
            activity.CreatedDate = DateTime.Now;
            activity.Activity = log;
            if (!String.IsNullOrEmpty(userId))
            {
                activity.UserId = userId;
                activity.UserName = UserManager.FindById(userId).UserName;
            }
            _activityLogService.Insert(activity);
        }

        protected string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;

            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;

            if (ip == "::1")
                ip = "127.0.0.1";

            return ip;
        }

        [HttpGet]
        public ActionResult Image(string url)
        {
            var dir = Server.MapPath("/");
            var path = Path.Combine(dir, url);

            //validate the path for security or use other means to generate the path.
            if (!System.IO.File.Exists(path))
                path = Path.Combine(dir, "Content/Images/images_none.png");
            Image image = System.Drawing.Image.FromFile(path);
            //Resize image when width > 256
            if (image.Width > 256)
            {
                double newHeight = ((double)image.Height / (double)image.Width) * 256;
                image = ImageHepler.ResizeImage(image, 256, (int)newHeight);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return File(ms.ToArray(), "image/jpeg");
            }
        }
    }
}