﻿using AutoMapper;
using sTour.Common;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region Field
        private readonly IExceptionLogService _exceptionLogService;
        private readonly ILocationService _locationService;
        private readonly IHotelService _hotelService;
        private readonly IRestaurantService _restaurantService;
        #endregion Field
        #region Ctr

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService, ILocationService locationService, IHotelService hotelService,
    IRestaurantService restaurantService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._exceptionLogService = exceptionLogService;
            this._locationService = locationService;
            this._hotelService = hotelService;
            this._restaurantService = restaurantService;
        }
        #endregion

        public ActionResult Index()
        {
            var homeVM = new HomeViewModel();
            homeVM.Hotels = Mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelModel>>(_hotelService.GetHotelInHome());
            homeVM.Locations = Mapper.Map<IEnumerable<Location>, IEnumerable<LocationModel>>(_locationService.GetLocationInHome());
            homeVM.Restaurants = Mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantModel>>(_restaurantService.GetRestaurantInHome());
            return View(homeVM);
        }

        public ActionResult Header()
        {
            return PartialView();
        }

        public ActionResult Footer()
        {
            return PartialView();
        }

        public async Task<ActionResult> Authorize()
        {
            if (await CheckPermission(SystemPermission.ManagerSystem))
            {
                return View();
            }
            else
            {
                return Unauthorized();
            }

        }

        public ActionResult SearchQuery(string q)
        {
            var result = _hotelService.SearchQuery(q);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult WeatherForecast()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Message()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SendMail(MailViewModel model)
        {
            if (MailHepler.SendMail(model.Email, model.Subject, model.Content, model.Name))
                return RedirectToAction("Message", "Home");
            else
                return new HttpNotFoundResult("System can't send email at this time");
        }
    }
}