﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class HotelController : BaseController
    {
        private IHotelService _hotelService;
        private IServiceHotelService _serviceHotelService;
        public HotelController(IHotelService hotelService,ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService, IServiceHotelService serviceHotelService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._hotelService = hotelService;
            this._serviceHotelService = serviceHotelService;
        }
        // GET: Hotel
        public ActionResult Index(string keyword, string sort = null, string prices = null, string stars = null, string services = null, int page = 1)
        {
            int pageSize = 6;
            int totalRow = 0;
            int fromPrice = 0;
            int toPrice = 0;
            List<int> starChecked = new List<int>();
            List<int> serviceChecked = new List<int>();
            if (!String.IsNullOrEmpty(stars))
            {
                stars.Split('_').ToList().ForEach(x => {
                    starChecked.Add(int.Parse(x));
                });
            }
            if (!String.IsNullOrEmpty(services))
            {
               services.Split('_').ToList().ForEach(x=> {
                   serviceChecked.Add(int.Parse(x));
               });
            }
            if (!String.IsNullOrEmpty(prices))
            {
                fromPrice = int.Parse(prices.Split('-')[0]);
                toPrice = int.Parse(prices.Split('-')[1]);
            }
            var hotels = _hotelService.GetHotelPagings(page, pageSize, keyword, sort, fromPrice, toPrice, starChecked, serviceChecked, out totalRow).ToList();
            var hotelModel = Mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelModel>>(hotels);
            int totalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            ViewBag.Services = _serviceHotelService.GetAll();
            var paginationSet = new PaginationSet<HotelModel>()
            {
                Items = hotelModel,
                MaxPage = 3,
                Page = page,
                TotalCount = totalRow,
                TotalPages = totalPage
            };
            return View(paginationSet);
        }

        public ActionResult Detail(int Id)
        {
            var hotel = _hotelService.GetById(Id);
            var model = Mapper.Map<Hotel, HotelModel>(hotel);
            return View(model);
        }
    }
}