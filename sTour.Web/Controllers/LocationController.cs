﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Infrastructure.Core;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace sTour.Web.Controllers
{
    public class LocationController : BaseController
    {
        private ILocationService _locationService;      
        public LocationController(ILocationService locationService, ApplicationUserManager userManager, ApplicationSignInManager signInManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissionService, IExceptionLogService exceptionLogService, IActivityLogService activityLogService) : base(userManager, signInManager, permissionMappingService, permissionService, activityLogService)
        {
            this._locationService = locationService;          
        }
        
        public ActionResult Index(string keyword, int page = 1)
        {
            int pageSize = 6;
            int totalRow = 0;           
            var locations = _locationService.GetLocations(page, pageSize, keyword, out totalRow).ToList();
            var locationModel = Mapper.Map<IEnumerable<Location>, IEnumerable<LocationModel>>(locations);
            int totalPage = (int)Math.Ceiling((double)totalRow / pageSize);           
            var paginationSet = new PaginationSet<LocationModel>()
            {
                Items = locationModel,
                MaxPage = 3,
                Page = page,
                TotalCount = totalRow,
                TotalPages = totalPage
            };
            return View(paginationSet);
        }

        public ActionResult Detail(int Id)
        {
            var location = _locationService.GetById(Id);
            var model = Mapper.Map<Location, LocationModel>(location);

            return View(model);
        }
    }
}