﻿using AutoMapper;
using sTour.Entity;
using sTour.Web.Models;

namespace sTour.Web.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Location, LocationModel>();
                cfg.CreateMap<Hotel, HotelModel>();
                cfg.CreateMap<Foods, FoodModel>();
                cfg.CreateMap<Cultural, CulturalModel>();
                cfg.CreateMap<Restaurant, RestaurantModel>();
                cfg.CreateMap<Country, CountryModel>();
                cfg.CreateMap<Provincial, ProvinceModel>();
                cfg.CreateMap<District, DistrictModel>();
                cfg.CreateMap<Banner, BannerModel>();
                cfg.CreateMap<ApplicationUser, UserModel>();
                cfg.CreateMap<ApplicationRole, RoleModel>();
                cfg.CreateMap<Permisstion, PermissionModel>();
                cfg.CreateMap<ExceptionLog, ExceptionErrorModel>();
                cfg.CreateMap<ServiceHotel, ServiceHotelModel>();
                cfg.CreateMap<ServiceRestaurant, ServiceRestaurantModel>();
                cfg.CreateMap<Enterprise, EnterpriseModel>();
                cfg.CreateMap<POI, POIModel>();
            });
        }
    }
}