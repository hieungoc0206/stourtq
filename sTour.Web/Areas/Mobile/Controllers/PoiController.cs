﻿using sTour.Service;
using sTour.Web.Models;
using System;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
    public class PoiController : Controller
    {
        private readonly IPOIService _poiService;

        public PoiController(IPOIService poiService)
        {
            this._poiService = poiService;
        }

        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                var pois = _poiService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var item in pois)
                {
                    if (item.Image != null)
                    {
                        item.Image = item.Image.Replace("~/", imageHost);
                    }
                }

                response.Data = pois;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}