﻿using sTour.Service;
using sTour.Web.Models;
using System;
using System.Web.Http;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
   
    public class CategoryLocationController : Controller
    {
        private readonly ICategoryLocationService _categoryLocationService;

        public CategoryLocationController(ICategoryLocationService categoryLocationService)
        {
            this._categoryLocationService = categoryLocationService;
        }

       
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                var categories = _categoryLocationService.GetAll();
                response.Data = categories;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}