﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
    public class LocationController : Controller
    {
        private readonly ILocationService _locationService;
        private readonly ICategoryLocationService _categoryLocationService;

        public LocationModel ConvertLocationToLocationModel(Location location)
        {
            var locationModel = new LocationModel();
            locationModel = Mapper.Map<Location, LocationModel>(location);
            locationModel.Images = String.Empty;
            locationModel.FileAttachs = JsonConvert.DeserializeObject<List<FileAttach>>(location.Images);
            return locationModel;
        }

        public LocationController(ILocationService locationService, ICategoryLocationService categoryLocationService)
        {
            this._locationService = locationService;
            this._categoryLocationService = categoryLocationService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                List<LocationModel> locationModels = new List<LocationModel>();
                var locations = _locationService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var item in locations)
                {
                    if (item.LocationCategoryId.HasValue)
                    {
                        item.categoryLocation = _categoryLocationService.GetById(item.LocationCategoryId.Value);
                    }

                    if (item.Avatar != null)
                    {
                        item.Avatar = item.Avatar.Replace("~/", imageHost);
                    }

                    if (item.Images != null)
                    {
                        item.Images = item.Images.Replace("~/", imageHost);
                    }
                    var model = ConvertLocationToLocationModel(item);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    locationModels.Add(model);
                }
                response.Data = locationModels;
                response.Message = "Success";
                response.Status = ResponseStatus.Success.ToString();
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Detail(int locationId)
        {
            var response = new ResponseModel();
            try
            {
                var location = _locationService.GetById(locationId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                if (location.LocationCategoryId.HasValue)
                    location.categoryLocation = _categoryLocationService.GetById(location.LocationCategoryId.Value);
                if (!String.IsNullOrEmpty(location.Avatar))
                    location.Avatar = location.Avatar.Replace("~/", imageHost);
                if (location.Images != null)
                {
                    location.Images = location.Images.Replace("~/", imageHost);
                }
                var model = ConvertLocationToLocationModel(location);
                model.Images = string.Empty;
                response.Data = model;
                response.Message = "Success";
                response.Status = ResponseStatus.Success.ToString();
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetByCategory(int categoryId)
        {
            var response = new ResponseModel();
            try
            {
                List<LocationModel> locationModels = new List<LocationModel>();
                var locations = _locationService.GetByCategory(categoryId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var item in locations)
                {
                    if (item.LocationCategoryId.HasValue)
                    {
                        item.categoryLocation = _categoryLocationService.GetById(item.LocationCategoryId.Value);
                    }

                    if (item.Avatar != null)
                    {
                        item.Avatar = item.Avatar.Replace("~/", imageHost);
                    }

                    if (item.Images != null)
                    {
                        item.Images = item.Images.Replace("~/", imageHost);
                    }
                    var model = ConvertLocationToLocationModel(item);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    locationModels.Add(model);
                }
                response.Data = locationModels;
                response.Message = "Success";
                response.Status = ResponseStatus.Success.ToString();
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}