﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
   
    public class HotelController : Controller
    {
        private readonly IHotelService _hotelService;

        public HotelModel ConvertHotelToHotelModel(Hotel hotel)
        {
            var hotelModel = new HotelModel();
            hotelModel = Mapper.Map<Hotel, HotelModel>(hotel);
            hotelModel.Images = String.Empty;
            hotelModel.FileAttachs = JsonConvert.DeserializeObject<List<FileAttach>>(hotel.Images);
            return hotelModel;
        }

        public HotelController(IHotelService hotelService)
        {
            this._hotelService = hotelService;
        }
 
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                List<HotelModel> hotelModels = new List<HotelModel>();
                var hotels = _hotelService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var hotel in hotels)
                {
                    if (!string.IsNullOrEmpty(hotel.Images))
                    {
                        hotel.Images = hotel.Images.Replace("~/", imageHost);
                    }

                    if (!string.IsNullOrEmpty(hotel.Avatar))
                    {
                        hotel.Avatar = hotel.Avatar.Replace("~/", imageHost);
                    }

                    var model = new HotelModel();
                    model = ConvertHotelToHotelModel(hotel);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    hotelModels.Add(model);
                }
                response.Data = hotelModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";

            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response,JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult Detail(int hotelId)
        {
            var response = new ResponseModel();
            try
            {
                var hotel = _hotelService.GetById(hotelId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                if (!string.IsNullOrEmpty(hotel.Images))
                {
                    hotel.Images = hotel.Images.Replace("~/", imageHost);
                }

                if (!string.IsNullOrEmpty(hotel.Avatar))
                {
                    hotel.Avatar = hotel.Avatar.Replace("~/", imageHost);
                }
                var model = new HotelModel();
                model = ConvertHotelToHotelModel(hotel);
                model.Images = string.Empty;
                response.Data = model;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
 
        public ActionResult GetByLocation(int locationId)
        {
            var response = new ResponseModel();
            try
            {
                List<HotelModel> hotelModels = new List<HotelModel>();
                var hotels = _hotelService.GetByLocationId(locationId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var hotel in hotels)
                {
                    if (!string.IsNullOrEmpty(hotel.Images))
                    {
                        hotel.Images = hotel.Images.Replace("~/", imageHost);
                    }

                    if (!string.IsNullOrEmpty(hotel.Avatar))
                    {
                        hotel.Avatar = hotel.Avatar.Replace("~/", imageHost);
                    }

                    var model = new HotelModel();
                    model = ConvertHotelToHotelModel(hotel);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    hotelModels.Add(model);
                }
                response.Data = hotelModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";

            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
