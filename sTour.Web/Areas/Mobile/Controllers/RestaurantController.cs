﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
   
    public class RestaurantController : Controller
    {
        private readonly IRestaurantService _restaurantService;

        public  RestaurantModel ConvertRestaurantToRestaurantModel(Restaurant restaurant)
        {
            var restaurantModel = new RestaurantModel();
            restaurantModel = Mapper.Map<Restaurant, RestaurantModel>(restaurant);
            restaurantModel.Images = string.Empty;
            restaurantModel.FileAttachs = JsonConvert.DeserializeObject<List<FileAttach>>(restaurant.Images);
            return restaurantModel;
        }

        public RestaurantController(IRestaurantService restaurantService)
        {
            this._restaurantService = restaurantService;
        }

      
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                List<RestaurantModel> restarantModels = new List<RestaurantModel>();
                var restaurants = _restaurantService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var restaurant in restaurants)
                {
                    if (!string.IsNullOrEmpty(restaurant.Images))
                    {
                        restaurant.Images = restaurant.Images.Replace("~/", imageHost);
                    }

                    if (!string.IsNullOrEmpty(restaurant.Avatar))
                    {
                        restaurant.Avatar = restaurant.Avatar.Replace("~/", imageHost);
                    }

                    var model = new RestaurantModel();
                    model = ConvertRestaurantToRestaurantModel(restaurant);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    restarantModels.Add(model);
                }
                response.Data = restarantModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult Detail(int restaurantId)
        {
            var response = new ResponseModel();
            try
            {
                var restaurant = _restaurantService.GetById(restaurantId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                if (!string.IsNullOrEmpty(restaurant.Images))
                {
                    restaurant.Images = restaurant.Images.Replace("~/", imageHost);
                }

                if (!string.IsNullOrEmpty(restaurant.Avatar))
                {
                    restaurant.Avatar = restaurant.Avatar.Replace("~/", imageHost);
                }
                var model = new RestaurantModel();
                model = ConvertRestaurantToRestaurantModel(restaurant);
                response.Data = model;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult GetByLocation(int locationId)
        {
            var response = new ResponseModel();
            try
            {
                List<RestaurantModel> restarantModels = new List<RestaurantModel>();
                var restaurants = _restaurantService.GetByLocationId(locationId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var restaurant in restaurants)
                {
                    if (!string.IsNullOrEmpty(restaurant.Images))
                    {
                        restaurant.Images = restaurant.Images.Replace("~/", imageHost);
                    }

                    if (!string.IsNullOrEmpty(restaurant.Avatar))
                    {
                        restaurant.Avatar = restaurant.Avatar.Replace("~/", imageHost);
                    }
                    var model = new RestaurantModel();
                    model = ConvertRestaurantToRestaurantModel(restaurant);
                    model.Images = string.Empty;
                    restarantModels.Add(model);
                }
                response.Data = restarantModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}