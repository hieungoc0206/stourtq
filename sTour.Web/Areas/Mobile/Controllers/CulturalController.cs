﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
    
    public class CulturalController : Controller
    {
        private readonly ICulturalService _culturalService;

        public CulturalController(ICulturalService culturalService)
        {
            this._culturalService = culturalService;
        }

        public CulturalModel ConvertCulturalToCulturalModel(Cultural cultural)
        {
            var culturalModel = new CulturalModel();
            culturalModel = Mapper.Map<Cultural, CulturalModel>(cultural);
            culturalModel.Images = String.Empty;
            culturalModel.FileAttachs = JsonConvert.DeserializeObject<List<FileAttach>>(cultural.Images);
            return culturalModel;
        }

       
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                var culturals = _culturalService.GetAll();
                List<CulturalModel> culturalModels = new List<CulturalModel>();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var cultural in culturals)
                {
                    if (!String.IsNullOrEmpty(cultural.Avatar))
                        cultural.Avatar = cultural.Avatar.Replace("~/", imageHost);
                    if (!String.IsNullOrEmpty(cultural.Images))
                        cultural.Images = cultural.Images.Replace("~/", imageHost);
                    var model = new CulturalModel();
                    model = ConvertCulturalToCulturalModel(cultural);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    culturalModels.Add(model);
                }
                response.Data = culturalModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]     
        public ActionResult Detail(int culturalId)
        {
            var response = new ResponseModel();
            try
            {
                var cultural = _culturalService.GetById(culturalId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                if (!String.IsNullOrEmpty(cultural.Avatar))
                    cultural.Avatar = cultural.Avatar.Replace("~/", imageHost);
                if (!String.IsNullOrEmpty(cultural.Images))
                    cultural.Images = cultural.Images.Replace("~/", imageHost);
                var model = new CulturalModel();
                model = ConvertCulturalToCulturalModel(cultural);
                model.Images = string.Empty;
                model.LongDes = string.Empty;
                response.Data = model;
                response.Message = "Success";
                response.Status = ResponseStatus.Success.ToString();

            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}