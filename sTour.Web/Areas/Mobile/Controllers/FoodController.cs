﻿using AutoMapper;
using Newtonsoft.Json;
using sTour.Entity;
using sTour.Service;
using sTour.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
    
    public class FoodController : Controller
    {
        private readonly IFoodService _foodService;

        public FoodController(IFoodService foodService)
        {
            this._foodService = foodService;
        }

        public FoodModel ConvertFoodToFoodModel(Foods food)
        {
            var foodModel = new FoodModel();
            foodModel = Mapper.Map<Foods, FoodModel>(food);
            foodModel.Images = string.Empty;
            foodModel.FileAttachs = JsonConvert.DeserializeObject<List<FileAttach>>(food.Images);
            return foodModel;
        }

      
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                List<FoodModel> foodModels = new List<FoodModel>();
                var foods = _foodService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach (var food in foods)
                {
                    if (!String.IsNullOrEmpty(food.Avatar))
                        food.Avatar = food.Avatar.Replace("~/", imageHost);
                    if (!String.IsNullOrEmpty(food.Images))
                        food.Images = food.Images.Replace("~/", imageHost);
                    var model = new FoodModel();
                    model = ConvertFoodToFoodModel(food);
                    model.Images = string.Empty;
                    model.LongDes = string.Empty;
                    foodModels.Add(model);
                }
                response.Data = foodModels;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

       
       
        public ActionResult Detail(int foodId)
        {
            var response = new ResponseModel();
            try
            {
                var food = _foodService.GetById(foodId);
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                if (!String.IsNullOrEmpty(food.Avatar))
                    food.Avatar = food.Avatar.Replace("~/", imageHost);
                if (!String.IsNullOrEmpty(food.Images))
                    food.Images = food.Images.Replace("~/", imageHost);
                var model = new FoodModel();
                model = ConvertFoodToFoodModel(food);
                model.Images = string.Empty;
                response.Data = model;
                response.Message = "Success";
                response.Status = ResponseStatus.Success.ToString();

            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}