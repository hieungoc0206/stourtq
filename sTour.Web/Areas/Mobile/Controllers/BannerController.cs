﻿using sTour.Service;
using sTour.Web.Models;
using System;
using System.Web.Http;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
   
    public class BannerController : Controller
    {
        private readonly IBannerService _bannerService;

        public BannerController(IBannerService bannerService)
        {
            this._bannerService = bannerService;
        }

     
        public ActionResult GetAll()
        {
            var response = new ResponseModel();
            try
            {
                var banners = _bannerService.GetAll();
                string httpHost = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string imageHost = httpHost + "/Base/Image?url=";
                foreach(var item in banners)
                {
                    if (item.Image != null)
                    {
                        item.Image = item.Image.Replace("~/", imageHost);
                    }
                }
               
                response.Data = banners;
                response.Status = ResponseStatus.Success.ToString();
                response.Message = "Success";
            }
            catch (Exception e)
            {
                response.Data = null;
                response.Status = ResponseStatus.Error.ToString();
                response.Message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}