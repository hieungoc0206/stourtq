﻿using System.Web.Http;
using System.Web.Mvc;

namespace sTour.Web.Areas.Mobile
{
    public class MobileAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Mobile";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Mobile_default",
                "mobile/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
           
        }
    }
}