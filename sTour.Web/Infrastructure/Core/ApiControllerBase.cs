﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sTour.Data;
using sTour.Entity;
using sTour.Service;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace sTour.Web.Infrastructure.Core
{
    public class ApiControllerBase : ApiController
    {
        protected IExceptionLogService _errorService;
        protected ApplicationUserManager _userManager;
        protected IRole_Permisstion_MappingService _permissonMappingService;
        protected IPermissionService _permissionService;
        protected IActivityLogService _activityLogService;

        public ApiControllerBase(IExceptionLogService errorService, ApplicationUserManager userManager, IRole_Permisstion_MappingService permissionMappingService, IPermissionService permissonService, IActivityLogService activityLogService)
        {
            this._errorService = errorService;
            this._userManager = userManager;
            this._permissionService = permissonService;
            this._permissonMappingService = permissionMappingService;
            this._activityLogService = activityLogService;
        }

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage requestMessage, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;
            try
            {
                response = function.Invoke();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Trace.WriteLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation error.");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Trace.WriteLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                    }
                }
                LogError(ex);
                response = requestMessage.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (DbUpdateException dbEx)
            {
                LogError(dbEx);
                response = requestMessage.CreateResponse(HttpStatusCode.BadRequest, dbEx.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = requestMessage.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            return response;
        }

        private void LogError(Exception ex)
        {
            try
            {
                ExceptionLog error = new ExceptionLog();
                error.LogTime = DateTime.Now;
                error.ExceptionMessage = ex.Message;
                error.ExceptionStackTrace = ex.StackTrace;
                _errorService.Insert(error);
            }
            catch
            {
            }
        }
        public HttpResponseMessage UnAuthorized()
        {
            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
        }

        public HttpResponseMessage Badrequest()
        {
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "BadRequest");
        }

        public async Task<bool> CheckPermission(string permisstionName)
        {
            if (String.IsNullOrEmpty(permisstionName))
            {
                return false;
            }
            if (User.Identity.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                ApplicationUser user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return false;
                }
                if (user.LockoutEnabled)
                {
                    return false;
                }
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                var userRoles = await _userManager.GetRolesAsync(userId);
                foreach (var roleName in userRoles)
                {
                    var applicationRole = await roleManager.FindByNameAsync(roleName);
                    string roleId = applicationRole.Id;
                    var pemisstions = _permissonMappingService.GetByRoleId(roleId);
                    foreach (var permissionMapping in pemisstions)
                    {
                        if (_permissionService.GetById(permissionMapping.PermissionId).Name.Equals(permisstionName))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public void InsertActivityLog(string log, string userId)
        {
            var activity = new ActivityLog();
            activity.CreatedDate = DateTime.Now;
            activity.Activity = log;
            if (!String.IsNullOrEmpty(userId))
            {
                activity.UserId = userId;
                activity.UserName = _userManager.FindById(userId).UserName;
            }
            _activityLogService.Insert(activity);
        }

        protected string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;

            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;

            if (ip == "::1")
                ip = "127.0.0.1";

            return ip;
        }
        public class FilenameMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            public FilenameMultipartFormDataStreamProvider(string path) : base(path)
            {
            }

            public override string GetLocalFileName(HttpContentHeaders headers)
            {
                var name = !string.IsNullOrWhiteSpace(headers.ContentDisposition.FileName) ? headers.ContentDisposition.FileName : Guid.NewGuid().ToString();
                return name.Replace("\"", string.Empty);
            }
        }
        //[HttpGet]
        //public ActionResult Image(string url)
        //{
        //    var dir = Server.MapPath("/");
        //    var path = Path.Combine(dir, url);

        //    //validate the path for security or use other means to generate the path.
        //    if (!System.IO.File.Exists(path))
        //        path = Path.Combine(dir, "Content/Images/images_none.png");
        //    Image image = System.Drawing.Image.FromFile(path);
        //    //Resize image when width > 256
        //    if (image.Width > 256)
        //    {
        //        double newHeight = ((double)image.Height / (double)image.Width) * 256;
        //        image = ImageHepler.ResizeImage(image, 256, (int)newHeight);
        //    }
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        return File(ms.ToArray(), "image/jpeg");
        //    }
        //}
    }
}