﻿(function () {
    angular.module('stour.common', ['angular-loading-bar', 'oc.lazyLoad', 'ui.router', 'ngBootbox', 'LocalStorageModule', 'angularUtils.directives.dirPagination', 'angularFileUpload', 'ngCkeditor', 'ui.toggle', 'ngYoutube']);
})();