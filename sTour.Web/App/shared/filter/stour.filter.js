﻿(function (app) {
    //Convert Date from Json
    app.filter('convertDate', function () {
        return function (datetime) {
            if (datetime == null || datetime == '')
                return '';
            var newdate = new Date(datetime);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            var ss = newdate.getSeconds();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (hh < 10)
                hh = "0" + hh;
            if (mm < 10)
                mm = "0" + mm;
            if (ss < 10)
                ss = "0" + ss;
            return day + "/" + month + "/" + year + " " + hh + ":" + mm + ":" + ss;
        }
    });

    //Format Gender
    app.filter('Gender', function () {
        return function (input) {
            if (input) {
                return "Nam";
            } else {
                return "Nữ";
            }
        }
    });

    //Convert isDelete
    app.filter('isDelete', function () {
        return function (input) {
            if (input == true) {
                return "Ẩn"
            } else {
                return "Hiện thị"
            }
        }
    });

    //Convert isActive
    app.filter('isActive', function () {
        return function (input) {
            if (input == false) {
                return "Ẩn"
            } else {
                return "Hiển thị"
            }
        }
    });

    //Convert Status to String
    app.filter('statusFomart', function () {
        return function (input) {
            if (input) {
                return "Khóa tài khoản"
            } else {
                return "Đang hoạt động"
            }
        }
    });
    //Convert Status to String
    app.filter('category', function () {
        return function (input) {
            return categories.find(x => x.Alias === input).Content;
        }
    });
    app.filter('filterImage', function () {
        return function (input) {
            if (input)
                return input.replace('~/', '/');
            else
                return '';
        }
    });
    app.filter('unsafe', function ($sce) { return $sce.trustAsHtml; });
    app.filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
})(angular.module('stour.common'));