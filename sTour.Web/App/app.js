﻿
var app = angular.module('app', ['stour.common', 'admin.account', 'admin.system', 'admin.role', 'admin.location', 'admin.hotel', 'admin.food', 'admin.restaurant', 'admin.country', 'admin.province', 'admin.district', 'admin.banner', 'admin.cultural', 'admin.servicehotel', 'admin.servicerestaurant', 'admin.touristservice']);

    //config ocLazyLoad
    app.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);

    app.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            $rootScope.title = toState.title;
            event.targetScope.$watch('$viewContentLoaded', function () {
                angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);
            });
        });

    }])

    //config state
app.config(['$stateProvider', '$urlRouterProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise('/login');
        $stateProvider
          .state('baseAdmin', {
              url: '',
              templateUrl: '/App/shared/view/baseAdminView.html',
              abstract: true
          })
          .state('admin', {
              url: '/admin',
              templateUrl: 'App/components/admin/adminView.html',
              parent: 'baseAdmin',
              controller: 'adminController',
              resolve: {
                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'app',
                          insertBefore: '#ng_load_plugins_before',
                          files: [
                              'App/components/admin/adminController.js'
                          ]
                      })
                  }]
              }
          })
          .state('login', {
              url: '/login',
              templateUrl: '/App/components/account/loginView.html',
              controller: 'loginController',
              resolve: {
                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'app',
                          insertBefore: '#ng_load_plugins_before',
                          files: [
                               '/Assets/login/css/style.css',
                               '/Assets/admin/font-awesome/font-rokkit.css',
                              'App/components/account/loginController.js'
                          ]
                      })
                  }]
              }
          })       
    }]);

    app.directive('customOnChange', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    });

    //config authentication

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $location, localStorageService) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};
                    var authData = localStorageService.get('TokenInfo');
                    if (authData) {
                        authData = JSON.parse(authData);
                        config.headers.Authorization = 'Bearer ' + authData.accessToken;
                    }

                    return config;
                },
                responseError: function (rejection) {
                    if (rejection.status === 401) {
                        $location.path('/login');
                        console.log("Không có quyền !")
                    }
                    return $q.reject(rejection);
                }
            };
        });
    }]);

