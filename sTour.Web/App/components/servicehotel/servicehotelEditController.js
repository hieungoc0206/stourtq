﻿(function (app) {
    app.controller('servicehotelEditController', servicehotelEditController);

    servicehotelEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService', '$state', '$stateParams', 'FileUploader', '$ngBootbox', '$filter'];

    function servicehotelEditController($scope, apiService, $http, notificationService, $state, $stateParams, FileUploader, $ngBootbox, $filter) {
        $scope.initData = function () {

            apiService.get('/api/servicehotel/getbyid/' + $stateParams.id, null, function (result) {
                $scope.servicehotel = result.data;
            }, function (err) {

            });
        }        
        $scope.servicehotel = {
            Id: 0,
            Name: '',
            NameEn: '',
            Description: '',
            DescriptionEn: '',
            Icon: '~/Assets/images_none.png',
        }
       
        $scope.editServiceHotel = function () {
            apiService.put('/api/servicehotel/edit/', $scope.servicehotel,
                function (result) {
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('servicehotels');
                }, function (error) {
                    notificationService.displayError('Sửa mới không thành công.');
                });
        }
        
        //update Icon
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1) {
                notificationService.displayError("Kích thước ảnh icon không thể vượt quá 1MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.servicehotel.Icon = e.target.result;
                });

            });
        }
    }
})(angular.module('admin.servicehotel'));