﻿(function (app) {
    app.controller('servicehotelListController', servicehotelListController);

    servicehotelListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function servicehotelListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.servicehotels = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getServiceHotels = function (page) {
            page = page || 1;
            apiService.get(`/api/servicehotel/getservicehotels?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {

                $scope.servicehotels = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.hotel'));