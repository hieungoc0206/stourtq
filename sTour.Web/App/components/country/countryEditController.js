﻿(function (app) {
    app.controller('countryEditController', countryEditController);

    countryEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService','$stateParams', 'FileUploader', '$state', '$filter'];

    function countryEditController($scope, apiService, $http, notificationService, $stateParams, FileUploader, $state, $filter) {
        $scope.initData = function () {
            apiService.get('/api/country/getbyid/' + $stateParams.id, null, function (result) {
                $scope.country = result.data;
            }, function (err) {

            });
        }
        $scope.editCountry = function () {

            apiService.put('/api/country/edit/', $scope.country,
                function (result) {                   
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('countries');
                }, function (error) {
                    notificationService.displayError(err.data.Message);
                });

        }
    }

})(angular.module('admin.country'));