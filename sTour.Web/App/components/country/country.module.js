﻿(function () {
    angular.module('admin.country', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('countries', {
            url: '/countries',
            parent: 'baseAdmin',
            templateUrl: 'App/components/country/countryListView.html',
            controller: 'countryListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/country/countryListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-country', {
                url: '/add-country',
                parent: 'baseAdmin',
                templateUrl: 'App/components/country/countryAddView.html',
                controller: 'countryAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/country/countryAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-country', {
                url: '/edit-country/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/country/countryEditView.html',
                controller: 'countryEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/country/countryEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();