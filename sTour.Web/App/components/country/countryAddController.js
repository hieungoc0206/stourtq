﻿(function (app) {
    app.controller('countryAddController', countryAddController);

    countryAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function countryAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        $scope.country = {
            
        }
        $scope.addCountry = function () {         
            apiService.post('/api/country/create/', $scope.country,
                function (result) {
                    notificationService.displaySuccess('Thêm mới thành công');
                    $state.go('countries');
                }, function (error) {
                    notificationService.displayError(err.data.Message);
                });
                
        }
    }

})(angular.module('admin.country'));