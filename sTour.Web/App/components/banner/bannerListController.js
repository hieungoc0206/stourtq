﻿(function (app) {
    app.controller('bannerListController', bannerListController);

    bannerListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function bannerListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.banners = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getBanners = function (page) {
            page = page || 1;
            apiService.get(`/api/banner/getbanners?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {

                $scope.banners = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.banner'));