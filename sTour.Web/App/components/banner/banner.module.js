﻿(function () {
    angular.module('admin.banner', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('banners', {
            url: '/banners',
            parent: 'baseAdmin',
            templateUrl: 'App/components/banner/bannerListView.html',
            controller: 'bannerListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/banner/bannerListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-banner', {
                url: '/add-banner',
                parent: 'baseAdmin',
                templateUrl: 'App/components/banner/bannerAddView.html',
                controller: 'bannerAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/banner/bannerAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-banner', {
                url: '/edit-banner/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/banner/bannerEditView.html',
                controller: 'bannerEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/banner/bannerEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();