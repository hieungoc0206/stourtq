﻿(function (app) {
    app.controller('bannerEditController', bannerEditController);

    bannerEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state','$stateParams', '$filter'];

    function bannerEditController($scope, apiService, $http, notificationService, FileUploader, $state, $stateParams, $filter) {
        $scope.banner = {}
        $scope.initData = function () {
            apiService.get('/api/banner/getbyid/' + $stateParams.id, null, function (result) {
                $scope.banner = result.data;
            }, function (err) {
            });
        }
       
        $scope.editBanner = function () {
            apiService.put('/api/banner/edit/', $scope.banner,
                function (result) {
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('banners');
                }, function (error) {
                    notificationService.displayError('Sửa mới không thành công.');
                });
        }
        //update image
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1) {
                notificationService.displayError("Kích thước ảnh  không thể vượt quá 1MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.banner.Image = e.target.result;
                });

            });
        }
    }

})(angular.module('admin.banner'));