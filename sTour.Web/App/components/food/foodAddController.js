﻿(function (app) {
    app.controller('foodAddController', foodAddController);

    foodAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter','commonService'];

    function foodAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter, commonService) {
        var uploader = $scope.uploader = new FileUploader({

        });
        $scope.GetSeoTitle = function () {
            $scope.food.Alias = commonService.getSeoTitle($scope.food.Name);
        }
        $scope.GetSeoTitleEn = function () {
            $scope.food.AliasEn = commonService.getSeoTitle($scope.food.NameEn);
        }
        uploader.filters.push({
            name: 'fileSize',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var itemSize = item.size / 1024 / 1024;
                if (itemSize > 10.24) {
                    alert('The maximum file size for uploads is 10MB');
                    return;
                }
                var filterFile = [];
                // not filter
                uploader.queue.forEach(function (data) {
                    if (data.file.name == item.name) {
                        filterFile.push(item);
                    }
                });
                if (filterFile.length > 0) {
                    alert('File exists');
                    return;
                }
                return item;
            }
        });
        uploader.queue = [];
        $scope.files = [];
       
        function loadDistrict() {
            apiService.get('/api/district/getall', null, function (result) {
                $scope.districts = result.data;
            }, function () {
                console.log('Cannot get list parent');
            });
        }
        $scope.food = {
            Name: '',
            NameE: '',
            Address: '',
            AddressEn: '',
            Avatar: '~/Assets/images_none.png',
            Star: 0,
            ViewCount: 0,
            Latitude: 21.774107,
            Longitude: 105.4693488,
            ShortDes: '',
            ShortDesEn: '',
            LongDes: '',
            LongDesEn: '',
            VideoDir: '',
            Price: 0,
            isActive : true
        }

        var latlng = new google.maps.LatLng($scope.food.Latitude, $scope.food.Longitude);
        var mapOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            scaleControl: true,
            panControl: false
        };
        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        //add Search Box
        var searchBox = new google.maps.places.SearchBox(document.getElementById("mapsearch"));
        document.getElementById('mapsearch').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("mapsearch"));
        // Bias the SearchBox results towards current map's viewport.
        $scope.map.addListener('bounds_changed', function () {
            searchBox.setBounds($scope.map.getBounds());
        });
        searchBox.addListener("places_changed", function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            //bound
            var bounds = new google.maps.LatLngBounds();
            var i, place;
            for (i = 0; place = places[i]; i++) {
                bounds.extend(place.geometry.location);
            }
            $scope.map.fitBounds(bounds);
            $scope.map.setZoom(17);

        });
        //change latitude, longitude when click map
        google.maps.event.addListener($scope.map, "click", function (e) {
            if ($scope.marker !== undefined)
                $scope.marker.setMap(null);
            //lat and lng is available in e object
            var latLng = e.latLng;
            $scope.food.Latitude = latLng.lat();
            $scope.food.Longitude = latLng.lng();
            $scope.marker = new google.maps.Marker({
                map: $scope.map,
                position: latLng,
                title: 'Vị trí ẩm thực'
            });
        });
        $scope.addFood = function () {
            uploader.uploadAll();
            uploader.queue.forEach(function (data) {
                $scope.files.push(data);
            });
            $http({
                method: "POST",
                url: "/api/food/create",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("Address", $scope.food.Address);
                    formData.append("AddressEn", $scope.food.AddressEn);
                    formData.append("Name", $scope.food.Name);
                    formData.append("NameEn", $scope.food.NameEn);
                    formData.append("Alias", $scope.food.Alias);
                    formData.append("AliasEn", $scope.food.AliasEn);
                    formData.append("ViewCount", $scope.food.ViewCount);
                    formData.append("Avatar", $scope.food.Avatar);
                    formData.append("Latitude", $scope.food.Latitude);
                    formData.append("Longitude", $scope.food.Longitude);
                    formData.append("DistrictId", $scope.food.DistrictId);
                    formData.append("VideoDir", $scope.food.VideoDir);
                    formData.append("ShortDes", $scope.food.ShortDes);
                    formData.append("ShortDesEn", $scope.food.ShortDesEn);
                    formData.append("LongDes", $scope.food.LongDes);
                    formData.append("LongDesEn", $scope.food.LongDesEn);
                    formData.append("isActive", $scope.food.isActive);
                    //now add all of the assigned files
                    if ($scope.files.length != 0) {
                        for (var i = 0; i < $scope.files.length; i++) {
                            formData.append("file" + i, data.files[i]._file);
                        }
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    model: $scope.model,
                    files: $scope.files
                }

            }).success(function (data, status, headers, config) {
                uploader.queue = [];
                notificationService.displaySuccess("Thêm mới ẩm thực thành công");
                $state.go('foods');
            }).
            error(function (data, status, headers, config) {
                uploader.queue = [];
                notificationService.displayError("Cập nhật ẩm thực thất bại");
            });
        }

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info("onWhenAddingFileFailed", item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            //$scope.files.push(fileItem);
            let reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    fileItem.uploadsrc = e.target.result;
                });

            });
            console.info("onAfterAddingFile", fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            // $scope.files.push(addedFileItems);
            console.info("onAfterAddingAll", addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info("onBeforeUploadItem", item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info("onProgressItem", fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info("onProgressAll", progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info("onSuccessItem", fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info("onErrorItem", fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info("onCancelItem", fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info("onCompleteItem", fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info("onCompleteAll");
        };
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.food.Avatar = e.target.result;
                });

            });
            //  console.log(e.target.files[0])
        }
        loadDistrict()
    }

})(angular.module('admin.food'));