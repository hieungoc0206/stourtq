﻿(function () {
    angular.module('admin.food', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('foods', {
            url: '/foods',
            parent: 'baseAdmin',
            templateUrl: 'App/components/food/foodListView.html',
            controller: 'foodListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/food/foodListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-food', {
                url: '/add-food',
                parent: 'baseAdmin',
                templateUrl: 'App/components/food/foodAddView.html',
                controller: 'foodAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/food/foodAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-food', {
                url: '/edit-food/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/food/foodEditView.html',
                controller: 'foodEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/food/foodEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();