﻿(function (app) {
    app.controller('restaurantListController', restaurantListController);

    restaurantListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function restaurantListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.restaurants = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getRestaurants = function (page) {
            page = page || 1;
            apiService.get(`/api/restaurant/getrestaurants?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {

                $scope.restaurants = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.restaurant'));