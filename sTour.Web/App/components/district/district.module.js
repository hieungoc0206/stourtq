﻿(function () {
    angular.module('admin.district', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('districts', {
            url: '/districts',
            parent: 'baseAdmin',
            templateUrl: 'App/components/district/districtListView.html',
            controller: 'districtListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/district/districtListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-district', {
                url: '/add-district',
                parent: 'baseAdmin',
                templateUrl: 'App/components/district/districtAddView.html',
                controller: 'districtAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/district/districtAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('import-district', {
                url: '/import-district',
                parent: 'baseAdmin',
                templateUrl: 'App/components/district/districtImportView.html',
                controller: 'districtImportController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/district/districtImportController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-district', {
                url: '/edit-district/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/district/districtEditView.html',
                controller: 'districtEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/district/districtEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();