﻿(function (app) {
    app.controller('districtListController', districtListController);

    districtListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function districtListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.districts = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getDistricts = function (page) {
            page = page || 1;
            apiService.get(`/api/district/getdistricts?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.districts = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError(err.data.Message);
            });
        }
    }

})(angular.module('admin.district'));