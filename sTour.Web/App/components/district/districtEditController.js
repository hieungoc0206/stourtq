﻿(function (app) {
    app.controller('districtEditController', districtEditController);

    districtEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService', '$stateParams', 'FileUploader', '$state', '$filter'];

    function districtEditController($scope, apiService, $http, notificationService, $stateParams, FileUploader, $state, $filter) {
        $scope.initData = function () {
            apiService.get('/api/district/getbyid/' + $stateParams.id, null, function (result) {
                $scope.district = result.data;
            }, function (err) {

            });
            apiService.get('/api/province/getall/', null,
                function (result) {
                    $scope.provinces = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }

        $scope.editDistrict = function () {

            apiService.put('/api/district/edit/', $scope.district,
                function (result) {
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('districts');
                }, function (error) {
                    notificationService.displayError('Sửa mới không thành công.');
                });

        }
    }

})(angular.module('admin.district'));