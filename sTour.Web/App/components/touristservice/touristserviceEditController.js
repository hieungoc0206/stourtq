﻿(function (app) {
    app.controller('touristserviceEditController', touristserviceEditController);

    touristserviceEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService','$stateParams', 'FileUploader', '$state', '$filter'];

    function touristserviceEditController($scope, apiService, $http, notificationService, $stateParams, FileUploader, $state, $filter) {
        $scope.initData = function () {
            apiService.get('/api/touristservice/getbyid/' + $stateParams.id, null, function (result) {
                $scope.touristservice = result.data;
            }, function (err) {

            });
        }
        $scope.categories = categories;
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.touristservice.Avatar = e.target.result;
                });

            });
        }
        $scope.edittouristservice = function () {

            apiService.put('/api/touristservice/edit/', $scope.touristservice,
                function (result) {                   
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('countries');
                }, function (error) {
                    notificationService.displayError(err.data.Message);
                });

        }
    }

})(angular.module('admin.touristservice'));