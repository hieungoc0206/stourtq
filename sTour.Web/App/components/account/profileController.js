﻿(function (app) {
    app.controller('profileController', profileController);

    profileController.$inject = ['$scope', 'apiService', 'notificationService', '$state', 'commonService', '$ngBootbox'];

    function profileController($scope, apiService, notificationService, $state, commonService, $ngBootbox) {
        $scope.user = { Gender: false, Country: 238, JobId: null };
        $scope.countries = [];
        $scope.updateProfile = updateProfile;
        //Gender
        $scope.genders = [{
            gender: false,
            name: 'Nữ'
        },
        {
            gender: true,
            name: 'Nam'
        }
        ]
        
        function loadCountry() {
            apiService.get('/api/country/getall', null, function (result) {
                $scope.countries = result.data;
            }, function () {
                console.log('Cannot get list parent');
            });
        }
        function getUser() {
            apiService.get('/api/user/getcurrentuser', null, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Lấy dữ liệu thất bại");
                    return;
                }
                $scope.user = result.data;
                $scope.user.BirthDay = commonService.convertDateService($scope.user.BirthDay);
            }, function (err) {
                notificationService.displayError("Lấy dữ liệu thất bại");
            })
        }

        function updateProfile() {
            $ngBootbox.confirm("Bạn có chắc chắn cập nhật tài khoản không ?").then(function () {
                apiService.put('/api/user/udateprofile', $scope.user, function (result) {
                
                    if (result.data == undefined) {
                        notificationService.displayError("Cập nhật tài khoản thất bại");
                        return;
                    }
                    notificationService.displaySuccess("Cập nhật tài khoản thành công");
                }, function (err) {

                });
            }, function () {
                notificationService.displayError("Đã có lỗi xảy ra");
            });

        }
        getUser();
        loadCountry();

    }

})(angular.module('admin.account'));