﻿(function (app) {
    app.controller('changePassUserController', changePassUserController);

    changePassUserController.$inject = ['$scope', 'apiService', 'notificationService', '$state', '$ngBootbox'];

    function changePassUserController($scope, apiService, notificationService, $state, $ngBootbox) {
        $scope.user = {};
        $scope.changePass = function () {
            $ngBootbox.confirm("Bạn có chắc chắn thay đổi mật khẩu ?").then(function () {            
                    apiService.put('/api/user/changepass', $scope.user, function (result) {
                        if (result.data == undefined) {
                            notificationService.displayError("Thay đổi mật khẩu thất bại");
                            return;
                        }                     
                        notificationService.displaySuccess(result.data);
                    }, function (err) {
                        notificationService.displayError(err.data.Message);
                    });
                
            }, function () {

            });

        }
    }

})(angular.module('admin.account'));