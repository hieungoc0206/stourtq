﻿(function () {
    angular.module('admin.account', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('users', {
            url: '/users',
            parent: 'baseAdmin',
            templateUrl: '/App/components/account/accountListView.html',
            controller: 'accountListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/account/accountListController.js'
                        ]
                    })
                }]
            }
        })
        .state('decentralization', {
            url: '/decentralization/:id/:username',
            parent: 'baseAdmin',
            templateUrl: '/App/components/account/decentralizationView.html',
            controller: 'decentralizationController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/account/decentralizationController.js'
                        ]
                    })
                }]
            }
        })
        .state('profile', {
            url: '/profile',
            parent: 'baseAdmin',
            templateUrl: '/App/components/account/profileView.html',
            controller: 'profileController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/account/profileController.js'
                        ]
                    })
                }]
            }
        })
        .state('changepassuser', {
            url: '/changepassuser',
            parent: 'baseAdmin',
            templateUrl: '/App/components/account/changePassUserView.html',
            controller: 'changePassUserController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/account/changePassUserController.js'
                        ]
                    })
                }]
            }
        })
        .state('logs', {
            url: '/logs',
            parent: 'baseAdmin',
            templateUrl: '/App/components/account/logListView.html',
            controller: 'logListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/account/logListController.js'
                        ]
                    })
                }]
            }
            })
            .state('register', {
                url: '/register',
                parent: 'baseAdmin',
                templateUrl: '/App/components/account/registerView.html',
                controller: 'registerController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [                              
                                'App/components/account/registerController.js'
                            ]
                        })
                    }]
                }
            })
    }
})();