﻿(function (app) {
    app.controller('registerController', ['$scope', 'apiService', 'notificationService', '$location', function ($scope, apiService, notificationService, $location) {
        $scope.user = {
            Gender: true
        };
        function validate() {
            Object.keys($scope.user).forEach(function (key) {
                if ($scope.user[key] == '') {
                    return false;
                }
            });

            return true;
        }
        $scope.register = function () {          
            //check null properties 
            if (!validate()) {
                notificationService.displayError("Bạn cần điền đầy đủ thông tin");
            } else if (/[abc]/.test($scope.user['PhoneNumber'])) {
                //check phone number
                notificationService.displayError("Bạn nhập sai số điện thoại");
            } else if ($scope.user['Password'] !== $scope.user['VerifyPassword']) {
                //check password 
                notificationService.displayError("Hai password không trùng nhau");
            } else {
                apiService.post('/api/user/register', $scope.user, function (res) {
                    notificationService.displaySuccess("Tạo tài khoản mới thành công");
                    $location.url('users');
                }, function (err) {
                    notificationService.displayError(err.data.Message);
                });
            }
        }        
    }]);
})(angular.module('app'));