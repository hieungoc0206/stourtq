﻿(function () {
    angular.module('admin.hotel', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('hotels', {
            url: '/hotels',
            parent: 'baseAdmin',
            templateUrl: 'App/components/hotel/hotelListView.html',
            controller: 'hotelListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/hotel/hotelListController.js'
                        ]
                    })
                }]
            }
        })
        .state('add-hotel', {
            url: '/add-hotel',
            parent: 'baseAdmin',
            templateUrl: 'App/components/hotel/hotelAddView.html',
            controller: 'hotelAddController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/hotel/hotelAddController.js'
                        ]
                    })
                }]
            }
        })
        .state('edit-hotel', {
            url: '/edit-hotel/:id',
            parent: 'baseAdmin',
            templateUrl: 'App/components/hotel/hotelEditView.html',
            controller: 'hotelEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/hotel/hotelEditController.js'
                        ]
                    })
                }]
            }
        })
    }

})();