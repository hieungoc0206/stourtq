﻿(function (app) {
    app.controller('hotelListController', hotelListController);

    hotelListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function hotelListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.hotels = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getHotels = function (page) {
            page = page || 1;
            apiService.get(`/api/hotel/gethotels?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.hotels = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.hotel'));