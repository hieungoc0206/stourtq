﻿(function () {
    angular.module('admin.location', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('locations', {
            url: '/locations',
            parent: 'baseAdmin',
            templateUrl: 'App/components/location/locationListView.html',
            controller: 'locationListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/location/locationListController.js'
                        ]
                    })
                }]
            }
        })
        .state('add-location', {
            url: '/add-location',
            parent: 'baseAdmin',
            templateUrl: 'App/components/location/locationAddView.html',
            controller: 'locationAddController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/location/locationAddController.js'
                        ]
                    })
                }]
            }
        })
        .state('edit-location', {
            url: '/edit-location/:id',
            parent: 'baseAdmin',
            templateUrl: 'App/components/location/locationEditView.html',
            controller: 'locationEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/location/locationEditController.js'
                        ]
                    })
                }]
            }
        })
    }

})();