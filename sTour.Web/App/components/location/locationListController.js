﻿(function (app) {
    app.controller('locationListController', locationListController);

    locationListController.$inject = ['$scope', 'apiService', 'notificationService', '$state'];

    function locationListController($scope, apiService, notificationService, $state) {
        $scope.keyword = '';
        $scope.locations = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getLocations = function (page) {
            page = page || 1;
            apiService.get(`/api/location/getlocations?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.locations = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }
})(angular.module('admin.location'));