﻿(function (app) {
    app.controller('permissionListController', permisstionListController);

    permisstionListController.$inject = ['$scope', 'apiService', 'notificationService'];

    function permisstionListController($scope, apiService, notificationService) {
        $scope.permisstions = [];

        function getPermisstion() {
            apiService.get('/api/user/getallpermission', null, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError('Không thể lấy được dữ liệu');
                    return;
                }
                $scope.permisstions = result.data;
            }, function (err) {

            });
        }

        getPermisstion();
    }
})(angular.module('admin.role'))