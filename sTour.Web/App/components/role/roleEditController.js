﻿(function (app) {
    app.controller('roleEditController', roleEditController);

    roleEditController.$inject = ['$scope', 'apiService', 'notificationService', '$state', '$stateParams'];

    function roleEditController($scope, apiService, notificationService, $state, $stateParams) {
        $scope.role = {};
        $scope.editRole = function () {
            apiService.put('/api/role/edit', $scope.role, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Cập nhật thất bại");
                    return;
                }
                var object = result.data;
                if (object) {
                    notificationService.displaySuccess("Cập nhật thành công");
                    $state.go('roles');
                } else {
                    notificationService.displayError("Cập nhật thất bại");
                }

            }, function (err) {

            });
        }
        function getRole() {

            apiService.get('/api/role/getbyid/'+ $stateParams.id,null, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Lấy dữ liệu thất baị");
                    return;
                }
                $scope.role = result.data;
            }, function (err) {

            });
        }

        getRole();
    }
})(angular.module('admin.role'));