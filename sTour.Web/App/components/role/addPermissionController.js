﻿(function (app) {
    app.controller('addPermissionController', addPermissionController);

    addPermissionController.$inject = ['$scope', 'apiService', 'notificationService', '$state', '$stateParams', '$ngBootbox'];

    function addPermissionController($scope, apiService, notificationService, $state, $stateParams, $ngBootbox) {
        $scope.roleName = $stateParams.name;
        $scope.rolePer = {};

        function getPermisstions() {
            apiService.get('/api/role/getpermission/'+$stateParams.id,null, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Lấy dữ liệu thất bại");
                    return;
                }
                $scope.rolePer = result.data;
            }, function (err) {

            });
        }

        $scope.addPermisstion = function () {
            $ngBootbox.confirm("Bạn có chắc chắn cập nhật ?").then(function () {
                var data = JSON.stringify($scope.rolePer);
                apiService.post('/api/role/addpermissiontorole?rolePermisstionModel=' + data , null,function (result) {
                    if (result.data == undefined) {
                        notificationService.displayError("Thêm chức năng thất bại");
                        return;
                    }
                    var object = result.data;
                    if (object) {
                        notificationService.displaySuccess("Thêm chức năng thành công");
                        $state.go('roles');
                    }
                }, function (err) {

                });
            }, function () {

            });
        }
        getPermisstions();
    }

})(angular.module('admin.role'));