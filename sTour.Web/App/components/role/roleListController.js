﻿(function (app) {
    app.controller('roleListController', roleListController);

    roleListController.$inject = ['$scope', 'apiService', 'notificationService'];

    function roleListController($scope, apiService, notificationService) {
        $scope.roles = [];
        function getRoles() {
            apiService.get('/api/role/getall', null, function (result) {
                if (result == undefined) {
                    notificationService.displayError("Không thể lấy được dữ liệu");
                    return;
                }
                $scope.roles = result.data;
            }, function (err) {
                notificationService.displayError("Không thể lấy được dữ liệu");
            });
        }

        getRoles();
    }

})(angular.module('admin.role'));