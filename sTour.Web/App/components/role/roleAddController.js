﻿(function (app) {
    app.controller('roleAddController', roleAddController);

    roleAddController.$inject = ['$scope', 'apiService', 'notificationService', '$state', '$stateParams'];

    function roleAddController($scope, apiService, notificationService, $state, $stateParams) {
        $scope.role = {};
        $scope.addRole = function () {
            apiService.post('/api/role/create', $scope.role, function (result) {
                if (result == undefined) {
                    notificationService.displayError('Tạo mới thất bại');
                    return;
                }

                var object = result.data;
                if (object) {
                    notificationService.displaySuccess("Tạo mới thành công");

                } else {
                    notificationService.displayError("Tạo mới thất bại");
                }
            }, function (err) {
                notificationService.displayError('Tạo mới thất bại');
            });
        }
    }
})(angular.module('admin.role'));