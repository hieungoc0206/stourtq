﻿(function (app) {
    app.controller('provinceAddController', provinceAddController);

    provinceAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function provinceAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        $scope.province = {

        }
        $scope.initData = function () {
            apiService.get('/api/country/getall/', null,
                function (result) {
                    $scope.countries = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }
        $scope.addProvince = function () {

            apiService.post('/api/province/create/', $scope.province,
                function (result) {
                    notificationService.displaySuccess('Thêm mới thành công');
                    $state.go('provinces');
                }, function (error) {
                    notificationService.displayError('Thêm mới không thành công.');
                });

        }
    }

})(angular.module('admin.province'));