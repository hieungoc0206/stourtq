﻿(function (app) {
    app.controller('backupController', backupController);

    backupController.$inject = ['$scope', 'apiService', 'notificationService','$http'];

    function backupController($scope, apiService, notificationService,$http) {
        $scope.downloadFile = function (name) {
            $http({
                method: 'GET',
                url: 'api/systemconfig/backup',
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                }
            }).error(function (data) {
                console.log(data);
            });
        };
    }

})(angular.module('admin.system'));