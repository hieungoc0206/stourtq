﻿(function (app) {
    app.controller('exceptionListController', exceptionListController);

    exceptionListController.$inject = ['$scope', 'apiService', 'notificationService', '$state'];

    function exceptionListController($scope, apiService, notificationService, $state) {
        $scope.errors = [];
        $scope.error = {};
        $scope.page = 0;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getErrors = getErrors;

        $scope.selectedError = selectedError;

        function selectedError(error) {
            $scope.error = error;
        }

        function getErrors(page) {
            page = page || 1;
            var config = {
                params: {
                    page: page,
                    pageSize: 10
                }
            }
            apiService.get('/api/exception/getall', config, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Lấy dữ liệu thất bại");
                    return;
                }
                var object = result.data;
                $scope.errors = object.Items;
                $scope.TotalCount = object.TotalCount;
                $scope.pagesCount = object.TotalPages;
            }, function (err) {

            });
        }

        getErrors();
    }

})(angular.module('admin.system'));