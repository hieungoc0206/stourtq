﻿var categories = [
    {
        Alias: "phuong-tien-giao-thong",
        Content: "Phương tiện giao thông",
    },
    {
        Alias: "ben-xe",
        Content: "Bến xe",
    },
    {
        Alias: "ngan-hang",
        Content: "Ngân hàng",
    },
    {
        Alias: "benh-vien",
        Content: "Bệnh viện",
    },
    {
        Alias: "truong-hoc",
        Content: "Trường học",
    },
    {
        Alias: "trung-tam-thuong-mai",
        Content: "Trung tâm thương mai - Chợ",
    },
    {
        Alias: "quan-ca-fe-karaoke",
        Content: "Quán Cafe - Karaoke",
    },
    {
        Alias: "home-stay",
        Content: "Homestay",
    },
    {
        Alias: "ban-quan-ly",
        Content: "Ban quản lý",
    }
]