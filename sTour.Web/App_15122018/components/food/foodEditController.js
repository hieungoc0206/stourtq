﻿(function (app) {
    app.controller('foodEditController', foodEditController);

    foodEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService', '$state', '$stateParams', 'FileUploader', '$ngBootbox', '$filter','commonService'];

    function foodEditController($scope, apiService, $http, notificationService, $state, $stateParams, FileUploader, $ngBootbox, $filter, commonService) {
       
        $scope.initData = function () {

                      
            apiService.get('/api/food/getbyid/' + $stateParams.id, null, function (result) {
                $scope.food = result.data;
              
                if ($scope.food.Images == null || $scope.food.Images == '') {
                    $scope.food.Images = [];
                } else {
                    $scope.food.Images = JSON.parse($scope.food.Images);
                }

                var latLng = new google.maps.LatLng($scope.food.Latitude, $scope.food.Longitude);
                $scope.marker = new google.maps.Marker({
                    map: $scope.map,
                    position: latLng,
                    title: $scope.food.Name
                });
                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent('<h4>' + $scope.food.Name + '</h4>');
                infoWindow.open($scope.map, $scope.marker);
            }, function (err) {

            });
        }

        $scope.GetSeoTitle = function () {
            $scope.food.Alias = commonService.getSeoTitle($scope.food.Name);
        }
        $scope.GetSeoTitleEn = function () {
            $scope.food.AliasEn = commonService.getSeoTitle($scope.food.NameEn);
        }
        function loadDistrict() {
            apiService.get('/api/district/getall', null, function (result) {
                $scope.districts = result.data;
            }, function () {
                console.log('Cannot get list parent');
            });
        }
        var uploader = $scope.uploader = new FileUploader({

        });
        uploader.filters.push({
            name: 'fileSize',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var itemSize = item.size / 1024 / 1024;
                if (itemSize > 10.24) {
                    alert('The maximum file size for uploads is 10MB');
                    return;
                }
                var filterFile = [];
                // not filter
                uploader.queue.forEach(function (data) {
                    if (data.file.name == item.name) {
                        filterFile.push(item);
                    }
                });
                if (filterFile.length > 0) {
                    alert('File exists');
                    return;
                }
                return item;
            }
        });
        uploader.queue = [];
        $scope.files = [];
        $scope.food = {
            Id: 0,
            Name: '',
            NameEn: '',
            Address: '',
            Address: '',
            Avatar: '~/Assets/images_none.png',
            Star: 0,
            ViewCount: 0,
            Latitude: 21.774107,
            Longitude: 105.4693488,
            ShortDes: '',
            ShortDesEn: '',
            LongDes: '',
            LongDesEn: '',
            VideoDir: '',
            Images: []
        }

        var latlng = new google.maps.LatLng($scope.food.Latitude, $scope.food.Longitude);
        var mapOptions = {
            zoom: 17,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            scaleControl: true,
            panControl: false
        };
        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        //add Search Box
        var searchBox = new google.maps.places.SearchBox(document.getElementById("mapsearch"));
        document.getElementById('mapsearch').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("mapsearch"));
        // Bias the SearchBox results towards current map's viewport.
        $scope.map.addListener('bounds_changed', function () {
            searchBox.setBounds($scope.map.getBounds());
        });
        searchBox.addListener("places_changed", function () {
            $scope.marker.setMap(null)
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            //bound
            var bounds = new google.maps.LatLngBounds();
            //var i, place;
            for (i = 0; place = places[i]; i++) {
                bounds.extend(place.geometry.location);
            }
            $scope.map.fitBounds(bounds);
            $scope.map.setZoom(17);

        });
        //change latitude, longitude when click map
        google.maps.event.addListener($scope.map, "click", function (e) {
            //lat and lng is available in e object
            var latLng = e.latLng;
            $scope.marker.setMap(null);
            $scope.food.Latitude = latLng.lat();
            $scope.food.Longitude = latLng.lng();
            $scope.marker = new google.maps.Marker({
                map: $scope.map,
                position: latLng,
                title: 'Vị trí ẩm thực'
            });
        });


        $scope.removeImage = function (index) {
            $ngBootbox.confirm(`Bạn có chắc chắn xóa ảnh ${$scope.food.Images[index].FileName} `).then(function () {
                $scope.food.Images.splice(index, 1);
            }, function () {

            })
        }

        $scope.backupImages = [];

        $scope.removeAllImages = function () {
            $ngBootbox.confirm(`Bạn có chắc chắn xóa tất cả các ảnh đã chọn `).then(function () {
                $scope.backupImages = $scope.food.Images.filter(function (image) {
                    return !image.checked;
                })

                $scope.food.Images = angular.copy($scope.backupImages);
            }, function () {

            })
        }

        $scope.$watch('checkAll', function (newValue) {
            $scope.food.Images.forEach(function (item) {
                item.checked = newValue;
            })
        })
        $scope.editFood = function () {
            uploader.uploadAll();
            uploader.queue.forEach(function (data) {
                $scope.files.push(data);
            });
            $http({
                method: "POST",
                url: "/api/food/edit",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("Id", $scope.food.Id);
                    formData.append("Address", $scope.food.Address);
                    formData.append("AddressEn", $scope.food.AddressEn);
                    formData.append("Name", $scope.food.Name); 
                    formData.append("NameEn", $scope.food.NameEn);    
                    formData.append("Alias", $scope.food.Alias);    
                    formData.append("AliasEn", $scope.food.AliasEn); 
                    formData.append("ViewCount", $scope.food.ViewCount);
                    formData.append("Avatar", $scope.food.Avatar);
                    formData.append("Latitude", $scope.food.Latitude);
                    formData.append("Longitude", $scope.food.Longitude);
                    formData.append("DistrictId", $scope.food.DistrictId);
                    formData.append("VideoDir", $scope.food.VideoDir);
                    formData.append("ShortDes", $scope.food.ShortDes);
                    formData.append("ShortDesEn", $scope.food.ShortDesEn);
                    formData.append("isActive", $scope.food.isActive);
                    formData.append("LongDes", $scope.food.LongDes);
                    formData.append("LongDesEn", $scope.food.LongDesEn);
                    formData.append("Images", JSON.stringify($scope.food.Images));
                    //now add all of the assigned files
                    if ($scope.files.length != 0) {
                        for (var i = 0; i < $scope.files.length; i++) {
                            formData.append("file" + i, data.files[i]._file);
                        }
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    model: $scope.model,
                    files: $scope.files
                }

            }).success(function (data, status, headers, config) {
                if (data) {
                    uploader.queue = [];
                    notificationService.displaySuccess("Sửa mới ẩm thực thành công");
                    $state.go('foods');
                } else {
                   
                    notificationService.displayError("Cập nhật ẩm thực thất bại");
                }

            }).
                error(function (data, status, headers, config) {
                    uploader.queue = [];
                   
                    notificationService.displayError("Cập nhật ẩm thực thất bại");
                });
        }

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info("onWhenAddingFileFailed", item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            //$scope.files.push(fileItem);
            let reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    fileItem.uploadsrc = e.target.result;
                });

            });
            console.info("onAfterAddingFile", fileItem);

        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            // $scope.files.push(addedFileItems);
            console.info("onAfterAddingAll", addedFileItems);

        };
        uploader.onBeforeUploadItem = function (item) {
            console.info("onBeforeUploadItem", item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info("onProgressItem", fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info("onProgressAll", progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info("onSuccessItem", fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info("onErrorItem", fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info("onCancelItem", fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info("onCompleteItem", fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info("onCompleteAll");
        };
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.food.Avatar = e.target.result;
                });

            });
            //  console.log(e.target.files[0])
        }
        loadDistrict()
        //  console.log(e.target.files[0])
    }
})(angular.module('admin.food'));