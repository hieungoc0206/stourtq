﻿(function (app) {
    app.controller('foodListController', foodListController);

    foodListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function foodListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.foods = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getFoods = function (page) {
            page = page || 1;
            apiService.get(`/api/food/getfoods?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.foods = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.food'));