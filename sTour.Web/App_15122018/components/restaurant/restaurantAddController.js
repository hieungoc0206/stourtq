﻿(function (app) {
    app.controller('restaurantAddController', restaurantAddController);

    restaurantAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter','commonService'];

    function restaurantAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter, commonService) {
        var uploader = $scope.uploader = new FileUploader({

        });
        $scope.GetSeoTitle = function () {
            $scope.restaurant.Alias = commonService.getSeoTitle($scope.restaurant.Name);
        }
        $scope.GetSeoTitleEn = function () {
            $scope.restaurant.AliasEn = commonService.getSeoTitle($scope.restaurant.NameEn);
        }
        uploader.filters.push({
            name: 'fileSize',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var itemSize = item.size / 1024 / 1024;
                if (itemSize > 10.24) {
                    alert('The maximum file size for uploads is 10MB');
                    return;
                }
                var filterFile = [];
                // not filter
                uploader.queue.forEach(function (data) {
                    if (data.file.name == item.name) {
                        filterFile.push(item);
                    }
                });
                if (filterFile.length > 0) {
                    alert('File exists');
                    return;
                }
                return item;
            }
        });
        uploader.queue = [];
        $scope.files = [];
        $scope.locations = [];
        $scope.services = [];
        $scope.initData = function () {
            apiService.get('api/enterprise/getall', null, function (result) {
                $scope.enterprises = result.data;
            }, function (err) {

            });

            apiService.get('api/location/getall', null, function (result) {
                $scope.locations = result.data;
                $scope.locations.forEach(function (item) {
                    item.checked = false;
                })
            }, function (err) {

                });
            apiService.get('/api/servicerestaurant/getall', null, function (result) {
                $scope.services = result.data;
                $scope.services.forEach(function (item) {
                    item.checked = false;
                })
            }, function (err) {

            });
        }

        $scope.locationname = '';
        $scope.searchLocation = function (item) {
            if ($scope.locationname == '') {
                return true;
            }
            if (item.Name.indexOf($scope.locationname) >= 0) {
                return true;
            }
        }
        $scope.servicename = '';
        $scope.searchService = function (item) {
            if ($scope.servicename == '') {
                return true;
            }
            if (item.Name.indexOf($scope.servicename) >= 0) {
                return true;
            }
        }
        $scope.restaurant = {
            Name: '',
            Name: '',
            Address: '',
            AddressEn: '',
            Avatar: '~/Assets/images_none.png',
            Star: 0,
            ViewCount: 0,
            Latitude: 21.774107,
            Longitude: 105.4693488,
            EnterpriseId: '',
            ShortDes: '',
            LongDes: '',
            ShortDesEn: '',
            LongDesEn: '',
            VideoDir: '',
            isActive : true
        }
        var latlng = new google.maps.LatLng($scope.restaurant.Latitude, $scope.restaurant.Longitude);
        var mapOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            scaleControl: true,
            panControl: false
        };
        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        //add Search Box
        var searchBox = new google.maps.places.SearchBox(document.getElementById("mapsearch"));
        document.getElementById('mapsearch').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("mapsearch"));
        // Bias the SearchBox results towards current map's viewport.
        $scope.map.addListener('bounds_changed', function () {
            searchBox.setBounds($scope.map.getBounds());
        });
        searchBox.addListener("places_changed", function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            //bound
            var bounds = new google.maps.LatLngBounds();
            var i, place;
            for (i = 0; place = places[i]; i++) {
                bounds.extend(place.geometry.location);
            }
            $scope.map.fitBounds(bounds);
            $scope.map.setZoom(17);

        });
        //change latitude, longitude when click map
        google.maps.event.addListener($scope.map, "click", function (e) {
            if ($scope.marker !== undefined)
                $scope.marker.setMap(null);
            //lat and lng is available in e object
            var latLng = e.latLng;
            $scope.restaurant.Latitude = latLng.lat();
            $scope.restaurant.Longitude = latLng.lng();
            $scope.marker = new google.maps.Marker({
                map: $scope.map,
                position: latLng,
                title: 'Vị trí nhà hàng'
            });
        });
        $scope.addRestaurant = function () {
            uploader.uploadAll();
            uploader.queue.forEach(function (data) {
                $scope.files.push(data);
            });
            var listLocation = [];
            listLocation = $filter('filter')($scope.locations, { checked: true });
            var locationIds = listLocation.map(function (item) {
                return item.Id;
            }).join(',');
            var listService = [];
            listService = $filter('filter')($scope.services, { checked: true });
            var serviceIds = listService.map(function (item) {
                return item.Id;
            }).join(',');
            $http({
                method: "POST",
                url: "/api/restaurant/create",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("Address", $scope.restaurant.Address);
                    formData.append("Name", $scope.restaurant.Name);
                    formData.append("Alias", $scope.restaurant.Alias);
                    formData.append("AddressEn", $scope.restaurant.AddressEn);
                    formData.append("NameEn", $scope.restaurant.NameEn);
                    formData.append("AliasEn", $scope.restaurant.AliasEn);
                    formData.append("Star", $scope.restaurant.Star);
                    formData.append("ViewCount", $scope.restaurant.ViewCount);
                    formData.append("Avatar", $scope.restaurant.Avatar);
                    formData.append("Latitude", $scope.restaurant.Latitude);
                    formData.append("Longitude", $scope.restaurant.Longitude);
                    formData.append("EnterpriseId", $scope.restaurant.EnterpriseId == '' ? null : parseInt($scope.restaurant.EnterpriseId, 10));
                    formData.append("VideoDir", $scope.restaurant.VideoDir);
                    formData.append("ShortDes", $scope.restaurant.ShortDes);
                    formData.append("LongDes", $scope.restaurant.LongDes);
                    formData.append("ShortDesEn", $scope.restaurant.ShortDesEn);
                    formData.append("LongDesEn", $scope.restaurant.LongDesEn);
                    formData.append("HotLine", $scope.restaurant.HotLine);
                    formData.append("Audio", $scope.restaurant.Audio);
                    formData.append("isActive", $scope.restaurant.isActive);
                    formData.append("OpenDate", $scope.restaurant.OpenDate);
                    formData.append('locationIds', locationIds);
                    formData.append('serviceIds', serviceIds);
                    //now add all of the assigned files
                    if ($scope.files.length != 0) {
                        for (var i = 0; i < $scope.files.length; i++) {
                            formData.append("file" + i, data.files[i]._file);
                        }
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    model: $scope.model,
                    files: $scope.files
                }

            }).success(function (data, status, headers, config) {
                uploader.queue = [];
                notificationService.displaySuccess("Thêm mới nhà hàng thành công");
                $state.go('restaurants');
            }).
            error(function (data, status, headers, config) {
                uploader.queue = [];
                notificationService.displayError("Cập nhật nhà hàng thất bại");
            });
        }

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info("onWhenAddingFileFailed", item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            //$scope.files.push(fileItem);
            let reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    fileItem.uploadsrc = e.target.result;
                });

            });
            console.info("onAfterAddingFile", fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            // $scope.files.push(addedFileItems);
            console.info("onAfterAddingAll", addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info("onBeforeUploadItem", item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info("onProgressItem", fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info("onProgressAll", progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info("onSuccessItem", fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info("onErrorItem", fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info("onCancelItem", fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info("onCompleteItem", fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info("onCompleteAll");
        };
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.restaurant.Avatar = e.target.result;
                });

            });
            //  console.log(e.target.files[0])
        }
        $scope.chooseAudioFile = function (fileUrl) {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.$apply(function () {
                    $scope.restaurant.Audio = fileUrl;
                })
            }
            finder.popup();
        }
    }

})(angular.module('admin.restaurant'));