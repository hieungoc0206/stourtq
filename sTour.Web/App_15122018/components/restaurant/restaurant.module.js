﻿(function () {
    angular.module('admin.restaurant', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('restaurants', {
            url: '/restaurants',
            parent: 'baseAdmin',
            templateUrl: 'App/components/restaurant/restaurantListView.html',
            controller: 'restaurantListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/restaurant/restaurantListController.js'
                        ]
                    })
                }]
            }
        })
        .state('add-restaurant', {
            url: '/add-restaurant',
            parent: 'baseAdmin',
            templateUrl: 'App/components/restaurant/restaurantAddView.html',
            controller: 'restaurantAddController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/restaurant/restaurantAddController.js'
                        ]
                    })
                }]
            }
        })
        .state('edit-restaurant', {
            url: '/edit-restaurant/:id',
            parent: 'baseAdmin',
            templateUrl: 'App/components/restaurant/restaurantEditView.html',
            controller: 'restaurantEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/restaurant/restaurantEditController.js'
                        ]
                    })
                }]
            }
        })
    }

})();