﻿(function (app) {
    app.controller('culturalListController', culturalListController);

    culturalListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function culturalListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.culturals = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getCulturals = function (page) {
            page = page || 1;
            apiService.get(`/api/cultural/getculturals?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {

                $scope.culturals = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.cultural'));