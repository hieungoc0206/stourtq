﻿(function () {
    angular.module('admin.cultural', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('culturals', {
            url: '/culturals',
            parent: 'baseAdmin',
            templateUrl: 'App/components/cultural/culturalListView.html',
            controller: 'culturalListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/cultural/culturalListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-cultural', {
                url: '/add-cultural',
                parent: 'baseAdmin',
                templateUrl: 'App/components/cultural/culturalAddView.html',
                controller: 'culturalAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/cultural/culturalAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-cultural', {
                url: '/edit-cultural/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/cultural/culturalEditView.html',
                controller: 'culturalEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/cultural/culturalEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();