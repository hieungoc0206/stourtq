﻿(function (app) {
    'use strict';
    app.controller('adminController', adminController);
    adminController.$inject = ['$scope', 'authData', 'apiService', 'notificationService', '$location']
    function adminController($scope, authData, apiService, notificationService, $location) {
        if (!authData.authenticationData.IsAuthenticated) {
            $location.path('/login');
            //notificationService.displayError('Sorry nhé ahihi')
        }
        $scope.user = authData.authenticationData;
    }
})(angular.module('app'));