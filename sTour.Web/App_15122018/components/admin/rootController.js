﻿(function (app) {
    app.controller('rootController', rootController);

    rootController.$inject = ['$state', 'authData', 'loginService', '$scope', 'authenticationService','apiService'];

    function rootController($state, authData, loginService, $scope, authenticationService, apiService) {
        $scope.logOut = function () {
            loginService.logOut();
            $state.go('login');
        }
        $scope.authentication = authData.authenticationData;
        
       
    }
})(angular.module('app'));