﻿(function (app) {
    app.controller('touristserviceAddController', touristserviceAddController);

    touristserviceAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function touristserviceAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        $scope.touristservice = {
            Avatar: '~/Assets/images_none.png',
        }
        $scope.categories = categories;
        $scope.addtouristservice = function () {         
            apiService.post('/api/touristservice/create/', $scope.touristservice,
                function (result) {
                    notificationService.displaySuccess('Thêm mới thành công');
                    $state.go('tourist-service');
                }, function (error) {
                    notificationService.displayError(err.data.Message);
                });
                
        }
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.touristservice.Avatar = e.target.result;
                });

            });         
        }
    }

})(angular.module('admin.touristservice'));