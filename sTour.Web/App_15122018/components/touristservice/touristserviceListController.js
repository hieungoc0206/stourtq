﻿(function (app) {
    app.controller('touristserviceListController', touristserviceListController);

    touristserviceListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function touristserviceListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.touristservices = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.gettouristservices = function (page) {
            page = page || 1;
            apiService.get(`/api/touristservice/gettouristservices?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.touristservices = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.touristservice'));