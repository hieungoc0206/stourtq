﻿(function () {
    angular.module('admin.touristservice', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('tourist-service', {
            url: '/tourist-service',
            parent: 'baseAdmin',
            templateUrl: 'App/components/touristservice/touristserviceListView.html',
            controller: 'touristserviceListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/touristservice/touristserviceListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-tourist-service', {
                url: '/add-tourist-service',
                parent: 'baseAdmin',
                templateUrl: 'App/components/touristservice/touristserviceAddView.html',
                controller: 'touristserviceAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/touristservice/touristserviceAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-tourist-service', {
                url: '/edit-tourist-service/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/touristservice/touristserviceEditView.html',
                controller: 'touristserviceEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/touristservice/touristserviceEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();