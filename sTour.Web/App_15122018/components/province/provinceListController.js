﻿(function (app) {
    app.controller('provinceListController', provinceListController);

    provinceListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function provinceListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.provinces = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getProvinces = function (page) {
            page = page || 1;
            apiService.get(`/api/province/getprovinces?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.provinces = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.province'));