﻿(function (app) {
    app.controller('provinceImportController', provinceImportController);

    provinceImportController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function provinceImportController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        
        $scope.files = [];
        $scope.CountryId = 0;
        $scope.initData = function () {
            apiService.get('/api/country/getall/', null,
                function (result) {
                    $scope.countries = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }
        //listen for the file selected event
        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                //add the file object to the scope's files collection
                $scope.files.push(args.file);
            });
        });
        $scope.ImportProvince = function () {
           
            $http({
                method: 'POST',
                url: "/api/province/import",
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();
                    //need to convert our json object to a string version of json otherwise
                    // the browser will do a 'toString()' on the object which will result 
                    // in the value '[Object object]' on the server.
                    formData.append("CountryId", angular.toJson(data.CountryId));
                    //now add all of the assigned files
                    for (var i = 0; i < data.files.length; i++) {
                        //add each file to the form data and iteratively name them
                        formData.append("file" + i, data.files[i]);
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    CountryId: $scope.CountryId,
                    files: $scope.files
                }
            }).then(function (result, status, headers, config) {
                
                notificationService.displaySuccess("Import File thành công");
                $state.go('provinces');
            },
                function (data, status, headers, config) {
                    
                    notificationService.displayError(data.data.Message);
                });
        }           
    }

})(angular.module('admin.province'));