﻿(function (app) {
    app.controller('provinceEditController', provinceEditController);

    provinceEditController.$inject = ['$scope', 'apiService', '$http', 'notificationService', '$stateParams', 'FileUploader', '$state', '$filter'];

    function provinceEditController($scope, apiService, $http, notificationService, $stateParams, FileUploader, $state, $filter) {
        $scope.initData = function () {          
            apiService.get('/api/province/getbyid/' + $stateParams.id, null, function (result) {
                $scope.province = result.data;
            }, function (err) {

                });
            apiService.get('/api/country/getall/', null,
                function (result) {
                    $scope.countries = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }
        
        $scope.editProvince = function () {

            apiService.put('/api/province/edit/', $scope.province,
                function (result) {
                    notificationService.displaySuccess('Sửa mới thành công');
                    $state.go('provinces');
                }, function (error) {
                    notificationService.displayError('Sửa mới không thành công.');
                });

        }
    }

})(angular.module('admin.province'));