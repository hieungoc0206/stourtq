﻿(function () {
    angular.module('admin.province', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('provinces', {
            url: '/provinces',
            parent: 'baseAdmin',
            templateUrl: 'App/components/province/provinceListView.html',
            controller: 'provinceListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/province/provinceListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-province', {
                url: '/add-province',
                parent: 'baseAdmin',
                templateUrl: 'App/components/province/provinceAddView.html',
                controller: 'provinceAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/province/provinceAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('import-province', {
                url: '/import-province',
                parent: 'baseAdmin',
                templateUrl: 'App/components/province/provinceImportView.html',
                controller: 'provinceImportController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/province/provinceImportController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-province', {
                url: '/edit-province/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/province/provinceEditView.html',
                controller: 'provinceEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/province/provinceEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();