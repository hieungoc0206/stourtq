﻿(function (app) {
    app.controller('districtAddController', districtAddController);

    districtAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function districtAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        $scope.district = {

        }
        $scope.initData = function () {
            apiService.get('/api/province/getall/', null,
                function (result) {
                    $scope.provinces = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }
        $scope.addDistrict = function () {

            apiService.post('/api/district/create/', $scope.district,
                function (result) {
                    notificationService.displaySuccess('Thêm mới thành công');
                    $state.go('districts');
                }, function (error) {
                    notificationService.displayError('Thêm mới không thành công.');
                });

        }
    }

})(angular.module('admin.district'));