﻿(function (app) {
    app.controller('districtImportController', districtImportController);

    districtImportController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function districtImportController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        
        $scope.files = [];
        $scope.ProvincialId = 0;
        $scope.initData = function () {
            apiService.get('/api/province/getall/', null,
                function (result) {
                    $scope.provinces = result.data;
                }, function (error) {
                    console.log('Cannot get list parent');
                });
        }
        //listen for the file selected event
        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                //add the file object to the scope's files collection
                $scope.files.push(args.file);
            });
        });
        $scope.ImportDistrict = function () {
           
            $http({
                method: 'POST',
                url: "/api/district/import",
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("ProvincialId", angular.toJson(data.ProvincialId));
                    //now add all of the assigned files
                    for (var i = 0; i < data.files.length; i++) {
                        //add each file to the form data and iteratively name them
                        formData.append("file" + i, data.files[i]);
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    ProvincialId: $scope.ProvincialId,
                    files: $scope.files
                }
            }).then(function (result, status, headers, config) {

                notificationService.displaySuccess("Import File thành công");
                $state.go('districts');
            },
                function (data, status, headers, config) {

                    notificationService.displayError("Import File thất bại");
                });
        }
       
    }

})(angular.module('admin.district'));