﻿(function (app) {
    app.controller('servicehotelAddController', servicehotelAddController);

    servicehotelAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', '$filter'];

    function servicehotelAddController($scope, apiService, $http, notificationService, FileUploader, $state, $filter) {
        $scope.servicehotel = {
            Name: '',
            NameEn: '',
            Icon: '~/Assets/images_none.png',
            Description: '',
            DescriptionEn: ''
        }       
        $scope.addServiceHotel = function () {
            
            apiService.post('/api/servicehotel/create/', $scope.servicehotel,
                function (result) {
                    notificationService.displaySuccess('Thêm mới thành công');
                    $state.go('servicehotels');
                }, function (error) {
                    notificationService.displayError('Thêm mới không thành công.');
                });
        }
        //update Icon
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1) {
                notificationService.displayError("Kích thước icon không thể vượt quá 1MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.servicehotel.Icon = e.target.result;
                });

            });
        }
    }

})(angular.module('admin.servicehotel'));