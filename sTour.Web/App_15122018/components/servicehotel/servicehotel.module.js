﻿(function () {
    angular.module('admin.servicehotel', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('servicehotels', {
            url: '/servicehotels',
            parent: 'baseAdmin',
            templateUrl: 'App/components/servicehotel/servicehotelListView.html',
            controller: 'servicehotelListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/servicehotel/servicehotelListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-servicehotel', {
                url: '/add-servicehotel',
                parent: 'baseAdmin',
                templateUrl: 'App/components/servicehotel/servicehotelAddView.html',
                controller: 'servicehotelAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/servicehotel/servicehotelAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-servicehotel', {
                url: '/edit-servicehotel/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/servicehotel/servicehotelEditView.html',
                controller: 'servicehotelEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/servicehotel/servicehotelEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();