﻿(function () {
    angular.module('admin.role', ['stour.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('roles', {
            url: '/roles',
            templateUrl: '/App/components/role/roleListView.html',
            parent: 'baseAdmin',
            controller: 'roleListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/role/roleListController.js'
                        ]
                    })
                }]
            }
        })
        .state('permissions', {
            url: '/permissions',
            templateUrl: '/App/components/role/permissionListView.html',
            parent: 'baseAdmin',
            controller: 'permissionListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/role/permissionListController.js'
                        ]
                    })
                }]
            }
        })
        .state('add-role', {
            url: '/add-role',
            templateUrl: '/App/components/role/roleAddView.html',
            parent: 'baseAdmin',
            controller: 'roleAddController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/role/roleAddController.js'
                        ]
                    })
                }]
            }
        })
        .state('edit-role', {
            url: '/edit-role/:id',
            templateUrl: '/App/components/role/roleEditView.html',
            parent: 'baseAdmin',
            controller: 'roleEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/role/roleEditController.js'
                        ]
                    })
                }]
            }
        })
        .state('add-permisstion', {
            url: '/add-permisstion/:id/:name',
            templateUrl: '/App/components/role/addPermissionView.html',
            parent: 'baseAdmin',
            controller: 'addPermissionController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/role/addPermissionController.js'
                        ]
                    })
                }]
            }
        })
        ;
    }

})();