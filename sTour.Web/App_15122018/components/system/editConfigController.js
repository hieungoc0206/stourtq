﻿(function (app) {
    app.controller('editConfigController', editConfigController);

    editConfigController.$inject = ['$scope', 'apiService', 'notificationService', '$ngBootbox'];

    function editConfigController($scope, apiService, notificationService, $ngBootbox) {
        $scope.config = {};
        $scope.updateConfig = updateConfig;

        function updateConfig() {
            $ngBootbox.confirm('Bạn có chắc chắn thay đổi cấu hình hệ thống không ?').then(function () {
                apiService.put('/api/systemconfig/edit', $scope.config, function (result) {
                    if (result.data == undefined) {
                        notificationService.displayError('Cập nhật cấu hình thất bại');
                        return;
                    }
                        notificationService.displaySuccess('Cập nhật cấu hình thành công');
                        getConfig();
                }, function (err) {
                    notificationService.displayError('Cập nhật cấu hình thất bại');
                });
            }, function (err) {

            });

        }

        function getConfig() {
            apiService.get('/api/systemconfig/getconfig', $scope.config, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError('Lấy cấu hình hệ thống thất bại');
                    return;
                }
                $scope.config = result.data;
            }, function (err) {

            });
        }

        getConfig();
    }

})(angular.module('admin.system'));