﻿(function () {
    angular.module('admin.system', ['stour.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('editSystem', {
            url: '/editSystem',
            templateUrl: '/App/components/system/systemConfigView.html',
            parent: 'baseAdmin',
            controller: 'editConfigController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'app/components/system/editConfigController.js'
                        ]
                    })
                }]
            }
        })
        .state('backup', {
            url: '/backup',
            templateUrl: '/App/components/system/backupView.html',
            parent: 'baseAdmin',
            controller: 'backupController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/system/backupController.js'
                        ]
                    })
                }]
            }
        })
        .state('systeminfo', {
            url: '/systeminfo',
            templateUrl: '/App/components/system/systeminfoView.html',
            parent: 'baseAdmin',
            controller: 'systeminfoController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/system/systeminfoController.js'
                        ]
                    })
                }]
            }
        })
        .state('errors', {
            url: '/errors',
            parent: 'baseAdmin',
            templateUrl: '/App/components/system/exceptionListView.html',
            controller: 'exceptionListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/system/exceptionListController.js'
                        ]
                    })
                }]
            }
        });
    }
})();