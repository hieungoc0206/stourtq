﻿(function (app) {
    app.controller('systeminfoController', systeminfoController);

    systeminfoController.$inject = ['$scope', 'apiService', 'notificationService', 'commonService'];

    function systeminfoController($scope, apiService, notificationService, commonService) {
        $scope.system = {};
        function getSystemInfo() {
            apiService.get('/api/systemconfig/systeminfo', null, function (result) {
                if (result.data == undefined) {
                    return;
                }
                $scope.system = result.data;
                $scope.system.ServerLocalTime = commonService.convertDateTimeService($scope.system.ServerLocalTime);
                $scope.system.UtcTime = commonService.convertDateTimeService($scope.system.UtcTime);
            }, function (err) {

            });
        }

        getSystemInfo();
    }

})(angular.module('admin.system'))