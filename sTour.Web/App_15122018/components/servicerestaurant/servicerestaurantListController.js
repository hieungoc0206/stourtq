﻿(function (app) {
    app.controller('servicerestaurantListController', servicerestaurantListController);

    servicerestaurantListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function servicerestaurantListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.servicerestaurants = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getServiceRestaurants = function (page) {
            page = page || 1;
            apiService.get(`/api/servicerestaurant/getservicerestaurants?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.servicerestaurants = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.servicerestaurant'));