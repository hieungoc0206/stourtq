﻿(function () {
    angular.module('admin.servicerestaurant', ['stour.common']).config(config).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({

        })
    }]);;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('servicerestaurants', {
            url: '/servicerestaurants',
            parent: 'baseAdmin',
            templateUrl: 'App/components/servicerestaurant/servicerestaurantListView.html',
            controller: 'servicerestaurantListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'App/components/servicerestaurant/servicerestaurantListController.js'
                        ]
                    })
                }]
            }
        })
            .state('add-servicerestaurant', {
                url: '/add-servicerestaurant',
                parent: 'baseAdmin',
                templateUrl: 'App/components/servicerestaurant/servicerestaurantAddView.html',
                controller: 'servicerestaurantAddController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/servicerestaurant/servicerestaurantAddController.js'
                            ]
                        })
                    }]
                }
            })
            .state('edit-servicerestaurant', {
                url: '/edit-servicerestaurant/:id',
                parent: 'baseAdmin',
                templateUrl: 'App/components/servicerestaurant/servicerestaurantEditView.html',
                controller: 'servicerestaurantEditController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'App/components/servicerestaurant/servicerestaurantEditController.js'
                            ]
                        })
                    }]
                }
            })
    }

})();