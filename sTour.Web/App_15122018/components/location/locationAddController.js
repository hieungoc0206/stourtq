﻿(function (app) {
    app.controller('locationAddController', locationAddController);

    locationAddController.$inject = ['$scope', 'apiService', '$http', 'notificationService', 'FileUploader', '$state', 'commonService'];

    function locationAddController($scope, apiService, $http, notificationService, FileUploader, $state, commonService) {
        var uploader = $scope.uploader = new FileUploader({

        });
        $scope.GetSeoTitle = function () {
            $scope.location.Alias = commonService.getSeoTitle($scope.location.Name);
        }
        $scope.GetSeoTitleEn = function () {
            $scope.location.AliasEn = commonService.getSeoTitle($scope.location.NameEn);
        }
        uploader.filters.push({
            name: 'fileSize',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var itemSize = item.size / 1024 / 1024;
                if (itemSize > 10.24) {
                    alert('The maximum file size for uploads is 10MB');
                    return;
                }
                var filterFile = [];
                // not filter
                uploader.queue.forEach(function (data) {
                    if (data.file.name == item.name) {
                        filterFile.push(item);
                    }
                });
                if (filterFile.length > 0) {
                    alert('File exists');
                    return;
                }
                return item;
            }
        });

        uploader.queue = [];

        $scope.files = [];
        $scope.categories = [];

        $scope.getCategories = function () {
            apiService.get('/api/categorylocation/getall', null, function (result) {

                $scope.categories = result.data;

            }, function (err) {

            });
        }

        $scope.location = {
            Name: '',
            NameEn: '',
            Address: '',
            AddressEn: '',
            Avatar: '~/Assets/images_none.png',
            Star: 0,
            ViewCount: 0,
            Latitude: 21.774107,
            Longitude: 105.4693488,
            LocationCategoryId: '',
            ShortDes: '',
            ShortDesEn: '',
            LongDes: '',
            LongDesEn: '',
            VideoDir: '',
            VideoDirEn: '',
            isActive: true
        }

        var latlng = new google.maps.LatLng($scope.location.Latitude, $scope.location.Longitude);
        var mapOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            scaleControl: true,
            panControl: false
        };
        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        //add Search Box
        var searchBox = new google.maps.places.SearchBox(document.getElementById("mapsearch"));
        document.getElementById('mapsearch').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("mapsearch"));
        // Bias the SearchBox results towards current map's viewport.
        $scope.map.addListener('bounds_changed', function () {
            searchBox.setBounds($scope.map.getBounds());
        });
        searchBox.addListener("places_changed", function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            //bound
            var bounds = new google.maps.LatLngBounds();
            var i, place;
            for (i = 0; place = places[i]; i++) {
                bounds.extend(place.geometry.location);
            }
            $scope.map.fitBounds(bounds);
            $scope.map.setZoom(17);

        });
        //change latitude, longitude when click map
        google.maps.event.addListener($scope.map, "click", function (e) {
            //remove marker before
            if ($scope.marker !== undefined)
                $scope.marker.setMap(null);
            //lat and lng is available in e object
            var latLng = e.latLng;
            $scope.location.Latitude = latLng.lat();
            $scope.location.Longitude = latLng.lng();
            $scope.marker = new google.maps.Marker({
                map: $scope.map,
                position: latLng,
                title: 'Vị trí địa danh'
            });
        });
        $scope.addLocation = function () {
            uploader.uploadAll();
            uploader.queue.forEach(function (data) {
                $scope.files.push(data)
            });
            $http({
                method: "POST",
                url: "/api/location/create",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("Address", $scope.location.Address);
                    formData.append("AddressEn", $scope.location.AddressEn);
                    formData.append("Name", $scope.location.Name);
                    formData.append("NameEn", $scope.location.NameEn);
                    formData.append("Alias", $scope.location.Alias);
                    formData.append("AliasEn", $scope.location.AliasEn);
                    formData.append("ViewCount", $scope.location.ViewCount);
                    formData.append("Avatar", $scope.location.Avatar);
                    formData.append("Latitude", $scope.location.Latitude);
                    formData.append("Longitude", $scope.location.Longitude);
                    formData.append("LocationCategoryId", $scope.location.LocationCategoryId);
                    formData.append("VideoDir", $scope.location.VideoDir);
                    formData.append("VideoDirEn", $scope.location.VideoDirEn);
                    formData.append("ShortDes", $scope.location.ShortDes);
                    formData.append("ShortDesEn", $scope.location.ShortDesEn);
                    formData.append("LongDes", $scope.location.LongDes);
                    formData.append("LongDesEn", $scope.location.LongDesEn);
                    formData.append("Audio", $scope.location.Audio);
                    formData.append("AudioEn", $scope.location.AudioEn);
                    formData.append("OpenDate", $scope.location.OpenDate);
                    formData.append("PhoneNumber", $scope.location.PhoneNumber);
                    formData.append("isActive", $scope.location.isActive);
                    //now add all of the assigned files
                    if ($scope.files.length != 0) {
                        for (var i = 0; i < $scope.files.length; i++) {
                            formData.append("file" + i, data.files[i]._file);
                        }
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                data: {
                    model: $scope.model,
                    files: $scope.files
                }

            }).success(function (data, status, headers, config) {
                uploader.queue = [];
                notificationService.displaySuccess("Thêm mới địa danh thành công");
                $state.go('locations');
            }).
                error(function (data, status, headers, config) {
                    uploader.queue = [];
                    notificationService.displayError("Thêm mới địa danh thất bại");
                });
        }

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info("onWhenAddingFileFailed", item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            //$scope.files.push(fileItem);
            let reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    fileItem.uploadsrc = e.target.result;
                });

            });
            console.info("onAfterAddingFile", fileItem);

        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            // $scope.files.push(addedFileItems);
            console.info("onAfterAddingAll", addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info("onBeforeUploadItem", item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info("onProgressItem", fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info("onProgressAll", progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info("onSuccessItem", fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info("onErrorItem", fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info("onCancelItem", fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info("onCompleteItem", fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info("onCompleteAll");
        };
        //update avatar
        $scope.uploadFile = function (e) {
            let reader = new FileReader();
            let file = e.target.files[0];
            var size = file.size / 1024 / 1024;
            if (size > 1.5) {
                notificationService.displayError("Kích thước ảnh avatar không thể vượt quá 1.5MB");
                return;
            }
            reader.readAsDataURL(file);
            reader.addEventListener("load", function (e) {
                e.preventDefault();
                $scope.$apply(function () {
                    $scope.location.Avatar = e.target.result;
                });

            });
            //  console.log(e.target.files[0])
        }
        $scope.chooseAudioFile = function (fileUrl) {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.$apply(function () {
                    $scope.location.Audio = fileUrl;
                })
            }
            finder.popup();
        }
        $scope.chooseAudioFileEn = function (fileUrl) {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.$apply(function () {
                    $scope.location.AudioEn = fileUrl;
                })
            }
            finder.popup();
        }
    }

})(angular.module('admin.location'));

