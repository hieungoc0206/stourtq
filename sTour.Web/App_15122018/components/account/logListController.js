﻿(function (app) {
    app.controller('logListController', logListController);

    logListController.$inject = ['$scope', 'apiService', 'notificationService', '$state'];

    function logListController($scope, apiService, notificationService, $state) {
        $scope.logs = [];
        $scope.page = 0;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getLogs = getLogs;
        function getLogs(page) {
            page = page || 1;
            var config = {
                params: {
                    page: page,
                    pageSize: 10
                }
            }
            apiService.get('/api/activitylog/getall', config, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Lấy dữ liệu thất bại");
                    return;
                }
                var object = result.data;
                $scope.logs = object.Items;
                $scope.TotalCount = object.TotalCount;
                $scope.pagesCount = object.TotalPages;
            }, function (err) {

            });
        }

        getLogs();
    }

})(angular.module('admin.account'));