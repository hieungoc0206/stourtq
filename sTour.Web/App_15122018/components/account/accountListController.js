﻿(function (app) {
    app.controller('accountListController', accountListController);

    accountListController.$inject = ['$scope', 'apiService', 'notificationService', '$state', '$ngBootbox'];

    function accountListController($scope, apiService, notificationService, $state, $ngBootbox) {
        $scope.users = [];
        $scope.page = 0;
        $scope.user = {};
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.keyword = '';
        $scope.SelectUser = function (user) {
            $scope.user = user;
        }
        //Change Pass
        $scope.password = '';
        $scope.veritypass = '';
        $scope.changePass = function () {
            $ngBootbox.confirm("Bạn có chắc chắn thay đổi mật khẩu không ?").then(function () {
                if ($scope.password == $scope.veritypass) {
                    var url = `/api/user/changepassuser?userId=${$scope.user.Id}&password=${$scope.password}`;
                    apiService.put(url, null, function (result) {
                        if (result == undefined) {
                            notificationService.displayError("Cập nhật mật khẩu thất bại");
                            return;
                        }
                        var object = result.data;
                            notificationService.displaySuccess(object);
                            $scope.password = '';
                            $scope.veritypass = '';
                            $('#changePass').modal('hide');
                    }, function (err) {
                        $scope.password = '';
                        $scope.veritypass = '';
                        $('#changePass').modal('hide');
                        notificationService.displayError(err.data.Message);
                    });
                } else {
                    notificationService.displayError("Hai mật khẩu không giống nhau");
                }
            }, function () {

            });
        }

        //Get Users
        $scope.getUsers = getUsers;

        //Lock User
        $scope.LockUser = function (UserId, Status) {
            var message;
            if (Status) {
                message = "Bạn có chắc chắn khóa tài khoản này ?";
            } else {
                message = "Bạn có chắc chắn mở tài khoản này ?";
            }
            $ngBootbox.confirm(message).then(function () {
                var url = `/api/user/lockuser?UserId=${UserId}&Status=${Status}`;
                apiService.put(url, null, function (result) {
                    if (result == undefined) {
                        notificationService.displayError("Cập nhật thất bại");
                        return;
                    }

                    if (result.data) {
                        notificationService.displaySuccess("Cập nhật thành công");
                        return;
                    } else {
                        notificationService.displayError("Cập nhật thất bại");
                        return;
                    }
                }, function (err) {
                    notificationService.displayError("Cập nhật thất bại");
                });
            }, function () {
                $scope.users.forEach(function (item) {
                    if (item.Id == UserId) {
                        item.LockoutEnabled = !Status;
                        return;
                    }
                });
            });

        }

        function getUsers(page) {
            page = page || 1;
            var config = {
                params: {
                    page: page,
                    pageSize: 10,
                    keyword: $scope.keyword
                }
            }
            apiService.get('/api/user/getall', config, function (result) {
                if (result.data == undefined) {
                    notificationService.displayError("Không thể tải được dữ liệu");
                    return;
                }
                var object = result.data;            
                $scope.users = object.Items;
                $scope.TotalCount = object.TotalCount;
                $scope.pagesCount = object.TotalPages;
            }, function (err) {

            });
        }

        getUsers();

        //Phân quyền người dùng
        $scope.roleUser = {};
        $scope.decentral = function () {
            $ngBootbox.confirm("Bạn có chắc chắn cập nhật ?").then(function () {
                var data = JSON.stringify($scope.roleUser);
                apiService.post('/api/user/updateroleuser?roleUserModel=' + data, null, function (result) {
                    if (result == undefined) {
                        notificationService.displayError("Phân quyền thất bại");
                        return;
                    }
                    var object = result.data;
                    if (object) {
                        notificationService.displaySuccess("Phân quyền thành công");
                        $('#decentralization').modal('hide');
                    }
                }, function (err) {
                    notificationService.displayError(err.data.Message);
                    $('#decentralization').modal('hide');
                });
            }, function () {

            });
        }

        $scope.decentralUser = function (user) {
            $scope.user = user;
            apiService.get('/api/user/getrolemodelbyuser?UserId=' + user.Id, null, function (result) {
                $scope.roleUser = result.data;
            }, function (err) {

            });
        }
        
    }

})(angular.module('admin.account'));