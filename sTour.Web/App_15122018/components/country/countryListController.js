﻿(function (app) {
    app.controller('countryListController', countryListController);

    countryListController.$inject = ['$scope', 'apiService', '$state', 'notificationService'];

    function countryListController($scope, apiService, $statem, notificationService) {
        $scope.keyword = '';
        $scope.countries = [];
        $scope.page = 1;
        $scope.pagesCount = 0;
        $scope.TotalCount = 0;
        $scope.getCountries = function (page) {
            page = page || 1;
            apiService.get(`/api/country/getcountries?page=${page}&pageSize=10&keyword=${$scope.keyword}`, null, function (result) {
                $scope.countries = result.data.Items;
                $scope.pagesCount = result.data.TotalPages;
                $scope.TotalCount = result.data.TotalCount;
            }, function (err) {
                notificationService.displayError("Không lấy được dữ liệu");
            });
        }
    }

})(angular.module('admin.country'));