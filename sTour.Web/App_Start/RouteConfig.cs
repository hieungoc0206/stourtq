﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace sTour.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
              name: "Login",
              url: "dang-nhap-he-thong",
              defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional },
              namespaces: new string[] { "sTour.Web.Controllers" }
             );
            routes.MapRoute(
              name: "Register",
              url: "dang-ky",
              defaults: new { controller = "Account", action = "Register", id = UrlParameter.Optional },
              namespaces: new string[] { "sTour.Web.Controllers" }
             );
            routes.MapRoute(
              name: "Hotels",
              url: "khach-san.html",
              defaults: new { controller = "Hotel", action = "Index", id = UrlParameter.Optional },
              namespaces: new string[] { "sTour.Web.Controllers" }
             );
            routes.MapRoute(
               name: "Hotel Detail",
               url: "hotel/{alias}-h.{id}.html",
               defaults: new { controller = "Hotel", action = "Detail", id = UrlParameter.Optional },
                 namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
              name: "Locations",
              url: "dia-danh.html",
              defaults: new { controller = "Location", action = "Index", id = UrlParameter.Optional },
              namespaces: new string[] { "sTour.Web.Controllers" }
             );
            routes.MapRoute(
             name: "Location Detail",
             url: "location/{alias}-lc.{id}.html",
             defaults: new { controller = "Location", action = "Detail", id = UrlParameter.Optional },
             namespaces: new string[] { "sTour.Web.Controllers" }
         );
            routes.MapRoute(
            name: "Restaurants",
            url: "nha-hang.html",
            defaults: new { controller = "Restaurant", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
               name: "Restaurant Detail",
               url: "restaurant/{alias}-r.{id}.html",
               defaults: new { controller = "Restaurant", action = "Detail", id = UrlParameter.Optional },
               namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
            name: "Foods",
            url: "am-thuc.html",
            defaults: new { controller = "Foods", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
               name: "Foods Detail",
               url: "food/{alias}-f.{id}.html",
               defaults: new { controller = "Foods", action = "Detail", id = UrlParameter.Optional },
               namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
            name: "Cultural",
            url: "van-hoa-le-hoi.html",
            defaults: new { controller = "Cultural", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
               name: "Cultural Detail",
               url: "cultural/{alias}-c.{id}.html",
               defaults: new { controller = "Cultural", action = "Detail", id = UrlParameter.Optional },
               namespaces: new string[] { "sTour.Web.Controllers" }
           );
            routes.MapRoute(
           name: "Map",
           url: "ban-do.html",
           defaults: new { controller = "Map", action = "Index", id = UrlParameter.Optional },
           namespaces: new string[] { "sTour.Web.Controllers" }
          );
            routes.MapRoute(
           name: "WeatherForecast",
           url: "du-bao-thoi-tiet.html",
           defaults: new { controller = "Home", action = "WeatherForecast", id = UrlParameter.Optional },
           namespaces: new string[] { "sTour.Web.Controllers" }
          );
            routes.MapRoute(
          name: "About",
          url: "gioi-thieu.html",
          defaults: new { controller = "Home", action = "About", id = UrlParameter.Optional },
          namespaces: new string[] { "sTour.Web.Controllers" }
         );
            routes.MapRoute(
         name: "TouristService",
         url: "dich-vu-tien-ich.html",
         defaults: new { controller = "TouristService", action = "Index", id = UrlParameter.Optional },
         namespaces: new string[] { "sTour.Web.Controllers" }
        );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
