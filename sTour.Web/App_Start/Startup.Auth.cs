﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using sTour.Common;
using sTour.Data;
using sTour.Entity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace sTour.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(STourContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
            app.CreatePerOwinContext<UserManager<ApplicationUser>>(CreateManager);

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/oauth/token"),
                Provider = new AuthorizationServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(1440),
                AllowInsecureHttp = true,
            });
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/dang-nhap-he-thong"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(1440),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "77a4589a-8ace-4f57-8067-1fd52f0be7a1",
            //    clientSecret: "nugyvN1108[=jvASMIGB6|]");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            app.UseFacebookAuthentication(
               appId: "246973889334837",
               appSecret: "b45043a2ac8abe40707a001183b3963f");

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "402142382113-hhbr4736hp48orjq3h463tockhjnqued.apps.googleusercontent.com",
                ClientSecret = "IE0EnMHp9zYoaCJOqPKwou3r"
            });
        }

        public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
        {
            public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
            {
                context.Validated();
            }

            public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
            {
                var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

                if (allowedOrigin == null) allowedOrigin = "*";

                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                UserManager<ApplicationUser> userManager = context.OwinContext.GetUserManager<UserManager<ApplicationUser>>();
                RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new STourContext()));
                ApplicationUser user;
                try
                {
                     user = await userManager.FindAsync(context.UserName, context.Password);
                }
                catch(Exception ex)
                {
                    // Could not retrieve the user due to error.
                    context.Rejected();
                    context.SetError("server_error",ex.Message);           
                    return;
                }
                if (user != null)
                {
                    if (!user.LockoutEnabled)
                    {
                        var roles = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new STourContext()));
                        //var role = roles.Roles.ToList();
                        foreach (var role in roles.Roles.ToList())
                        {
                            if (role.Name == "User" | role.Name==null)
                            {
                                context.Rejected();
                                context.SetError("invalid_grant", "Truy cập bị chặn");
                            }
                            else
                            {
                                if ((user.Roles.Where(c => c.RoleId == roleManager.FindByNameAsync(role.Name).Result.Id).FirstOrDefault() != null))
                                {
                                    ClaimsIdentity identity = await userManager.CreateIdentityAsync(
                                                                      user,
                                                                      DefaultAuthenticationTypes.ExternalBearer);
                                    string Ip = IPAddress.GetIPAddress();
                                    user.LoginEndIP = Ip;
                                    user.LoginCount = user.LoginCount + 1;
                                    user.LoginEndDate = DateTime.Now;
                                    await userManager.UpdateAsync(user);
                                    context.Validated(identity);
                                    break;
                                }

                            }
                        }
                        //context.Rejected();
                        //context.SetError("invalid_grant", "Truy cập bị chặn");
                        //var roleAdmin = roleManager.FindByNameAsync("Admin").Result.Id;
                        //var roleEnterprise = roleManager.FindByNameAsync("Doanh nghiệp").Result.Id;

                        //if ((user.Roles.Where(c => c.RoleId == roleAdmin).FirstOrDefault() != null))
                        //{
                        //    ClaimsIdentity identity = await userManager.CreateIdentityAsync(
                        //                                      user,
                        //                                      DefaultAuthenticationTypes.ExternalBearer);
                        //    string Ip = IPAddress.GetIPAddress();
                        //    user.LoginEndIP = Ip;
                        //    user.LoginCount = user.LoginCount + 1;
                        //    user.LoginEndDate = DateTime.Now;
                        //    await userManager.UpdateAsync(user);
                        //    context.Validated(identity);
                        //}
                        //else if(user.Roles.Where(c=>c.RoleId==roleEnterprise).FirstOrDefault()!=null)
                        //{
                        //    ClaimsIdentity identity = await userManager.CreateIdentityAsync(
                        //                                      user,
                        //                                      DefaultAuthenticationTypes.ExternalBearer);
                        //    string Ip = IPAddress.GetIPAddress();
                        //    user.LoginEndIP = Ip;
                        //    user.LoginCount = user.LoginCount + 1;
                        //    user.LoginEndDate = DateTime.Now;
                        //    await userManager.UpdateAsync(user);
                        //    context.Validated(identity);
                        //}
                        //else
                        //{
                        //    context.Rejected();
                        //    context.SetError("invalid_grant", "Truy cập bị chặn");

                        //}
                    }
                    else
                    {
                        context.Rejected();
                        context.SetError("invalid_grant", "Tài khoản bị khoá. Vui lòng liên hệ với admin để được mở lại tài khoản");
                    }
                }
                else
                {
                    context.Rejected();
                    context.SetError("invalid_grant", "Tài khoản hoặc mật khẩu không đúng.");
                    
                }
            }
        }

        private static UserManager<ApplicationUser> CreateManager(IdentityFactoryOptions<UserManager<ApplicationUser>> options, IOwinContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context.Get<STourContext>());
            var owinManager = new UserManager<ApplicationUser>(userStore);
            return owinManager;
        }
    }
}