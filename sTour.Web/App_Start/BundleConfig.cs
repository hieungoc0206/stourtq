﻿using System.Web;
using System.Web.Optimization;

namespace sTour.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/commonscript").Include(
                       "~/Assets/home/js/common_scripts_min.js",
                       "~/Assets/home/js/functions.js"
                       ));

        }
    }
}
