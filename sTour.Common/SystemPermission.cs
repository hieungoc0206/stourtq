﻿namespace sTour.Common
{
    public class SystemPermission
    {
        public const string ManagerUsers = "ManagerUsers";
        public const string ManagerSystem = "ManagerSystem";
        public const string ManagerHotel = "ManagerHotel";
        public const string ManagerRestaurant = "ManagerRestaurant";
        public const string ManagerFood = "ManagerFood";
        public const string ManagerCountry = "ManagerCountry";
        public const string ManagerProvince = "ManagerProvince";
        public const string ManagerDistrict = "ManagerDistrict";
        public const string ManagerCultural = "ManagerCultural";
        public const string ManagerBanner = "ManagerBanner";
        public const string ManagerService = "ManagerService";
        public const string ManagerLocation = "ManagerLocation";
        public const string ManagerEnterprise = "ManagerEnterprise";
        public const string ManagerTouristService = "ManagerTouristService";
    }
}