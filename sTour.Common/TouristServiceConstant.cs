﻿namespace sTour.Common
{
    public class TouristServiceConstant
    {
        public const string STATION = "ben-xe";

        public const string BANK = "ngan-hang";

        public const string TRANSPORT = "phuong-tien-giao-thong";

        public const string HOSPITAL = "benh-vien";

        public const string SCHOOL = "truong-hoc";

        public const string MARKET = "trung-tam-thuong-mai";

        public const string COFFEE = "quan-ca-fe-karaoke";

        public const string HOMESTAY = "home-stay";

        public const string MANAGERMENT = "ban-quan-ly";
    }
}