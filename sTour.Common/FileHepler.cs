﻿using System.IO;

namespace sTour.Common
{
    public class FileHepler
    {
        public static void CreateFileFromInputStream(Stream inputStream, string path)
        {
            using (var fileStream = System.IO.File.Create(path))
            {
                inputStream.Seek(0, SeekOrigin.Begin);
                inputStream.CopyTo(fileStream);
                fileStream.Close();
            }
        }
    }
}