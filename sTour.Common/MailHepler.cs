﻿using System;
using System.Net.Mail;

namespace sTour.Common
{
    public class MailHepler
    {
        public static bool SendMail(string toEmail, string subject, string content, string name)
        {
            try
            {
                var host = "smtp.gmail.com";
                var port = 587;
                var fromEmail = "smarttourtq@gmail.com";//Stour
                var password = "smarttour123";
                var fromName = "Stour";

                var smtpClient = new SmtpClient(host, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true,
                    Timeout = 100000
                };

                var mail = new MailMessage
                {
                    Body = content + " - được gửi từ "+toEmail,
                    Subject = subject,
                    From = new MailAddress(fromEmail, fromName)
                };

                mail.To.Add(new MailAddress("bkreslora@gmail.com"));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                smtpClient.Send(mail);

                return true;
            }
            catch (SmtpException ex)
            {
                return false;
            }
        }
    }
}