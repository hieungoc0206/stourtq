﻿namespace sTour.Common
{
    public class Email
    {
        public string FromEmail { get; set; }

        public string PassEmail { get; set; }

        public string ToEmail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}