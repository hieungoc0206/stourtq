﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sTour.Entity
{
    public class TouristService
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string NameEn { get; set; }

        public string HotLine { get; set; }

        public string Category { get; set; }

        public string Address { get; set; }

        public string AddressEn { get; set; }

        public string Content { get; set; }

        public string ContentEn { get; set; }

        public string Avatar { get; set; }
    }
}
