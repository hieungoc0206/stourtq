﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class Role_Permisstion_Mapping
    {
        [Key]
        [Column(Order = 0, TypeName = "nvarchar")]
        public string RoleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int PermissionId { get; set; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole Roles { get; set; }

        [ForeignKey("PermissionId")]
        public virtual Permisstion Permisstions { get; set; }
    }
}