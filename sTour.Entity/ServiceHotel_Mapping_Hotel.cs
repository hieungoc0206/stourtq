﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    [Table("ServiceHotel_Mapping_Hotel")]
    public class ServiceHotel_Mapping_Hotel
    {
        [Key]
        [Column(Order = 0)]
        public int HotelId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ServiceHotelId { get; set; }

        [ForeignKey("HotelId")]
        public virtual Hotel Hotel { get; set; }

        [ForeignKey("ServiceHotelId")]
        public virtual ServiceHotel ServiceHotel { get; set; }
    }
}