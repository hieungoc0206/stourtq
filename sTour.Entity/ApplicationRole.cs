﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sTour.Entity
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string name) : base(name)
        {
        }

        [Required]
        [StringLength(300)]
        public string Description { get; set; }

        public virtual IEnumerable<Role_Permisstion_Mapping> Role_Permisstion_Mappings { get; set; }
    }
}