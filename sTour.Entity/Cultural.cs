﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class Cultural
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        [StringLength(300)]
        public string NameEn { get; set; }

        [Required]
        [StringLength(256)]
        public string Alias { get; set; }

        [StringLength(256)]
        public string AliasEn { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(500)]
        public string AddressEn { get; set; }

        public string ShortDes { get; set; }

        public string ShortDesEn { get; set; }

        public string LongDes { get; set; }

        public string LongDesEn { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        [StringLength(200)]
        public string Avatar { get; set; }

        public int? DistrictId { get; set; }

        //public int? EnterpriseId { get; set; }

        public string VideoDir { get; set; }

        public string VideoDirEn { get; set; }

        public string Images { get; set; }

        public string Audio { get; set; }

        public string AudioEn { get; set; }

        public string OpenDate { get; set; }

        public DateTime CreatedAt { get; set; }

        public int? Star { get; set; }

        public int? ViewCount { get; set; }

        public bool isActive { get; set; }

        //[ForeignKey("EnterpriseId")]
        //public virtual Enterprise enterprise { get; set; }

        [ForeignKey("DistrictId")]
        public virtual District district { get; set; }
    }
}