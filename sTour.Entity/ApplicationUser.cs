﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace sTour.Entity
{
    [Table("ApplicationUser")]
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(256)]
        public string FullName { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        public DateTime? BirthDay { get; set; }

        public DateTime? LoginEndDate { get; set; }

        public bool Gender { get; set; }

        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string LoginEndIP { get; set; }

        public int LoginCount { get; set; }

        public int? CountryId { get; set; }

        public int? EnterpriseId { get; set; }

        [ForeignKey("EnterpriseId")]
        public virtual Enterprise Enterprise { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        public virtual IEnumerable<ActivityLog> ActivityLogs { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}