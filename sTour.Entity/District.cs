﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
namespace sTour.Entity
{
    public class District
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string SortName { get; set; }

        public int ProvincialId { get; set; }

        [ForeignKey("ProvincialId")]
        public virtual Provincial Provincial { get; set; }

        public virtual IEnumerable<Location> Locations { get; set; }

        public virtual IEnumerable<Foods> Foods { get; set; }

        public virtual IEnumerable<Cultural> Culturals { get; set; }
    }
}