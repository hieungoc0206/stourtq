﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace sTour.Entity
{
    [Table("Enterprise")]
    public class Enterprise
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual IEnumerable<Hotel> Hotels { get; set; }

        public virtual IEnumerable<Restaurant> Restaurants { get; set; }

        ////Add
        //public virtual IEnumerable<Cultural> Culturals { get; set; }

        //public virtual IEnumerable<Foods> Foods { get; set; }

        public virtual IEnumerable<ApplicationUser> ApplicationUsers { get; set; }
    }
}
