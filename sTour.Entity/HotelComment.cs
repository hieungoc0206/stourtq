﻿using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class HotelComment
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public int HotelId { get; set; }

        [ForeignKey("HotelId")]
        public virtual Hotel Hotel { get; set; }
    }
}