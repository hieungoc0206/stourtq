﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    [Table("CategoryLocation")]
    public class CategoryLocation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string NameEn { get; set; }

        public string Description { get; set; }

        [StringLength(300)]
        public string Avatar { get; set; }

        public virtual IEnumerable<Location> Locations { get; set; }
    }
}