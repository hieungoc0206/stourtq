﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class ExceptionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ExceptionMessage { get; set; }

        public string ControllerName { get; set; }

        public string ExceptionStackTrace { get; set; }

        public DateTime LogTime { get; set; }
    }
}