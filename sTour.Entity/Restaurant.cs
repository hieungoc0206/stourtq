﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace sTour.Entity
{
    public class Restaurant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        [StringLength(300)]
        public string NameEn { set; get; }

        [Required]
        [StringLength(256)]
        public string Alias { get; set; }

        [StringLength(256)]
        public string AliasEn { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(500)]
        public string AddressEn { get; set; }

        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string HotLine { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int? EnterpriseId { get; set; }

        [StringLength(200)]
        public string Avatar { get; set; }

       
        public string ShortDes { get; set; }

        public string ShortDesEn { get; set; }

        public string LongDes { get; set; }

        public string LongDesEn { get; set; }

        public string Images { get; set; }

        [StringLength(300)]
        public string VideoDir { get; set; }

        public string Audio { get; set; }

        public string OpenDate { get; set; }

        public int Price { get; set; }

        public int Star { get; set; }

        public int ViewCount { get; set; }

        public bool isActive { get; set; }

        [ForeignKey("EnterpriseId")]
        public virtual Enterprise enterprise { get; set; }

        public virtual IEnumerable<Restaurant_Mapping_Location> Restaurant_Mapping_Locations { get; set; }

        public virtual IEnumerable<ServiceRestaurant_Mapping_Restaurant> ServiceRestaurant_Mapping_Restaurants { get; set; }

        [NotMapped]
        public virtual IEnumerable<ServiceRestaurant> Services { get; set; }


    }
}
