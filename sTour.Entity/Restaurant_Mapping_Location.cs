﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sTour.Entity
{
    public class Restaurant_Mapping_Location
    {
        [Key]
        [Column(Order = 0)]
        public int RestaurantId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int LocationId { get; set; }

        [ForeignKey("RestaurantId")]
        public virtual Restaurant Restaurant { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    }
}
