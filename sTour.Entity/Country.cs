﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    [Table("Country")]
    public class Country
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string PhoneCode { get; set; }

        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string SortName { get; set; }

        public virtual IEnumerable<ApplicationUser> ApplicationUsers { get; set; }

        public virtual IEnumerable<Provincial> Provincials { get; set; }
    }
}