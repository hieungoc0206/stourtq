﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class SystemConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string SystemName { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string MailServer { get; set; }

        public int CachedTime { get; set; }

        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string Email { get; set; }

        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string Password { get; set; }

        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string HotLine { get; set; }

        [StringLength(500)]
        public string Contact { get; set; }
    }
}