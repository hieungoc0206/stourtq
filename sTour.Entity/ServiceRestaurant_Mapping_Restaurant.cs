﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    [Table("ServiceRestaurant_Mapping_Restaurant")]
    public class ServiceRestaurant_Mapping_Restaurant
    {
        [Key]
        [Column(Order = 0)]
        public int RestaurantId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ServiceRestaurantId { get; set; }

        [ForeignKey("RestaurantId")]
        public virtual Restaurant Restaurant { get; set; }

        [ForeignKey("ServiceRestaurantId")]
        public virtual ServiceRestaurant ServiceRestaurant { get; set; }
    }
}