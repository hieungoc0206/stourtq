﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    public class Hotel_Mapping_Location
    {
        [Key]
        [Column(Order = 0)]
        public int HotelId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int LocationId { get; set; }

        [ForeignKey("HotelId")]
        public virtual Hotel Hotel { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    }
}
