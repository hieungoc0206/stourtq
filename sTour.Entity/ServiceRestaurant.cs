﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sTour.Entity
{
    [Table("ServiceRestaurant")]
    public class ServiceRestaurant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        [StringLength(300)]
        public string NameEn { get; set; }

        [Required]
        [StringLength(300)]
        public string Icon { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(500)]
        public string DescriptionEn { get; set; }

        public virtual IEnumerable<ServiceRestaurant_Mapping_Restaurant> ServiceRestaurant_Mapping_Restaurants { get; set; }
    }
}